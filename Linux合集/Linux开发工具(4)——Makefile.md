## Makefile
Makefile是Linux下的项目自动化构建工具。
Makefile包含两部分，make是一个指令，makefile是一个文件。
在makefile这个文件里面需要写两部分内容：

1. 依赖关系
2. 依赖方法
### makefile语法
> makefile的意义就是为了构建项目（完成某件事情），举个例子：依赖关系就是完成这件事情要依靠的各种人脉关系，依赖方法就是完成这件事情要做什么。

> makefile的语法，makefile写成Makefile也是没问题的。
> 将makefile文件放入源代码所处的目录中。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668335529325-b4d899f2-1370-4bac-b7d9-8a0bad707d1c.png#averageHue=%234a4a47&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=121&id=u340bf6b9&margin=%5Bobject%20Object%5D&name=image.png&originHeight=182&originWidth=530&originalType=binary&ratio=1&rotation=0&showTitle=false&size=10240&status=done&style=none&taskId=u75d194ff-507d-4ddb-ba3b-131160daddf&title=&width=353.3333333333333)
> 这个就是makefile的语法，第一行是依赖关系，mytest是目标程序，test.c是依赖文件列表，可以是多个文件
> 第二行必须以Tab开头，不可以用四个空格代替，第二行写的就是依赖方法，也就是完成任务所要做的动作。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668335702293-1d6f1408-39ab-41be-a940-9a575ccb09d6.png#averageHue=%230e0c0b&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=439&id=u4206965b&margin=%5Bobject%20Object%5D&name=image.png&originHeight=658&originWidth=852&originalType=binary&ratio=1&rotation=0&showTitle=false&size=67898&status=done&style=none&taskId=ue68447f8-9600-4538-be2f-42f8717e875&title=&width=568)
> 有了makefile之后使用make指令就可以完成对源文件的编译。

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668335780396-d9d9a911-f39b-4f5f-8ba8-352869700046.png#averageHue=%232c2b28&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=161&id=u7fdf830c&margin=%5Bobject%20Object%5D&name=image.png&originHeight=242&originWidth=524&originalType=binary&ratio=1&rotation=0&showTitle=false&size=14321&status=done&style=none&taskId=ue1eedfc9-a2c8-42c6-9251-25d46d83ea3&title=&width=349.3333333333333)
> 新增一个clean功能，既然makefile可以自动化构建项目自然也可清理，这里的.PHONY修饰了clean表示clean是一个伪目标，伪目标就是可以保证目标一直被执行。clean没有依赖关系，也就是clean自己就可以完成任务不需要依赖任何人。

### makefile原理
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668335992368-455e14b4-13a2-4709-a12d-92ec15c3992b.png#averageHue=%23181513&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=163&id=u9b31ded5&margin=%5Bobject%20Object%5D&name=image.png&originHeight=245&originWidth=496&originalType=binary&ratio=1&rotation=0&showTitle=false&size=21187&status=done&style=none&taskId=u096bd64a-74b8-4f2f-9954-a7f668808cb&title=&width=330.6666666666667)
> make只有在第一次编译的时候才会进行更新，后序就不会进行更新因为没有对源文件进行修改自然不需要再重复编译，这是gcc的特性，因为在大型的项目中，编译一个项目少则几十分钟，多则几个小时，所以重复编译没有修改的代码就会造成时间的浪费。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668336163334-4f30b556-a5ab-41a3-9f4a-601ab07a82c1.png#averageHue=%230f0d0b&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=170&id=u4d4d69ce&margin=%5Bobject%20Object%5D&name=image.png&originHeight=255&originWidth=663&originalType=binary&ratio=1&rotation=0&showTitle=false&size=20074&status=done&style=none&taskId=uc9a8521e-cfaa-4787-a925-27094f75b6c&title=&width=442)
> clean是可以被重复执行的。因为clean被.PHONY修饰成伪目标所以其一直是被执行的。
> 这里调用clean的使用的是make clean 因为在调用make的时候其实是省略了mytest，因为调用make的时候默认是从makefile的第一个依赖方法开始执行。所以在这里的make == make mytest。

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668337427224-c5bf3534-b917-4e37-905f-135dd1fe6447.png#averageHue=%23373634&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=201&id=u1bc7201e&margin=%5Bobject%20Object%5D&name=image.png&originHeight=301&originWidth=511&originalType=binary&ratio=1&rotation=0&showTitle=false&size=16380&status=done&style=none&taskId=ub337496c-3622-453d-9440-ee71fee477c&title=&width=340.6666666666667)
> 如果将mytest也修饰成伪目标，那么他也可以每次都被执行。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668337469999-1def0705-2fbd-4ccd-9623-c62573e9f533.png#averageHue=%2313110f&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=167&id=u0897b38e&margin=%5Bobject%20Object%5D&name=image.png&originHeight=251&originWidth=530&originalType=binary&ratio=1&rotation=0&showTitle=false&size=19824&status=done&style=none&taskId=u3df8a712-4590-459b-84f5-78f1f55aafe&title=&width=353.3333333333333)
> 但是不建议这样写，因为如果源文件代码没有被修改那么重复编译是没有意义的。

> gcc防止重复编译的原理就是通过比较可执行程序和源文件的Modify的时间，因为是先有了源文件再有了可执行程序所以，可执行程序的Modify时间一定比源文件早，如果源文件早，那么说明源文件在生成了可执行程序后又进行了修改，所以可以进行再次编译。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668337750046-6e0041a8-4d7f-4df4-a732-018a0547e2e6.png#averageHue=%23100c0b&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=375&id=u01629a09&margin=%5Bobject%20Object%5D&name=image.png&originHeight=562&originWidth=1177&originalType=binary&ratio=1&rotation=0&showTitle=false&size=56908&status=done&style=none&taskId=ub2a5e953-1669-4fe6-9255-9c189390cda&title=&width=784.6666666666666)
> Access时间是访问时间，因为文件被访问的次数可能会很多，所以并不是一次访问文件，文件的Access时间就被更改，而是在一段时间内进行了多次的修改才会修改Access，可以减少在文件高频访问的时候每次都修改时间的性能消耗。
> Modify时间是文件内容被修改的时间
> Change时间是文件属性被修改的时间，一般伴随Modify时间的改变而改变，因为文件的大小也是属性，或者是修改文件的属性而改变。

> 了解了为什么gcc可以禁止重复编译之后，.PHONY的原理也很明朗了，.PHONY修饰的目标可以编译是因为被.PHONY修饰了以后取消使用了gcc的时间比较策略。（每次make是不会影响test.c的时间的）

makefile默认只会生成一个可执行程序。
makefile的推导规则类似于栈，比如下面这样写makefile。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668342167628-2f4a2e41-c567-4a3b-9c38-f187fc5a02e4.png#averageHue=%23333332&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=307&id=u4d621edd&margin=%5Bobject%20Object%5D&name=image.png&originHeight=460&originWidth=628&originalType=binary&ratio=1&rotation=0&showTitle=false&size=27470&status=done&style=none&taskId=u7293cbaa-106d-4478-be14-36da6ad3eb5&title=&width=418.6666666666667)
推导顺序是，mytest依赖于test.o但是现在没有test.o就去下面查看，发现test.o依赖于test.s然后test.s依赖于test.i继续推导test.i依赖于test.c最后test.c发现有这个文件，然后执行test.i的依赖方法生成了test.i然后向上回溯，用test.i生成了test.s 用test.s生成了test.o最后test.o生成了mytest。就是一个栈结构，先遇到的反而最后执行。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668342402026-e9a62526-5a17-494e-b53f-faa27fa318c6.png#averageHue=%230e0c0a&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=395&id=u2de36bb0&margin=%5Bobject%20Object%5D&name=image.png&originHeight=592&originWidth=864&originalType=binary&ratio=1&rotation=0&showTitle=false&size=55549&status=done&style=none&taskId=u7dcb0939-a30d-49fa-bc85-7ce70c4d0ba&title=&width=576)
这张图的命令执行顺序也说明了，Makefile的推导顺序是栈结构的先进后出。
## Linux小程序
### 倒计时小程序
> \n和\r的作用和区别，\r的作用时回车，\n的作用的换行，回车的意思就是将光标退回到这一行的开始，换行就是将光标移动到下一行，所以我们平时说的语言层面的回车是指\r + \n回车加换行。
> 倒计时是指在一个位置显示从10到0的数字，并不是将这11个数字都打印出来，而是在一个位置显示，后面显示的数字会覆盖掉前面显示的数字。这时候就会用到\r让光标回到一行的最开始位置。使用一个sleep休眠函数，就可以了就像下面的代码

```c
  1 #include<stdio.h>
  2 #include<unistd.h>
  3 
  4 int main()
  5 {
  6     int cnt = 10;
  7     while(cnt >= 0)
  8     {
  9         printf("%2d\r",cnt);
 11         sleep(1);
 12         cnt--;
 13     }
 15     return 0;
 16 }
```
但是运行这个代码什么都没有打印出来，这是因为printf打印出来的字符都是在行缓冲区里面，而行缓冲区是很大的，所以除非写满了行缓冲区会将所有的字符一起输出，或遇到了\n换行也会清空行缓冲区。还有一个函数fflush也可以用来清空行缓冲区。
所以最终代码如下：
```c
1 #include<stdio.h>
  2 #include<unistd.h>
  3 
  4 int main()
  5 {
  6     int cnt = 10;
  7     while(cnt >= 0)
  8     {
  9         printf("%2d\r",cnt);
 10         fflush(stdout);
 11         sleep(1);
 12         cnt--;
 13     }
 15     return 0;
 16 }

```
### 进度条程序
进度条程序也是利用了\r回到行最开始的位置，和输出缓冲区的一个小程序代码。
目标的样子是[##########################][100%][/]，'#'代表进度条，后面的数字代表当前加载到的百分比。最后一个方框显示的是一条线在沿着顺时针转动，一般我们看到的进度条都有一个小标记来表示是不是卡死了。如果最后的线还在转说明程序没有卡死。
```c
//test.c
//主函数所在的文件
#include "process.h"
int main()
{
    Process();
    return 0;
}
//process.h
//函数声明所在文件
#include <stdio.h>
#include <unistd.h>

void func();//测试makefile的函数
void Process();
//process.c
//函数实现所在的文件
#include "process.h"

void func()
{
    printf("hello func\n");
}

void Process()
{
    char arr[110] = {0};
    char brr[5] = {'|','/','-','\\'};
    int cnt = 0;
    while(cnt <= 100)
    {
        printf("[%-100s][%d%%][%c]\r",arr,cnt,brr[cnt%4]);
        fflush(stdout);
        usleep(100000);
        arr[cnt++] = '#';
    }
    puts("");
}
```
最终的效果就是这样子。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668388235646-a1c7e829-6bc3-4735-9994-aedcdb6957d9.png#averageHue=%23151311&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=48&id=u9a9e90d5&margin=%5Bobject%20Object%5D&name=image.png&originHeight=72&originWidth=1802&originalType=binary&ratio=1&rotation=0&showTitle=false&size=6466&status=done&style=none&taskId=u92fb4ad5-0ab5-4da5-b8dd-bf8ac4d39dc&title=&width=1201.3333333333333)
当然如果想要更好看一些还可以更改颜色。C语言的printf是可以控制打印的颜色的
主要格式就是：printf("\033[字符背景颜色;字体颜色m要打印的字符串\033[0m]");
最后的0m是控制码
字背景颜色范围: 40--49     字颜色: 30--39
                40: 黑                           30: 黑
                41: 红                           31: 红
                42: 绿                           32: 绿
                43: 黄                           33: 黄
                44: 蓝                           34: 蓝
                45: 紫                           35: 紫
                46: 深绿                       36: 深绿
                47: 白色                       37: 白色
控制码：
  \033[0m   关闭所有属性   
  \033[1m   设置高亮度   
  \033[4m   下划线   
  \033[5m   闪烁   
  \033[7m   反显   
  \033[8m   消隐   
  \033[30m   --   \033[37m   设置前景色   
  \033[40m   --   \033[47m   设置背景色   
  \033[nA   光标上移n行   
  \033[nB   光标下移n行   
  \033[nC   光标右移n行   
  \033[nD   光标左移n行   
  \033[y;xH设置光标位置   
  \033[2J   清屏   
  \033[K   清除从光标到行尾的内容   
  \033[s   保存光标位置   
  \033[u   恢复光标位置   
  \033[?25l   隐藏光标   
  \033[?25h   显示光标
```c
printf("\033[47;35m[%-100s][%d%%][%c]\r\033[0m",arr,cnt,brr[cnt%4]); 
```
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668389523279-21720b89-c8a6-48b7-ac1e-3893813fb4dc.png#averageHue=%23575656&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=48&id=u1e90bee0&margin=%5Bobject%20Object%5D&name=image.png&originHeight=72&originWidth=1786&originalType=binary&ratio=1&rotation=0&showTitle=false&size=6657&status=done&style=none&taskId=u6aae4016-6c0d-4f18-8e38-932b038d6c8&title=&width=1190.6666666666667)
