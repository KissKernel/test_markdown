## git版本控制器
### git是什么
标题也说了git就是一个版本控制器，版本控制器是用来保存一个文件的历史版本，如果有需要可以进行回溯，也就是取得以前编辑完成的版本。
比如你在写一个小程序，你写了三天后第一次测试，有一些bug但是逻辑是没有问题的，所以你上传了git，又进行了几天的bug修复，结果测试完了之后bug更多了。这时候你想拿到以前的版本，就可以进行代码回溯，拿到三天前的那个版本，这就是版本控制器的作用。
简单来说，git就是用来完成版本管理——》版本获取和可视化服务（你可以看到自己的历史版本）
### git的操作
> 如果进行操作的时候出现git没有下载。
> sudo yum install -y git 
> 这行指令就可以下载git。

#### clone仓库到本地
> 要使用git首先你需要先在gitee或者是GitHub上创建一个代码仓库，其实代码仓库就是一个类似于Windows下的一个目录文件。你之后上传的代码都会在这个目录下。

创建好仓库后，可进入仓库找到clone键
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668390365436-5e8efdfe-8f0d-4fe3-afa1-6d94ec177ea5.png#averageHue=%23fcfafa&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=783&id=u9fc3ec20&margin=%5Bobject%20Object%5D&name=image.png&originHeight=1175&originWidth=2109&originalType=binary&ratio=1&rotation=0&showTitle=false&size=333018&status=done&style=none&taskId=u0a49943a-bd3f-4377-b350-c1ac7c51cfb&title=&width=1406)
将这个网址复制下来。进入Linux服务器。
现在要做的就是将创建好的目录clone到本地。
> 指令：git clone [刚刚复制的网址]

![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668426238454-8771e650-fbd7-4394-9520-0e1379b4893c.png#averageHue=%230e0c0b&clientId=uab41144a-45cf-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=149&id=u6890d0cb&margin=%5Bobject%20Object%5D&name=image.png&originHeight=224&originWidth=1312&originalType=binary&ratio=1&rotation=0&showTitle=false&size=26835&status=done&style=none&taskId=ud77e4760-560a-4c73-869d-99b98a0f296&title=&width=874.6666666666666)
现在git上的仓库已经被你拉本地了。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668426284890-3a9a38a1-2e88-4da1-a158-2e0653e894c6.png#averageHue=%230e0a09&clientId=uab41144a-45cf-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=111&id=uccc2ac1d&margin=%5Bobject%20Object%5D&name=image.png&originHeight=167&originWidth=956&originalType=binary&ratio=1&rotation=0&showTitle=false&size=17327&status=done&style=none&taskId=u9c1866df-9f9d-45ae-aa2e-f01d4211856&title=&width=637.3333333333334)
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668426810663-1d1b3a20-fdbc-44aa-aeb9-322627a564f0.png#averageHue=%23110f0d&clientId=uab41144a-45cf-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=249&id=ub4a5a20e&margin=%5Bobject%20Object%5D&name=image.png&originHeight=374&originWidth=843&originalType=binary&ratio=1&rotation=0&showTitle=false&size=40191&status=done&style=none&taskId=u0242c58c-5287-46b3-b88b-1840c8457db&title=&width=562)
我们clone下来的仓库里面默认是就有五个文件的，他们分别是：

1. .git目录，这个.git目录就是本地仓库，想要往gitee上传文件的时候一定是先上传到.git的本地仓库。
2. .gitignore文件，ignore就是忽略的意思，凡是在该文件中出现的后缀在上传gitee的时候都会被忽略。![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668427003643-c47ccc67-99b9-4fee-85a9-158d0747f389.png#averageHue=%23040404&clientId=uab41144a-45cf-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=687&id=u0cade818&margin=%5Bobject%20Object%5D&name=image.png&originHeight=1031&originWidth=651&originalType=binary&ratio=1&rotation=0&showTitle=false&size=47257&status=done&style=none&taskId=u343f250d-26c9-4d4d-9084-4f4901f0128&title=&width=434)
3. LICENESE文件是一个开源许可证。
4. Readme.md是介绍仓库的文件
5. Readme.en.md是英文的仓库介绍文件
#### 上传本地文件到git
> 指令：git add .
> 最后的点代表了当前目录。

当新增了文件之后，使用add就是将新增的文件添加到了.git目录的临时文件保存区。
> 指令：git commit -m '更新日志'
> commit是将保存在.git本地仓库的临时文件上传到本地仓库。

![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668427807801-bfb7f6e9-8a51-4a0f-8497-53db7ab00dfa.png#averageHue=%23080706&clientId=uab41144a-45cf-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=278&id=uba6b3896&margin=%5Bobject%20Object%5D&name=image.png&originHeight=417&originWidth=1147&originalType=binary&ratio=1&rotation=0&showTitle=false&size=28682&status=done&style=none&taskId=uf48689bc-f609-4758-a500-06d997d56dd&title=&width=764.6666666666666)
第一次使用git会遇到以上情况。需要手动设置用户的邮箱，以及用户名。按照系统提示的格式进行初始化即可。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668428031833-9b19396d-a01c-4cd7-9c86-1239da77e62a.png#averageHue=%230d0c0a&clientId=uab41144a-45cf-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=173&id=ud0c3a7d1&margin=%5Bobject%20Object%5D&name=image.png&originHeight=259&originWidth=1003&originalType=binary&ratio=1&rotation=0&showTitle=false&size=25540&status=done&style=none&taskId=u796660df-f9d0-4f24-af3a-1c813008c41&title=&width=668.6666666666666)
初始化完成之后再次进行上传即可。

然后会让你 
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668304357037-17a8e229-f03e-43ac-a448-265f1c028550.png#averageHue=%230d0707&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=291&id=u88bdec29&margin=%5Bobject%20Object%5D&name=image.png&originHeight=436&originWidth=1362&originalType=binary&ratio=1&rotation=0&showTitle=false&size=32474&status=done&style=none&taskId=u9cf45609-17d2-4177-b4cb-2e5d2ea4222&title=&width=908)
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668304384347-bb5d9a78-21df-4038-acae-17d1feb5f004.png#averageHue=%230c0a09&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=337&id=u31d25a63&margin=%5Bobject%20Object%5D&name=image.png&originHeight=506&originWidth=1244&originalType=binary&ratio=1&rotation=0&showTitle=false&size=39506&status=done&style=none&taskId=u612ba17b-8b46-4b08-86c1-7276d349874&title=&width=829.3333333333334)
> 指令：git push
> 将本地仓库的修改同步到远程仓库，也就是上传到gitee上。

![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668428336473-abb29b9a-4444-4e9b-810f-f43edeb8f748.png#averageHue=%230e0b0a&clientId=uab41144a-45cf-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=526&id=uc5aeab31&margin=%5Bobject%20Object%5D&name=image.png&originHeight=789&originWidth=1188&originalType=binary&ratio=1&rotation=0&showTitle=false&size=74141&status=done&style=none&taskId=uc2c6e5e1-f889-4d2b-914d-048365c0b38&title=&width=792)
push的时候需要输入gitee的注册邮箱和gitee的密码。
关于下面这两个警告，可以解决也可不用管。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668384953736-4f8754dd-3eb2-43d9-81a3-d12522e2674d.png#averageHue=%23100c0b&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=317&id=ucab19b32&margin=%5Bobject%20Object%5D&name=image.png&originHeight=476&originWidth=1230&originalType=binary&ratio=1&rotation=0&showTitle=false&size=44037&status=done&style=none&taskId=uf1621406-c11c-4fed-89bb-d783cee4b61&title=&width=820)
如果要解决就按照下面的两条指令输入即可。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668385106627-8232dc6c-8ab4-49f4-91d0-f937cf62a574.png#averageHue=%23151210&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=49&id=udb06577f&margin=%5Bobject%20Object%5D&name=image.png&originHeight=74&originWidth=654&originalType=binary&ratio=1&rotation=0&showTitle=false&size=5219&status=done&style=none&taskId=uc388e2ff-bbb2-4302-a29d-0981edb9fba&title=&width=436)
上传完成后，就可以在gitee的在线仓库上看到了。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668428580944-f0a47e0b-33f4-47eb-b5ba-58a3eabbccf5.png#averageHue=%23fcfbf9&clientId=uab41144a-45cf-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=539&id=ub720713d&margin=%5Bobject%20Object%5D&name=image.png&originHeight=808&originWidth=1531&originalType=binary&ratio=1&rotation=0&showTitle=false&size=123799&status=done&style=none&taskId=u85c2265a-6949-4254-8e21-1c8fc3b824c&title=&width=1020.6666666666666)
> 指令：git log
> 查看这个仓库的日志，那个用户上传的代码，邮箱是什么，上传的信息是什么等等都可看到。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668429087741-b6efab94-47ba-4efe-a274-2b3c2f086f27.png#averageHue=%230c0a09&clientId=uab41144a-45cf-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=524&id=u9b33982e&margin=%5Bobject%20Object%5D&name=image.png&originHeight=786&originWidth=909&originalType=binary&ratio=1&rotation=0&showTitle=false&size=71028&status=done&style=none&taskId=u646e9a11-4684-4878-b58a-5ca47493302&title=&width=606)
> 所以commit的时候的信息一定要好好写。

> 指令：git status
> 查看当前目录的文件修改情况，就是当前目录相对于本地仓库发生了那些差别。比如新增了某些文件，或者删除了某些文件，某些文件被移动了位置，或者被修改了名称等等。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668429202307-d8d11e0d-ad46-4955-9896-a09f9b7db3b6.png#averageHue=%23110e0d&clientId=uab41144a-45cf-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=71&id=ue6e0eb49&margin=%5Bobject%20Object%5D&name=image.png&originHeight=106&originWidth=767&originalType=binary&ratio=1&rotation=0&showTitle=false&size=8890&status=done&style=none&taskId=uf8481532-4d6b-4cba-bc46-cc65b42d3db&title=&width=511.3333333333333)

> git rm [选项]
> 如果想要在本地仓库删除某些文件，一定要使用git rm，这样才可以把你的删除操作同步到远端仓库。

> git mv [选项]
> 同上面的git rm指令。

> 指令：git pull
> 将远程仓库发生的改动拉取到本地仓库。
> 因为git的出现是为了方便多人协同开发的。所以远端仓库如果发生了改变，此时你当前用户的仓库如果没有更新，那么此时你是无法将文件push到远端仓库的，必须要先pull同步到本地，再push才可以。
> 对于这种情况就是发生了冲突（hint），要先进行冲突处理。

![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668429575053-08bc0605-9893-4db1-b2f0-aa2100727925.png#averageHue=%23110e0d&clientId=uab41144a-45cf-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=258&id=uef9946d1&margin=%5Bobject%20Object%5D&name=image.png&originHeight=387&originWidth=1352&originalType=binary&ratio=1&rotation=0&showTitle=false&size=46668&status=done&style=none&taskId=u1b9ce89b-d89b-4a76-b322-db12ba31239&title=&width=901.3333333333334)
这种情况就是本地仓库和远端仓库不同步，发生了冲突。所以要先pull解决冲突才可以push。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668429674970-00fb4955-7c23-43c0-aecd-89f1d7aac6c5.png#averageHue=%230c0a09&clientId=uab41144a-45cf-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=437&id=ucb75387e&margin=%5Bobject%20Object%5D&name=image.png&originHeight=656&originWidth=1069&originalType=binary&ratio=1&rotation=0&showTitle=false&size=57440&status=done&style=none&taskId=u06d3349a-58cf-4aca-897b-aa41e0778db&title=&width=712.6666666666666)
现在我们已经完成了同步，然后再次push就可以了。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668429759498-6dcaa688-df55-4522-81bf-f4234a5fe16d.png#averageHue=%230b0908&clientId=uab41144a-45cf-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=197&id=u7a0e24e9&margin=%5Bobject%20Object%5D&name=image.png&originHeight=296&originWidth=1249&originalType=binary&ratio=1&rotation=0&showTitle=false&size=26087&status=done&style=none&taskId=u8f41b5ed-5234-404f-92fc-012a9307d2d&title=&width=832.6666666666666)
查看文件状态可见，当前有一个文件发生了修改，并且修改还没有同步到本地仓库，所以我们需要add，commit一下。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668429831130-d41f2b5f-2229-4bd1-9430-7789b9e30eb5.png#averageHue=%23100e0d&clientId=uab41144a-45cf-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=86&id=u5506aba4&margin=%5Bobject%20Object%5D&name=image.png&originHeight=129&originWidth=978&originalType=binary&ratio=1&rotation=0&showTitle=false&size=16090&status=done&style=none&taskId=u72abdffa-daa9-41d7-bd24-3121c7e5442&title=&width=652)
现在文件的修改已经同步到本地仓库，现在就可以push到远端仓库了。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668429887738-8523e2f5-0e25-4eba-977e-7aee2972e6d5.png#averageHue=%23120f0e&clientId=uab41144a-45cf-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=233&id=ua0270aba&margin=%5Bobject%20Object%5D&name=image.png&originHeight=350&originWidth=915&originalType=binary&ratio=1&rotation=0&showTitle=false&size=39173&status=done&style=none&taskId=ucf89c85a-b75d-4846-9089-6817d223b50&title=&width=610)
