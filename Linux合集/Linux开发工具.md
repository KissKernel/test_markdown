Linux下的工具本质也是指令 , 下面我会介绍几个常用的工具 , 分别是yum(相当于是手机上的应用商店 , 可以在里面下载工具 ) vim（多模式编辑器）gcc（编译工具）
## 软件包管理器 —— yum
> 在Linux下安装软件, 一个通常的办法是下载到程序的源代码, 并进行编译, 得到可执行程序. 
> 但是这样太麻烦了, 于是有些人把一些常用的软件提前编译好, 做成软件包(可以理解成win上的安
> 装程序)放在一个远端服务器上, 通过包管理器可以很方便的获取到这个编译好的软件包, 直接进行安装.

### 安装软件的三个问题
> **1.确定当前环境是否联网**
> 所以我们安装软件之前要先下载下来软件包，这个过程必须是联网进行的。因为软件包不在本地，而是在远端服务器上。
> 如果你使用的是云服务器那么不需要管，云服务器都是联网的。
> 如果使用的是虚拟机，那么可以使用下面这条指令来检测一下当前是否联网。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667881543690-41c3b1d9-ebcf-4968-907e-595d9d54582c.png#averageHue=%23161412&clientId=u44887941-4d01-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=145&id=ue25d2b8b&margin=%5Bobject%20Object%5D&name=image.png&originHeight=217&originWidth=1280&originalType=binary&ratio=1&rotation=0&showTitle=false&size=35489&status=done&style=none&taskId=uefb4571f-f19a-48b8-ab27-41064777072&title=&width=853.3333333333334)
> 如果下面的信息在不断更新那么当前的网络就是没有问题的。

> **2.软件包不在本地是在远端服务器上**

> **3.Linux系统如何得知去那个服务器上下载软件包？**
> 电脑：一般去百度搜索软件得到下载地址，或者去软件的官网下载。
> 手机：可以通过搜索，也可以通过应用商店（手机上的App）应用商店会把在远端服务器上的软件列出来供用户查看下载）
> Linux：yum工具，yum工具也是类似于AppStore一样的东西，yum里面也保存了各种软件的下载地址，他们都放在一个文件中，这个文件叫做yum源。

> **4.是谁提供的软件且上传到了服务器上？**
> a. 企业或者是个人，他们为了获得某种利益。他们写好了软件之后放在了两个部分，针对电脑用户放在软件的官网，或者是给搜索引擎马内，然后让搜索引擎将你的软件放到首页。方便用户看到。
> b. Linux是个开源系统，所以里面的软件也是具有开源精神的大佬写的。他们写出来的软件也是放在了远端的云服务器上。

### Linux开源生态
上面提到了，开源大佬写的软件都是放在了远端云服务器上，首先第一个问题，真的有人会去写这些软件嘛？答案是：当然有，Linux这个开源系统都有人写何况是一个小小的开源软件。第二个问题就是，Linux既然是开源的，那么必然是没有收费的，那么租用云服务器的钱是从哪里来的呢？
> 这里就要说到了Linux的生态，Linux有自己的社区，这个社区有两个作用，一个作用就是为了开源代码，交流学习，另一个作用就是方便别人找到组织，特别是那些有钱有技术的人找到组织，这些人还有一些企业他们都会给Linux社区捐款，来维持社区的运行。

现在还有一个问题就是，Linux社区这些服务器都是在国内的，那么软件包的下载链接也都是链接到国外的服务器，有些在我们国内是无法访问的，因此，国内的一些企业比如阿里，腾讯，百度，包括一些高校比如清华等，他们会将这些国外的软件资源镜像到国内的服务器上，他们会提供一套国内的下载链接配置文件（yum源）我们只需要通过配置yum源即可。
通过百度就能找到清华大学或者是阿里，百度，腾讯的镜像网站：
清华大学 ：[https://mirrors.tuna.tsinghua.edu.cn/](https://mirrors.tuna.tsinghua.edu.cn/)
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667902390696-0a9ca9d5-5cfe-49c1-b29a-68c20e723596.png#averageHue=%23fefefd&clientId=u44887941-4d01-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=987&id=u804d6626&margin=%5Bobject%20Object%5D&name=image.png&originHeight=1480&originWidth=2145&originalType=binary&ratio=1&rotation=0&showTitle=false&size=349758&status=done&style=none&taskId=u66fc7693-5214-48fc-ac24-a1d0f8635ad&title=&width=1430)
### yum查找软件
> 指令：yum list就会将Linux系统所有的软件包都列举出来。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667902639874-4a94d435-f70d-4474-90e3-7c15cd34bc11.png#averageHue=%23050504&clientId=u44887941-4d01-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=311&id=ubaed0b95&margin=%5Bobject%20Object%5D&name=image.png&originHeight=466&originWidth=2291&originalType=binary&ratio=1&rotation=0&showTitle=false&size=57131&status=done&style=none&taskId=u9875d20c-30e5-4355-a761-29217ae8579&title=&width=1527.3333333333333)
> 但是软件包非常多，我们可以使用grep进行行过滤来找到对应的软件包
> 指令：yum list | grep sl
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667902732052-f3158400-b10a-40e2-b6a0-2613711da8d5.png#averageHue=%230e0c0a&clientId=u44887941-4d01-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=355&id=u75cc68dd&margin=%5Bobject%20Object%5D&name=image.png&originHeight=533&originWidth=1341&originalType=binary&ratio=1&rotation=0&showTitle=false&size=77960&status=done&style=none&taskId=u0866405a-c6d5-4911-8da7-046025530e0&title=&width=894)
> 通过行过滤就会将所有软件包中带有sl字符的软件包都列举出来，我们只需要找到需要的哪一个就可以。sl.x86_64这里的后缀就是体系结构，表示这个软件包适合64位架构的计算机。后面的5.02表示软件版本号，el7表示适合centen os7系统，最后的epel表示的是扩展软件集合，是“软件源”的名称，类似于小米应用商店，华为应用商店这种名称。

> 使用yum查找软件还有一个指令，那就是search
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667903883693-6007708a-4860-40ea-bd5a-b13076b77c44.png#averageHue=%23100e0d&clientId=u44887941-4d01-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=548&id=uf8490fb4&margin=%5Bobject%20Object%5D&name=image.png&originHeight=822&originWidth=1581&originalType=binary&ratio=1&rotation=0&showTitle=false&size=129426&status=done&style=none&taskId=u58ff021b-a587-40f3-a7ef-e2c3c2c524f&title=&width=1054)
> search的作用就是查找到软件包名称中有sl字符的，将其列举出来，并且会将其功能也打印出来。第二个框框里面就是软件功能的描述。

通过上面的工具找到了软件包，下面就是下载软件包了。
### yum下载软件
> 指令：yum install [软件包名称]
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667903175640-37c4c35d-c637-450b-978b-6fef8903ad8d.png#averageHue=%23060605&clientId=u44887941-4d01-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=503&id=u4b3c7952&margin=%5Bobject%20Object%5D&name=image.png&originHeight=754&originWidth=2316&originalType=binary&ratio=1&rotation=0&showTitle=false&size=58698&status=done&style=none&taskId=u66b5c4fc-4054-4451-8e8c-647dd5165b4&title=&width=1544)
> 下载软件一定要使用root用户，或者使用sudo提权，因为软件安装要将一些文件拷贝到指定的目录，有些目录只能用root才可以访问，就像win安装软件的时候以管理员身份运行这个命令。最后一行要输入y就是同意安装，d就是删除软件包，N就是不安装。
> 如果我不想系统询问我，可以用：yum install -y sl 这时候会默认同意所有询问。

安装完软件之后，直接输入sl然后回车，就可以执行这个软件，然后你就会看到一个小火车跑过屏幕。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667903387272-93228e69-d265-42ef-adb8-faf4942a8bcb.png#averageHue=%23030202&clientId=u44887941-4d01-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=487&id=ua66aaf98&margin=%5Bobject%20Object%5D&name=image.png&originHeight=730&originWidth=1877&originalType=binary&ratio=1&rotation=0&showTitle=false&size=20925&status=done&style=none&taskId=u9fbefb06-7d74-4bce-9606-ddb02462f6a&title=&width=1251.3333333333333)
### yum删除软件
> 指令：yum remove [软件名称]
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667903536590-8fb6384b-1db9-4dd6-b3e8-5a19c0fcb1a2.png#averageHue=%23060605&clientId=u44887941-4d01-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=471&id=uca90b797&margin=%5Bobject%20Object%5D&name=image.png&originHeight=707&originWidth=2320&originalType=binary&ratio=1&rotation=0&showTitle=false&size=53308&status=done&style=none&taskId=u9d2561f1-0f53-4581-9ee9-70f69191305&title=&width=1546.6666666666667)
> 删除某个软件同样需要root用户的权限，同安装的时候一样，系统会询问你是否要删除该软件，y就是yes，N就是No。
> 同理如果不想系统询问，那么可以直接使用yum remove -y sl 

### 配置yum源
> 通过下面这条指令我们可以看到当前yum已配置的yum源。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667904114062-c25dcc0a-5844-46a2-bd89-bfcde0954e27.png#averageHue=%2313110f&clientId=u44887941-4d01-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=48&id=u11e7d8f0&margin=%5Bobject%20Object%5D&name=image.png&originHeight=72&originWidth=718&originalType=binary&ratio=1&rotation=0&showTitle=false&size=6992&status=done&style=none&taskId=u785fd704-f1e9-4fcf-9244-6f5a55b19f6&title=&width=478.6666666666667)
> Base结尾的是CentenOS系统自带的基础的yum源，第二个就是CentenOS扩展的yum源。
> 我们可以使用vim打开这些文件。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667904251487-c994d4a7-4cd5-4680-b0e5-8733e4234218.png#averageHue=%23110f0f&clientId=u44887941-4d01-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=399&id=uab4b8977&margin=%5Bobject%20Object%5D&name=image.png&originHeight=598&originWidth=1233&originalType=binary&ratio=1&rotation=0&showTitle=false&size=56513&status=done&style=none&taskId=udd59e330-ab32-438a-be61-343c2d38b9d&title=&width=822)
> 我当前打开的是-Base的文件，可以看到里面的链接就是腾讯的镜像网站，因为使用的是腾讯云，所以这个就不需要我自己配置了。如果虚拟机里面的这个文件中的网址不带有mirrors那么就是需要自己配置的。

这里系统自带的都是官方的软件集合，该集合内的软件都是比较稳定成熟的软件，还有一些软件他们是测试软件，所以还没有收录到官方软件集合里面，我们也可以下载下来这个软件集合的yum源。
指令：yum install -y epel-release
> 推荐一个软件rzsz
> 可以实现在云服务器和win系统的文件传递
> 使用指令：yum install -y lrzsz即可下载该软件
> 下载后可以使用rz，将windows下的文件传送的Linux的当前目录
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667905978058-f6dac490-e0d7-410e-b607-7309ea0f27ae.png#averageHue=%23828181&clientId=u44887941-4d01-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=508&id=u8be19754&margin=%5Bobject%20Object%5D&name=image.png&originHeight=762&originWidth=1559&originalType=binary&ratio=1&rotation=0&showTitle=false&size=76525&status=done&style=none&taskId=u94909246-e4b6-4b81-963f-f2f2fc1ef02&title=&width=1039.3333333333333)
> 就是这种界面。我们可以选中某个文件，然后将该文件发送到Linux系统的当前目录下。
> 还有一中简单的方式，如果我们想要从win发送文件到Linux只需要将文件拖到Linux的黑框框里面即可。但是Linux没有图标所以不能使用拖拽的方式将文件发送到Windows下。

> sz指令就是将Linux下的文件发送到windows下。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667906250316-55652e29-a0bc-491b-9b5a-33a936b84c76.png#averageHue=%235d5c5c&clientId=u44887941-4d01-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=453&id=u72d1fb7c&margin=%5Bobject%20Object%5D&name=image.png&originHeight=680&originWidth=1419&originalType=binary&ratio=1&rotation=0&showTitle=false&size=49289&status=done&style=none&taskId=ufaa57a11-bdfb-4e58-a396-9f03cb8ed59&title=&width=946)
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667906292791-4d0353d2-8cde-492c-9709-ae71b484e353.png#averageHue=%23393736&clientId=u44887941-4d01-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=64&id=ub8b618f6&margin=%5Bobject%20Object%5D&name=image.png&originHeight=96&originWidth=1073&originalType=binary&ratio=1&rotation=0&showTitle=false&size=10079&status=done&style=none&taskId=u27a8178f-ac96-4c4d-b530-b886f79e00b&title=&width=715.3333333333334)
> sz后面需要跟上要传送到window下的文件的相对路径或者绝对路径。

> 如果使用的是虚拟机的话，rzsz软件可能会失效。导致shell子进程卡死。

**注意：安装软件A的时候并不是只A本身，会同时安装上与软件A相关联的其他软件，使用yum下载的时候也是会一起下载下来。**
**软件和软件之间是存在一定的关联的，具有一定的耦合性。**
## 多模式编辑器——vim
> vim是一种多模式编辑器，我这里的vim总共有14种模式，如何查看vim都有什么模式呢？

首先输入指令vim打开vim，然后输入：help vim-modes最后输入回车即可查看vim的模式信息。
vim是一个编辑器那么什么是编辑器？编辑器也是可以写代码的，但是不能编译和运行代码，只能用来编辑代码，这就类似Windows里面的记事本，要和vs2019这种IDE区分开来，IDE是一个集编辑器编译器链接器等等的一个**集成开发环境**有了这一个IDE我们就可以完成代码的编写编译和运行调试。
> vim的模式众多但是日常用到的模式只有几个，这里我会介绍四个模式：命令模式（Normal mode），底行模式（last line mode），插入模式（Insert mode），替换模式（Replace mode）。

### vim的基本操作
> vim [文件名]
> 例如： vim test.c
> 使用vim打开文件。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667997570282-fa0867db-a297-4543-a061-828b4e817e46.png#averageHue=%23303030&clientId=u624123eb-1089-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=242&id=u6ed7339c&margin=%5Bobject%20Object%5D&name=image.png&originHeight=363&originWidth=470&originalType=binary&ratio=1&rotation=0&showTitle=false&size=8337&status=done&style=none&taskId=u241e93a0-023f-4777-b057-defe7833417&title=&width=313.3333333333333)
> 然后就进入了这个界面，需要注意的是使用vim打开文件，如果该文件不存在，那么vim会创建一个test.c文件并打开。

> **模式之间的切换**
> 我们使用vim打开文件之后默认处于命令模式
> 在命令模式下，按大多数按键除了命令按键之外，都是没有用的，系统还会发出嗡嗡的警告声。
> **命令模式和插入模式之间的切换**
> 在命令模式下输入i 就会进入插入模式，现在vim最底下会显示一个--INSERT--这时候就可正常进行文件的编写了。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667997845405-99d4967e-a3c5-4e7f-abbf-7eb1bd771b53.png#averageHue=%233f3f3f&clientId=u624123eb-1089-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=77&id=uaf45caab&margin=%5Bobject%20Object%5D&name=image.png&originHeight=116&originWidth=408&originalType=binary&ratio=1&rotation=0&showTitle=false&size=2472&status=done&style=none&taskId=u4c02b6a6-9d35-4483-ae25-ec023024057&title=&width=272)
> 如果想要从插入模式退回到命令模式，只需要按Esc按键即可。
> **命令模式和底行模式之间的切换**
> 在命令模式下，输入：（冒号，也就是shift + ；）即可进入底行模式。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667997995027-916956c1-f0a7-40e7-a699-4f7e20071bdd.png#averageHue=%23343434&clientId=u624123eb-1089-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=77&id=uabeae7f9&margin=%5Bobject%20Object%5D&name=image.png&originHeight=115&originWidth=366&originalType=binary&ratio=1&rotation=0&showTitle=false&size=1457&status=done&style=none&taskId=u83bd89e1-af3d-45c2-b3bc-3faaa68514a&title=&width=244)
> 当前在最底行会显示一个冒号和光标，这时候我们就可在底行输入指令了。
> 从底行模式退回到命令模式也是按Esc即可。
> **注意：不可能从插入模式直接跳到底行模式，或者从底行模式直接跳到插入模式**
> **从命令模式进入替换模式**
> 在命令模式下，输入R（也就是shift + r）就可进入替换模式，同样的，退出替换模式只需要按Esc即可。

### 命令模式（Normal mode）
> 在命令模式下，我们除了可以向其他模式跳转，还可进行一些快速高效率的文件操作。

> 首先是进行光标移动
> 上下左右的移动，可以使用小键盘的方向键，但是还可以使用HJKL这四个按键，因为小键盘离我们的手比较远，所以使用H J K L比较方便。
> H：向左移动光标
> J ：向下移动光标
> K： 向上移动光标
> L： 向右移动光标

那么为什么会出现这种移动光标的按键呢？
其实是在早期的键盘上是没有上下左右小键盘的，所以要进行光标移动就需要使用别的按键
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667998799709-ac174a97-e801-4d07-9120-1f072116cb45.png#averageHue=%2343534d&clientId=u624123eb-1089-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=413&id=ufba0f013&margin=%5Bobject%20Object%5D&name=image.png&originHeight=619&originWidth=1265&originalType=binary&ratio=1&rotation=0&showTitle=false&size=1403813&status=done&style=none&taskId=u2e6a4eb0-e4a7-4361-9929-a6b26552d93&title=&width=843.3333333333334)
> 为了 方便记忆按键对应的方向，可以这样想，H在最左端就是向左移动，L在最右端就是向右移动，J就是Jump的意思，向下跳，自然对应向下移动，K就是King国王自然高高在上就是向上移动。

> 光标定位
> **shift + 4 （$）**：将光标移动到行末
> **shift + 6 （^）**：将光标移动到行始
> **shift + g **： （G）将光标移动到文档末尾
> **gg**：回到文档的第一行。
> **n + shift + g**： 跳转到文档的第n行

> 文本编辑相关的快捷键
> **yy** ：复制光标所在行的文本 （nyy从当前行开始向下总计复制n行文本）yy相当于是1yy
> **p** ：粘贴，将复制的文本粘贴到光标所在行（支持多次粘贴，npp就是从光标所在行向下连续粘贴n次）
> **dd** ：删除， 删除光标所在行（ndd删除n行）
> 剪切指令在vim这里就是先dd然后再p，就可完成剪切的功能。
> **ctrl + r** ：恢复撤销的操作，也就是取消u的操作。
> **shift + `**：（组合起来就是~）将光标移动到要进行大小写转换的行上，一直按住shift + `就自动向后进行大小写转换，会把大写变为小写，小写变为大写，如果想要转换一个字符那么就按一次。
> 行内删除：**x**就是从光标开始向后删除，**shift + x（X）**就是删除光标前的字符，向前删除
> 删除支持**nx**和**nX**，同时删除掉的字符可以使用**p**粘贴出来。
> **w**：向后移动光标，按单词移动
> **b** ：向前移动光标，按单词移动

总结：命令模式给我们提供的快捷键可以大大加快我们对文档进行批量化操作的效率，所以命令模式的意义就是提高编辑效率。
### 插入模式（Insert mode）
插入模式很简单，就是可以正常的进行文本输入。
从命令模式进入插入模式有很多方式，常用的就是三个按键
i，进入插入模式后光标不动，可以直接再光标处输入
a，进入插入模式后光标向后移动一个位置
o，进入插入模式后光标会向下插入一个空行，并且移动到该空行上
### 底行模式（Last line mode）
在命令模式下输入：进入底行模式，在底行里我们可以输入很多指令，这里主要介绍几个常用的指令。

1. set nu  可以显示行号
2. set nonu  取消显示的行号
3. vs [文件名] 可以进行分屏，如果文件不存在会创建一个文件![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668001595022-af4989e7-6ccf-488b-aa21-66189cf333d0.png#averageHue=%23110e0e&clientId=u624123eb-1089-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=743&id=ud6652177&margin=%5Bobject%20Object%5D&name=image.png&originHeight=1114&originWidth=2348&originalType=binary&ratio=1&rotation=0&showTitle=false&size=120436&status=done&style=none&taskId=ua9137831-db30-4f80-89b7-748714605a9&title=&width=1565.3333333333333)还可以分很多屏理论上没有限制，但是我们的屏幕只有这么大，而且在我分到第五屏的时候就卡死了。我们可以使用ctrl + ww在分屏之间切换，也就是在各个屏幕之间移动光标，光标在那一屏那么我们就可以在那一屏进行操作。
4. 底行输入！+ shell指令就可以执行命令，比如我们在vim下编写代码的时候遇到了一个库函数不知道该如何使用，那么我们可以直接用man查询这个函数。指令就是：！man 3 memset 然后加回车即可。![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668002342970-07cb292a-9187-4f0b-a867-8f4356a1f265.png#averageHue=%231e1e1d&clientId=u624123eb-1089-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=832&id=ucad69def&margin=%5Bobject%20Object%5D&name=image.png&originHeight=1248&originWidth=2352&originalType=binary&ratio=1&rotation=0&showTitle=false&size=78960&status=done&style=none&taskId=ubd0995dc-2937-4156-87fc-4a94919362f&title=&width=1568)会直接跳转到man手册里面，按下q退出man手册之后会回到命令行界面。![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668002471121-8d1d34ff-5bc4-4a75-9865-fefcd21341c5.png#averageHue=%23070605&clientId=u624123eb-1089-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=127&id=u7c1d0294&margin=%5Bobject%20Object%5D&name=image.png&originHeight=191&originWidth=1304&originalType=binary&ratio=1&rotation=0&showTitle=false&size=15318&status=done&style=none&taskId=uc95c477a-da21-4419-938a-46210018870&title=&width=869.3333333333334)只要按下enter就可回到vim里面继续编辑代码。
5. : %s/printf/cout/g 批量替换，输入这一行指令就是将文档中所有的printf替换成cout，这里的g就是globa代表全局的意思。
6. 在底行中直接输入数字，就可将光标跳转到数字对应的行。
7. /关键字，输入/后直接输入要查找的关键字，就可以在文档中查找你输入的关键字并进行高亮。如果当前显示的不是你要找的，按n会直接向下跳到下一个关键字处。
8. ？关键字，输入？后直接输入要查找的关键字，就可以在文档中查找你输入的关键字并进行高亮。如果当前显示的不是你要找的，按n会跳到上一个关键字处，按enter会跳到下一个关键字处。

可以看到？和/的功能是不是一样的，看起来是一样的，但是他们还是有不同的。
> 最后的指令就是如何保存文件并退出vim，在底行模式下，输入q就是quit的意思，直接退出不保存，输入w就是保存文档，保存并退出就是wq。如果遇到了只读文件使用w不能保存那么我们可以输入w！加上！表示强制保存，退出的时候如果退出不了也可以使用q！强制退出，wq！就是强制保存并退出。

## vim的基本配置原理
我们现在的vim实际上是非常难用的，没有配置的vim写代码就像是在windows的记事本上写代码，没有自动补全，没有代码高亮，没有缩进等等。
所以我们要对我们的vim进行配置，配置vim的原理就是在当前用户的家目录下添加一个.vimrc的文件，以点开头的文件都是隐藏文件，添加完.vimrc文件然后我们就可以在这个文件里面进行配置了。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668005027858-d42bb0eb-d37f-4a8b-8b90-92f0922e09af.png#averageHue=%23444343&clientId=u624123eb-1089-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=114&id=u9e1717ea&margin=%5Bobject%20Object%5D&name=image.png&originHeight=171&originWidth=295&originalType=binary&ratio=1&rotation=0&showTitle=false&size=4632&status=done&style=none&taskId=ud37df600-9be4-4250-86a2-c3bf3874d48&title=&width=196.66666666666666)
这是.vimrc里面的内容，保存退出。
配置完之后再用vim打开一个文件
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668004964927-3b4043ab-3a5b-48db-8502-3d0bf056a52d.png#averageHue=%23070606&clientId=u624123eb-1089-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=253&id=u5ed5c62c&margin=%5Bobject%20Object%5D&name=image.png&originHeight=379&originWidth=662&originalType=binary&ratio=1&rotation=0&showTitle=false&size=12955&status=done&style=none&taskId=ucec4d050-d2f2-40e2-b2ec-b9c96eb9622&title=&width=441.3333333333333)
这样就可以默认显示行号了。
更多具体的配置我们可以百度出来，然后将对应的选项加入.vimrc文件保存之后，如果添加的指令没有在vim里面生效，可以输入source .vimrc让配置生效一下，如果还是不行，那说明你设置的选项在当前的vim下不支持。
关于vim的配置：

1. vim的配置是一人一份的，配置一个用户不影响其他用户，因为我们当前用户的.vimrc配置文件时是在用户的家目录下，每个用户的配置文件不同那么vim的配置自然就不同。
2. 所有用户使用的是同一个vim程序，但是使用的是不同的vim配置文件。
3. 配置vim，就是修改用户家目录下的.vimrc配置文件。
4. 从哪里可以获取vim的配置选项？百度吧少年

虽然说vim的配置文件是每个用户都有一个的也就是说每个用户的vim配置是不一样的，但是我们也可设置一个全局的vim配置给所有的用户都进行vim的配置，这个配置文件的路径就是/etc/vimrc，vimrc就是这个全局的vim配置文件。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668006157630-b9026e59-4262-454f-b924-a4f3236e85ba.png#averageHue=%2312100e&clientId=u624123eb-1089-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=45&id=u1b072e62&margin=%5Bobject%20Object%5D&name=image.png&originHeight=68&originWidth=826&originalType=binary&ratio=1&rotation=0&showTitle=false&size=7730&status=done&style=none&taskId=u2e831d13-790c-41a0-b807-d64e4ccd03b&title=&width=550.6666666666666)
## 配置sudoers文件
如果没有配置过sudoers文件那么是不能使用sudo指令来进行提权的。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667831608434-41eb4bc9-e857-4f03-ba5a-c8335cb1ddaa.png#averageHue=%230f0d0c&clientId=uba966951-6c08-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=66&id=u0c9c7527&margin=%5Bobject%20Object%5D&name=image.png&originHeight=99&originWidth=1043&originalType=binary&ratio=1&rotation=0&showTitle=false&size=10417&status=done&style=none&taskId=u5a61a34e-aa90-44ed-b588-d13427cdf85&title=&width=695.3333333333334)
会爆出错误说你当前的用户不在sudoers文件中，为了解决这个问题我们就需要配置一下该文件。
首先需要找到sudoers这个文件，这个文件的路径是：/etc/sudoers
