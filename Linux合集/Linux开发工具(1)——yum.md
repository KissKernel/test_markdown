Linux下的工具本质也是指令 , 下面我会介绍几个常用的工具 , 分别是yum(相当于是手机上的应用商店 , 可以在里面下载工具 ) vim（多模式编辑器）gcc（编译工具）
## 软件包管理器 —— yum
> 在Linux下安装软件, 一个通常的办法是下载到程序的源代码, 并进行编译, 得到可执行程序. 
> 但是这样太麻烦了, 于是有些人把一些常用的软件提前编译好, 做成软件包(可以理解成win上的安
> 装程序)放在一个远端服务器上, 通过包管理器可以很方便的获取到这个编译好的软件包, 直接进行安装.

### 安装软件的三个问题
> **1.确定当前环境是否联网**
> 所以我们安装软件之前要先下载下来软件包，这个过程必须是联网进行的。因为软件包不在本地，而是在远端服务器上。
> 如果你使用的是云服务器那么不需要管，云服务器都是联网的。
> 如果使用的是虚拟机，那么可以使用下面这条指令来检测一下当前是否联网。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667881543690-41c3b1d9-ebcf-4968-907e-595d9d54582c.png#averageHue=%23161412&clientId=u44887941-4d01-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=145&id=ue25d2b8b&margin=%5Bobject%20Object%5D&name=image.png&originHeight=217&originWidth=1280&originalType=binary&ratio=1&rotation=0&showTitle=false&size=35489&status=done&style=none&taskId=uefb4571f-f19a-48b8-ab27-41064777072&title=&width=853.3333333333334)
> 如果下面的信息在不断更新那么当前的网络就是没有问题的。

> **2.软件包不在本地是在远端服务器上**

> **3.Linux系统如何得知去那个服务器上下载软件包？**
> 电脑：一般去百度搜索软件得到下载地址，或者去软件的官网下载。
> 手机：可以通过搜索，也可以通过应用商店（手机上的App）应用商店会把在远端服务器上的软件列出来供用户查看下载）
> Linux：yum工具，yum工具也是类似于AppStore一样的东西，yum里面也保存了各种软件的下载地址，他们都放在一个文件中，这个文件叫做yum源。

> **4.是谁提供的软件且上传到了服务器上？**
> a. 企业或者是个人，他们为了获得某种利益。他们写好了软件之后放在了两个部分，针对电脑用户放在软件的官网，或者是给搜索引擎马内，然后让搜索引擎将你的软件放到首页。方便用户看到。
> b. Linux是个开源系统，所以里面的软件也是具有开源精神的大佬写的。他们写出来的软件也是放在了远端的云服务器上。

### Linux开源生态
上面提到了，开源大佬写的软件都是放在了远端云服务器上，首先第一个问题，真的有人会去写这些软件嘛？答案是：当然有，Linux这个开源系统都有人写何况是一个小小的开源软件。第二个问题就是，Linux既然是开源的，那么必然是没有收费的，那么租用云服务器的钱是从哪里来的呢？
> 这里就要说到了Linux的生态，Linux有自己的社区，这个社区有两个作用，一个作用就是为了开源代码，交流学习，另一个作用就是方便别人找到组织，特别是那些有钱有技术的人找到组织，这些人还有一些企业他们都会给Linux社区捐款，来维持社区的运行。

现在还有一个问题就是，Linux社区这些服务器都是在国内的，那么软件包的下载链接也都是链接到国外的服务器，有些在我们国内是无法访问的，因此，国内的一些企业比如阿里，腾讯，百度，包括一些高校比如清华等，他们会将这些国外的软件资源镜像到国内的服务器上，他们会提供一套国内的下载链接配置文件（yum源）我们只需要通过配置yum源即可。
通过百度就能找到清华大学或者是阿里，百度，腾讯的镜像网站：
清华大学 ：[https://mirrors.tuna.tsinghua.edu.cn/](https://mirrors.tuna.tsinghua.edu.cn/)
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667902390696-0a9ca9d5-5cfe-49c1-b29a-68c20e723596.png#averageHue=%23fefefd&clientId=u44887941-4d01-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=987&id=u804d6626&margin=%5Bobject%20Object%5D&name=image.png&originHeight=1480&originWidth=2145&originalType=binary&ratio=1&rotation=0&showTitle=false&size=349758&status=done&style=none&taskId=u66fc7693-5214-48fc-ac24-a1d0f8635ad&title=&width=1430)
### yum查找软件
> 指令：yum list就会将Linux系统所有的软件包都列举出来。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667902639874-4a94d435-f70d-4474-90e3-7c15cd34bc11.png#averageHue=%23050504&clientId=u44887941-4d01-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=311&id=ubaed0b95&margin=%5Bobject%20Object%5D&name=image.png&originHeight=466&originWidth=2291&originalType=binary&ratio=1&rotation=0&showTitle=false&size=57131&status=done&style=none&taskId=u9875d20c-30e5-4355-a761-29217ae8579&title=&width=1527.3333333333333)
> 但是软件包非常多，我们可以使用grep进行行过滤来找到对应的软件包
> 指令：yum list | grep sl
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667902732052-f3158400-b10a-40e2-b6a0-2613711da8d5.png#averageHue=%230e0c0a&clientId=u44887941-4d01-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=355&id=u75cc68dd&margin=%5Bobject%20Object%5D&name=image.png&originHeight=533&originWidth=1341&originalType=binary&ratio=1&rotation=0&showTitle=false&size=77960&status=done&style=none&taskId=u0866405a-c6d5-4911-8da7-046025530e0&title=&width=894)
> 通过行过滤就会将所有软件包中带有sl字符的软件包都列举出来，我们只需要找到需要的哪一个就可以。sl.x86_64这里的后缀就是体系结构，表示这个软件包适合64位架构的计算机。后面的5.02表示软件版本号，el7表示适合centen os7系统，最后的epel表示的是扩展软件集合，是“软件源”的名称，类似于小米应用商店，华为应用商店这种名称。

> 使用yum查找软件还有一个指令，那就是search
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667903883693-6007708a-4860-40ea-bd5a-b13076b77c44.png#averageHue=%23100e0d&clientId=u44887941-4d01-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=548&id=uf8490fb4&margin=%5Bobject%20Object%5D&name=image.png&originHeight=822&originWidth=1581&originalType=binary&ratio=1&rotation=0&showTitle=false&size=129426&status=done&style=none&taskId=u58ff021b-a587-40f3-a7ef-e2c3c2c524f&title=&width=1054)
> search的作用就是查找到软件包名称中有sl字符的，将其列举出来，并且会将其功能也打印出来。第二个框框里面就是软件功能的描述。

通过上面的工具找到了软件包，下面就是下载软件包了。
### yum下载软件
> 指令：yum install [软件包名称]
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667903175640-37c4c35d-c637-450b-978b-6fef8903ad8d.png#averageHue=%23060605&clientId=u44887941-4d01-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=503&id=u4b3c7952&margin=%5Bobject%20Object%5D&name=image.png&originHeight=754&originWidth=2316&originalType=binary&ratio=1&rotation=0&showTitle=false&size=58698&status=done&style=none&taskId=u66b5c4fc-4054-4451-8e8c-647dd5165b4&title=&width=1544)
> 下载软件一定要使用root用户，或者使用sudo提权，因为软件安装要将一些文件拷贝到指定的目录，有些目录只能用root才可以访问，就像win安装软件的时候以管理员身份运行这个命令。最后一行要输入y就是同意安装，d就是删除软件包，N就是不安装。
> 如果我不想系统询问我，可以用：yum install -y sl 这时候会默认同意所有询问。

安装完软件之后，直接输入sl然后回车，就可以执行这个软件，然后你就会看到一个小火车跑过屏幕。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667903387272-93228e69-d265-42ef-adb8-faf4942a8bcb.png#averageHue=%23030202&clientId=u44887941-4d01-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=487&id=ua66aaf98&margin=%5Bobject%20Object%5D&name=image.png&originHeight=730&originWidth=1877&originalType=binary&ratio=1&rotation=0&showTitle=false&size=20925&status=done&style=none&taskId=u9fbefb06-7d74-4bce-9606-ddb02462f6a&title=&width=1251.3333333333333)
### yum删除软件
> 指令：yum remove [软件名称]
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667903536590-8fb6384b-1db9-4dd6-b3e8-5a19c0fcb1a2.png#averageHue=%23060605&clientId=u44887941-4d01-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=471&id=uca90b797&margin=%5Bobject%20Object%5D&name=image.png&originHeight=707&originWidth=2320&originalType=binary&ratio=1&rotation=0&showTitle=false&size=53308&status=done&style=none&taskId=u9d2561f1-0f53-4581-9ee9-70f69191305&title=&width=1546.6666666666667)
> 删除某个软件同样需要root用户的权限，同安装的时候一样，系统会询问你是否要删除该软件，y就是yes，N就是No。
> 同理如果不想系统询问，那么可以直接使用yum remove -y sl 

### 配置yum源
> 通过下面这条指令我们可以看到当前yum已配置的yum源。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667904114062-c25dcc0a-5844-46a2-bd89-bfcde0954e27.png#averageHue=%2313110f&clientId=u44887941-4d01-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=48&id=u11e7d8f0&margin=%5Bobject%20Object%5D&name=image.png&originHeight=72&originWidth=718&originalType=binary&ratio=1&rotation=0&showTitle=false&size=6992&status=done&style=none&taskId=u785fd704-f1e9-4fcf-9244-6f5a55b19f6&title=&width=478.6666666666667)
> Base结尾的是CentenOS系统自带的基础的yum源，第二个就是CentenOS扩展的yum源。
> 我们可以使用vim打开这些文件。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667904251487-c994d4a7-4cd5-4680-b0e5-8733e4234218.png#averageHue=%23110f0f&clientId=u44887941-4d01-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=399&id=uab4b8977&margin=%5Bobject%20Object%5D&name=image.png&originHeight=598&originWidth=1233&originalType=binary&ratio=1&rotation=0&showTitle=false&size=56513&status=done&style=none&taskId=udd59e330-ab32-438a-be61-343c2d38b9d&title=&width=822)
> 我当前打开的是-Base的文件，可以看到里面的链接就是腾讯的镜像网站，因为使用的是腾讯云，所以这个就不需要我自己配置了。如果虚拟机里面的这个文件中的网址不带有mirrors那么就是需要自己配置的。

这里系统自带的都是官方的软件集合，该集合内的软件都是比较稳定成熟的软件，还有一些软件他们是测试软件，所以还没有收录到官方软件集合里面，我们也可以下载下来这个软件集合的yum源。
指令：yum install -y epel-release
> 推荐一个软件rzsz
> 可以实现在云服务器和win系统的文件传递
> 使用指令：yum install -y lrzsz即可下载该软件
> 下载后可以使用rz，将windows下的文件传送的Linux的当前目录
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667905978058-f6dac490-e0d7-410e-b607-7309ea0f27ae.png#averageHue=%23828181&clientId=u44887941-4d01-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=508&id=u8be19754&margin=%5Bobject%20Object%5D&name=image.png&originHeight=762&originWidth=1559&originalType=binary&ratio=1&rotation=0&showTitle=false&size=76525&status=done&style=none&taskId=u94909246-e4b6-4b81-963f-f2f2fc1ef02&title=&width=1039.3333333333333)
> 就是这种界面。我们可以选中某个文件，然后将该文件发送到Linux系统的当前目录下。
> 还有一中简单的方式，如果我们想要从win发送文件到Linux只需要将文件拖到Linux的黑框框里面即可。但是Linux没有图标所以不能使用拖拽的方式将文件发送到Windows下。

> sz指令就是将Linux下的文件发送到windows下。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667906250316-55652e29-a0bc-491b-9b5a-33a936b84c76.png#averageHue=%235d5c5c&clientId=u44887941-4d01-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=453&id=u72d1fb7c&margin=%5Bobject%20Object%5D&name=image.png&originHeight=680&originWidth=1419&originalType=binary&ratio=1&rotation=0&showTitle=false&size=49289&status=done&style=none&taskId=ufaa57a11-bdfb-4e58-a396-9f03cb8ed59&title=&width=946)
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667906292791-4d0353d2-8cde-492c-9709-ae71b484e353.png#averageHue=%23393736&clientId=u44887941-4d01-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=64&id=ub8b618f6&margin=%5Bobject%20Object%5D&name=image.png&originHeight=96&originWidth=1073&originalType=binary&ratio=1&rotation=0&showTitle=false&size=10079&status=done&style=none&taskId=u27a8178f-ac96-4c4d-b530-b886f79e00b&title=&width=715.3333333333334)
> sz后面需要跟上要传送到window下的文件的相对路径或者绝对路径。

> 如果使用的是虚拟机的话，rzsz软件可能会失效。导致shell子进程卡死。

**注意：安装软件A的时候并不是只A本身，会同时安装上与软件A相关联的其他软件，使用yum下载的时候也是会一起下载下来。**
**软件和软件之间是存在一定的关联的，具有一定的耦合性。**
