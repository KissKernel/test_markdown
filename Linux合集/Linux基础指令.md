## 操作系统概念
> 谈到Linux之前我们知道Linux是一个操作系统，那么什么是操作系统？

我们的计算机可以说是由这四部分组成，从底层往上看分别是，硬件，驱动，操作系统，应用软件。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665371903399-a81d53d6-b1b2-4b55-9990-063ba40bc286.png#clientId=u3ed62342-cc95-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=318&id=u5e19d7a5&margin=%5Bobject%20Object%5D&name=image.png&originHeight=477&originWidth=994&originalType=binary&ratio=1&rotation=0&showTitle=false&size=176137&status=done&style=none&taskId=u39a751b3-ea0f-43bc-9915-43949050448&title=&width=662.6666666666666)
应用软件就是用于使用的软件像是QQ，浏览器等等都是属于用户层应用软件。
操作系统是属于一个连接层，对上需要给用户提供一个稳定，安全，高效的运行环境。对下需要进行各种软硬件资源的管理。操作系统简称是OS( operator system )
操作系统的管理工作主要可以分为四部分：

1. 进程管理
2. 文件系统
3. 内存管理
4. 驱动管理
## Linux的基本指令
> 下面将要介绍的是Linux的基本指令，这里使用的环境是centen os 7.6会存在某些指令和其他的Linux系统存在不同，比如centen os使用的是YUM软件包，所以下载的时候使用的yum..........但是基本的指令还是一样的。

### ls指令
> **语法：** ls [选项][目录或文件] 
> **功能：**对于文件显示当前文件的信息，对于目录是当前目录下的所有子目录和文件
> **常用选项：** 
> 1. -l 显示更多属性，可以缩写成ll
> 2. -a 列出目录下的所有文件，包括以 . 开头的隐含文件。
> 3. -d  [文件名\目录名] 可以不进入目录显示目录的信息
> 4. -n   用数字的 UID,GID 代替名称。
> 5. -F 在每个文件名后附上一个字符以说明该文件的类型，“*”表示可执行的普通文件；“/”表示目录；“@”表示符号链接；“|”表示FIFOs；“=”表示套接字(sockets)。
> 6. -r 对目录进行反向排序
> 7. -t 以时间排序。
> 8. -s 在l文件名后输出该文件的大小。（大小排序，如何找到目录下最大的文件）
> 9. -R 列出所有子目录下的文件。(递归)
> 10. -1 一行只输出一个文件。

指令是可以任意组合的，可以将多个指令组合在一起执行，比如 ls -al等等都是没用问题的。
**指令演示部分**
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665373179116-cfa66faa-0c49-4619-b531-8211fc9f8b2e.png#clientId=u3ed62342-cc95-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=50&id=u6fbab88f&margin=%5Bobject%20Object%5D&name=image.png&originHeight=75&originWidth=1946&originalType=binary&ratio=1&rotation=0&showTitle=false&size=10110&status=done&style=none&taskId=ude37c47f-8315-49ef-ab34-fcba3aaf7e7&title=&width=1297.3333333333333)
> 这是使用ls打印出来的内容，目录下的子目录。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665373228143-a8229cc5-3086-43ce-8bcc-6eb55c2ea16f.png#clientId=u3ed62342-cc95-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=171&id=u50b09a9f&margin=%5Bobject%20Object%5D&name=image.png&originHeight=257&originWidth=1004&originalType=binary&ratio=1&rotation=0&showTitle=false&size=27288&status=done&style=none&taskId=u2abfdb55-d270-4ab9-be0e-035176cf969&title=&width=669.3333333333334)
> 这是我们使用ls -l打印出来的内容当然使用（ll也是一样的效果），每个目录都带上了目录的具体信息，这里以dr开头的都是目录，dr就是directory的缩写。

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665373759189-0e1b6a9e-61ef-4f5e-8b07-21d2f022bbe2.png#clientId=u3ed62342-cc95-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=76&id=u216cf05f&margin=%5Bobject%20Object%5D&name=image.png&originHeight=114&originWidth=2047&originalType=binary&ratio=1&rotation=0&showTitle=false&size=13839&status=done&style=none&taskId=ud34b9da1-8807-42db-9f7d-e7a1e45fed6&title=&width=1364.6666666666667)
> 这是ls -a打印出来的目录，可以看到这里多了两个文件，Linux下以点开头的文件都是隐藏文件，一个点表示当前目录，两个点表示上级目录。

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665374142116-11942753-8ded-4c9a-afdc-ae41e0b618a4.png#clientId=u3ed62342-cc95-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=117&id=u57313a9e&margin=%5Bobject%20Object%5D&name=image.png&originHeight=175&originWidth=772&originalType=binary&ratio=1&rotation=0&showTitle=false&size=13640&status=done&style=none&taskId=u2682a78b-bc7d-43b5-ae5e-b95ace7c331&title=&width=514.6666666666666)
> 这里是ls -d的使用，可以不进入目录指定显示该目录或者文件的信息

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665374369981-80697cd5-2b33-47cd-8d5d-869845762818.png#clientId=u3ed62342-cc95-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=211&id=u54deb278&margin=%5Bobject%20Object%5D&name=image.png&originHeight=317&originWidth=724&originalType=binary&ratio=1&rotation=0&showTitle=false&size=29339&status=done&style=none&taskId=uf48dd9cd-6e28-4929-9159-a4c307bc53a&title=&width=482.6666666666667)
> 这是ls -n的使用，这里的root被替换成了数字0.

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665374535878-fc491213-7bd7-4ad3-90b6-72289d899549.png#clientId=u3ed62342-cc95-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=87&id=u4b1b78fc&margin=%5Bobject%20Object%5D&name=image.png&originHeight=131&originWidth=517&originalType=binary&ratio=1&rotation=0&showTitle=false&size=9202&status=done&style=none&taskId=u4ddfcdd8-8e2e-416f-8c3b-c0f73ce790e&title=&width=344.6666666666667)
> 这是ls -F的使用，目录的后面都加上了 \

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665374627171-b340cb02-8289-4240-9c8a-ced6c8eb6fc4.png#clientId=u3ed62342-cc95-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=43&id=u2363b846&margin=%5Bobject%20Object%5D&name=image.png&originHeight=65&originWidth=496&originalType=binary&ratio=1&rotation=0&showTitle=false&size=4451&status=done&style=none&taskId=u2080aeee-a372-479f-a68f-be3111f53c1&title=&width=330.6666666666667)
> ls -r 的使用，将目录反向排序了

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665374716999-5fd3fb11-e075-4f17-bc0f-bfcb3ea0d0af.png#clientId=u3ed62342-cc95-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=159&id=ue036b47e&margin=%5Bobject%20Object%5D&name=image.png&originHeight=239&originWidth=768&originalType=binary&ratio=1&rotation=0&showTitle=false&size=21633&status=done&style=none&taskId=ude47855d-d62f-4caa-9da0-49e0554b013&title=&width=512)
> ls -t 按照时间进行排序，最后创建的目录或文件打印在第一行

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665375412668-7c851e1a-27e1-4ea0-b499-ba738514ac60.png#clientId=u3ed62342-cc95-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=149&id=u9beaa06a&margin=%5Bobject%20Object%5D&name=image.png&originHeight=223&originWidth=769&originalType=binary&ratio=1&rotation=0&showTitle=false&size=21604&status=done&style=none&taskId=u9563a1de-c80e-46b1-a43f-2adaf4bd6ab&title=&width=512.6666666666666)
> ls -s 显示文件或者目录大小在开头这里多了数字4

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665375978245-0efd14a0-fb54-4e53-befb-1e04671c4920.png#clientId=u3ed62342-cc95-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=313&id=u52d25838&margin=%5Bobject%20Object%5D&name=image.png&originHeight=469&originWidth=592&originalType=binary&ratio=1&rotation=0&showTitle=false&size=15004&status=done&style=none&taskId=u7259cf32-e8b6-46e1-ac50-6fc56c7d3e6&title=&width=394.6666666666667)
> ls -R 递归打印出了当前目录的子目录下的所有文件和目录

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665376031936-1aed02ca-9c14-4bd7-a476-3c28d20253bf.png#clientId=u3ed62342-cc95-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=108&id=u7f90a1ca&margin=%5Bobject%20Object%5D&name=image.png&originHeight=162&originWidth=501&originalType=binary&ratio=1&rotation=0&showTitle=false&size=6100&status=done&style=none&taskId=u20a858f3-5d91-49c7-8c08-3fe8c7e586f&title=&width=334)
> ls -1 就是将目录每行显示一个，也就是打印一个就换个行。

我们使用ls -l可以打印出文件的很多属性下面就是打印出来的信息分别对应的是什么意思
> ls -l或者是ll显示的内容的分类分区

   文件属性(占10个字符空间)       文件数      拥有者       所属的group      文件大小        建档日期              文件名
           drwx------                            2              Guest              users               1024              Nov 21 21:05       Mail 
           -rwx--x--x                            1               root                root                  89080           Nov 7 22:41         tar* 
           -rwxr-xr-x                             1               root                bin                    5013             Aug 15 9:32         uname* 
           lrwxrwxrwx                          1               root                root                  4                    Nov 24 19:30       zcat->gzip 
           -rwxr-xr-x                             1               root                bin                    308364         Nov 29 7:43         zsh* 
           -rwsr-x---                             1               root                bin                    9853              Aug 15 5:46          su*
           -rw-r--r--                              1               Hhf                 197121             146                10月 18 17:37       main.m

这里既然谈到了文件就补充一些文件相关的信息：
> 在Linux下有一个设计哲学就是，一切皆文件
> 我们刚刚敲的指令都是文件，甚至显示器，键盘，磁盘中的文件和磁盘都是文件。

> 那么我们该如何理解文件？
> 空文件占不占空间呢？
> 答案是空文件也会占用磁盘空间的。因为我们刚刚使用ll打印的时候可以看到文件都有很多的属性，这些属性都是要占用磁盘空间来保存的。
> 文件的大小 = 文件内容数据 + 文件属性数据
> 所以我们操作文件就有两种方式：1. 操作文件的内容 2. 操作文件的属性

> 关于文件的属性，Linux的文件基本属性有三个：w(可读)，r(可写)，x(可执行) 更多的属性在后面权限的章节会学到。

### pwd指令
> **语法**: pwd 
> **功能**：显示用户当前所在的目录 
> **常用选项**：无

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665376218692-4877bc45-6013-4e01-812b-40f5da895b3b.png#clientId=u3ed62342-cc95-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=46&id=u2f300c2c&margin=%5Bobject%20Object%5D&name=image.png&originHeight=69&originWidth=613&originalType=binary&ratio=1&rotation=0&showTitle=false&size=5571&status=done&style=none&taskId=ubbe7265a-fa8e-4b20-a0a9-86c204b24c5&title=&width=408.6666666666667)
> 显示了当前所在的目录的绝对路径。

> **路径分隔符相关：**
> 我们使用pwd打印出来的指令都是用一个\分隔开的。这个斜杠就叫做路径分隔符' \ '。
> Windows中的分隔符是' / '
> **D:\VSCode\test_markdown\DS集合**
> 上面这一段就是win下的一个路径地址。

### cd指令
> **语法**:    cd 目录名 
> **功能**：改变工作目录。将当前工作目录改变到指定的目录下。
> **具体指令**演示：
> cd ..  : 返回上级目录 
> cd  /home/litao/linux/ : 通过绝对路径访问 
> cd  ../day02/ : 通过相对路径访问 
> cd ~：进入当前用户的工作目录 
> cd -：返回最近访问目录

> 我们在上面提到过的隐藏文件中两个点代表上级目录，我们的cd就是改变当前的所处的工作目录，所以我们可以通过cd ..跳到上级路径
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665576390758-bc30be02-529b-40b8-8994-7fb0f699f849.png#clientId=u8dee7beb-38aa-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=253&id=ub355f4d6&margin=%5Bobject%20Object%5D&name=image.png&originHeight=380&originWidth=618&originalType=binary&ratio=1&rotation=0&showTitle=false&size=31211&status=done&style=none&taskId=ub0d3e113-4296-4eb6-81c0-389cce556ef&title=&width=412)
> 可以看到这里我们一路向上级目录跳转最后到了根目录。

> Linux下的目录结构
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665576489443-20b922a8-6f97-4b17-8bab-6e6ad70b19eb.png#clientId=u8dee7beb-38aa-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=219&id=u30d9075f&margin=%5Bobject%20Object%5D&name=image.png&originHeight=328&originWidth=974&originalType=binary&ratio=1&rotation=0&showTitle=false&size=72957&status=done&style=none&taskId=u5232b98d-b9f2-4421-b767-1e3e6f93f0f&title=&width=649.3333333333334)
> Linux下的目录结构是一课多叉树。win我们很熟悉有C盘D盘E盘等等，每个盘都是一颗多叉树，所以实际win下面的文件目录结构是一个森林。但是都是树形结构

> 那么为什么文件系统要使用属性结构来构造呢？**便于查找，每个文件都有唯一的表示方式**，因为在树形结构中每个节点都只有一个父节点，所以从下到上只有一条路径，那么从上向下找也就只有一条路径了。保证了路径的唯一性，这个就是绝对路径。
> 绝对路径写起来比较麻烦但是绝对路径不管我们所处在那个目录下都是有效的。相对路径就不行了。相对路径是相对于当前文件来讲的。一旦我们不在当时的路径下相对路径就失效了。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665576903895-ed7856b4-c235-4b9d-9593-68491fc41b91.png#clientId=u8dee7beb-38aa-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=73&id=ud8a36f07&margin=%5Bobject%20Object%5D&name=image.png&originHeight=109&originWidth=760&originalType=binary&ratio=1&rotation=0&showTitle=false&size=10264&status=done&style=none&taskId=u20977578-749e-45f3-a0c2-81862f990e3&title=&width=506.6666666666667)
> 绝对路径的跳转。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665576998724-cd9b0cc5-623e-4d3d-8cd6-8f8acc8f342c.png#clientId=u8dee7beb-38aa-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=110&id=u6d45f415&margin=%5Bobject%20Object%5D&name=image.png&originHeight=165&originWidth=636&originalType=binary&ratio=1&rotation=0&showTitle=false&size=14739&status=done&style=none&taskId=u0bc8d1d5-2c34-4f8c-906a-61babf868ef&title=&width=424)
> 相对路径的跳转，code下有两个目录分别是test和test1.我们想从test1跳转到test就可以使用相对路径。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665577077513-5afe6db4-bfe0-4cda-ae91-1cef9238888e.png#clientId=u8dee7beb-38aa-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=105&id=u4592d408&margin=%5Bobject%20Object%5D&name=image.png&originHeight=157&originWidth=547&originalType=binary&ratio=1&rotation=0&showTitle=false&size=12840&status=done&style=none&taskId=udbfa8551-f61c-496f-9b84-90af9066efa&title=&width=364.6666666666667)
> cd ~可以直接跳转到当前用户的工作目录
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665577131155-221a5d97-3aa7-4cb1-a794-28c0ca03f385.png#clientId=u8dee7beb-38aa-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=169&id=u8403d36f&margin=%5Bobject%20Object%5D&name=image.png&originHeight=253&originWidth=550&originalType=binary&ratio=1&rotation=0&showTitle=false&size=18238&status=done&style=none&taskId=u82bfbe1f-67ec-4b0d-95d2-4845551f001&title=&width=366.6666666666667)
> cd - 反复横跳

### touch命令
> **语法**:touch [选项]... 文件... 
> **功能**：touch命令参数可更改文档或目录的日期时间，包括存取时间和更改时间，或者新建一个不存在的文件。 
> **常用选项**：
> -a   或--time=atime或--time=access或--time=use只更改存取时间。 
> -c   或--no-create  不建立任何文档。 
> -d  使用指定的日期时间，而非现在的时间。 
> -f  此参数将忽略不予处理，仅负责解决BSD版本touch指令的兼容性问题。 
> -m   或--time=mtime或--time=modify  只更改变动时间。 
> -r  把指定文档或目录的日期时间，统统设成和参考文档或目录的日期时间相同。 
> -t  使用指定的日期时间，而非现在的时间

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665577305727-5d4861e9-97bf-4533-84fc-ef31fa7f9f2d.png#clientId=u8dee7beb-38aa-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=171&id=u0759a28b&margin=%5Bobject%20Object%5D&name=image.png&originHeight=257&originWidth=700&originalType=binary&ratio=1&rotation=0&showTitle=false&size=20311&status=done&style=none&taskId=u0b7c7d1e-6a51-4373-8c81-95f44852bad&title=&width=466.6666666666667)
> touch已存在文件会更新文件的三个时间。
> 如何查看一个文件更加详细的信息呢？我们可以使用stat命令
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665577519520-8e8af0d7-260a-4997-8c6d-453c06d9f8ca.png#clientId=u8dee7beb-38aa-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=418&id=u3a735251&margin=%5Bobject%20Object%5D&name=image.png&originHeight=627&originWidth=1177&originalType=binary&ratio=1&rotation=0&showTitle=false&size=66441&status=done&style=none&taskId=ub50605ed-fc7c-4ddf-8946-b2e147f661e&title=&width=784.6666666666666)
> touch之后文件的三个时间都被更改了。

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665577387478-2545d07d-129a-4a5c-84c2-ab3de7249153.png#clientId=u8dee7beb-38aa-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=254&id=ud0fc53c3&margin=%5Bobject%20Object%5D&name=image.png&originHeight=381&originWidth=843&originalType=binary&ratio=1&rotation=0&showTitle=false&size=37104&status=done&style=none&taskId=u12fdc4d8-3d21-495a-a29e-a77b6059467&title=&width=562)
> touch 加上文件名和后缀就可以直接创建出文件。

### mkdir指令
> **语法**：mkdir [选项] dirname... 
> **功能**：在当前目录下创建一个名为 “dirname”的目录 
> **常用选项**：
> -p 直接递归式创建多个目录构成一整条路径

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665577705461-4bc72f65-a1e5-4b19-a358-6cbd089479ed.png#clientId=u8dee7beb-38aa-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=210&id=u80cd7e61&margin=%5Bobject%20Object%5D&name=image.png&originHeight=315&originWidth=774&originalType=binary&ratio=1&rotation=0&showTitle=false&size=33550&status=done&style=none&taskId=u002f621c-be4c-49b4-a550-6506a6d9012&title=&width=516)
> 直接创建一个空目录。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665577780092-e5fefd32-b67b-456d-877d-2503a019b835.png#clientId=u8dee7beb-38aa-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=359&id=ua14ff966&margin=%5Bobject%20Object%5D&name=image.png&originHeight=538&originWidth=936&originalType=binary&ratio=1&rotation=0&showTitle=false&size=27102&status=done&style=none&taskId=ufc81cca9-b9eb-408b-bfcb-04a12d25446&title=&width=624)
> -p 创建了一整条路径。

> 这里的tree命令可以按照树形结构打印出当前目录下的所有文件和目录。
> tree命令是需要下载的，下面是下载命令(必须使用root用户才可以下载)

```c
yum install -y tree
```
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665577928918-f88f73de-6732-4fed-a663-4b7ea26414ea.png#clientId=u8dee7beb-38aa-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=149&id=u70881d7f&margin=%5Bobject%20Object%5D&name=image.png&originHeight=224&originWidth=608&originalType=binary&ratio=1&rotation=0&showTitle=false&size=9269&status=done&style=none&taskId=ufd357c86-6685-4a48-9610-1fba59583bd&title=&width=405.3333333333333)
> tree命令后面可以跟上想打印的目录。指定只打印这一个目录。

### rmdir指令
> **语法**：rmdir [-p][dirName] 
> **适用对象**：具有当前目录操作权限的所有使用者 
> **功能**：删除空目录
> 这个指令只能删除掉空目录。
> rmdir -p 可以递归删除，如果子目录被删除之后父目录变成空目录的话，就会连带着父目录一起删除。

### rm指令
> **rm命令可以同时删除文件或目录 **
> **语法**：rm [-f-i-r-v][dirName/dir] 
> **适用对象**：所有使用者 
> **功能**：删除文件或目录
> **常用选项**：
> -f 即使文件属性为只读(即写保护)，亦直接删除 
> -i 删除前逐一询问确认 
> -r 删除目录及其下所有文件
> *是通配符，会删除当前目录下的所有普通文件。

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665578619670-26a66d3b-2ca0-4cd1-86c8-7a6616e8369d.png#clientId=u8dee7beb-38aa-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=276&id=uc0df41b9&margin=%5Bobject%20Object%5D&name=image.png&originHeight=414&originWidth=794&originalType=binary&ratio=1&rotation=0&showTitle=false&size=41979&status=done&style=none&taskId=uc35d9d36-990a-4414-813b-49491867b49&title=&width=529.3333333333334)
> rm直接删除一个不是空目录的目录是删除不掉的，必须使用-r进行递归删除，删除的时候系统会自动询问我们是否要执行这次删除，y或者n。当然如果不想看到询问可以直接将r和f组合，-rf就是递归强制删除。
> 如果递归删除多个文件的时候需要系统一一询问我们，就可以加上-i 选项

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665578953554-9986a185-40ad-4ef5-8a25-5ad085a2bdab.png#clientId=u8dee7beb-38aa-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=444&id=ubc574595&margin=%5Bobject%20Object%5D&name=image.png&originHeight=666&originWidth=820&originalType=binary&ratio=1&rotation=0&showTitle=false&size=67205&status=done&style=none&taskId=u7393b82a-43f8-4681-bab3-186e6eb6609&title=&width=546.6666666666666)
> 这里介绍一种删除方式 rm * ，这个*是一个通配符，这句指令的意思是，删除当前目录下的所有普通文件。

### man指令
> Linux下的指令有很多，选项也是很多我们不可能都记住，所以这时候我们就需要查询手册，man就是这个手册。

> man **语法**: man [选项] 命令
> **常用选项**
> -k 根据关键字搜索联机帮助 
> num 只在第num章节找 
> -a 将所有章节的要查找的命令都显示出来，从第一个手册开始向后查找。

> man一共有8个手册
> 1 是普通的命令 
> 2 是系统调用,如open,write之类的(通过这个，至少可以很方便的查到调用这个函数，需要加什么头文 
> 件) 
> 3 是库函数,如printf,fread4是特殊文件,也就是/dev下的各种设备文件 
> 5 是指文件的格式,比如passwd, 就会说明这个文件中各个字段的含义 
> 6 是给游戏留的,由各个游戏自己定义 
> 7 是附件还有一些变量,比如向environ这种全局变量在这里就有说明 
> 8是系统管理用的命令,这些命令只能由root使用,如ifconﬁg

> man指令使用前需要下载。

```c
yum install -y man-pages
```
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665579363880-2d83f572-dfc0-4666-8d24-2313b13d601a.png#clientId=u8dee7beb-38aa-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=133&id=u52a2baab&margin=%5Bobject%20Object%5D&name=image.png&originHeight=199&originWidth=1242&originalType=binary&ratio=1&rotation=0&showTitle=false&size=19210&status=done&style=none&taskId=ue22653da-f797-47e5-973d-aa099f338a6&title=&width=828)
> ctrl + D可以跳过当前手册查找，ctrl+C直接终止。

### cp指令
> **语法**：cp [选项] 源文件或目录目标文件或目录 
> **功能**: 复制文件或目录 
> **说明**: cp指令用于复制文件或目录，如同时指定两个以上的文件或目录，且最后的目的地是一个已经存在的目录，
> 则它会把前面指定的所有文件或目录复制到此目录中。若同时指定多个文件或目录，而最后的目的地并非一个已存
> 在的目录，则会出现错误信息 
> **常用选项：**
> -f 或 --force 强行复制文件或目录，不论目的文件或目录是否已经存在 
> -i 或 --interactive 覆盖文件之前先询问用户 
> -r递归处理，将指定目录下的文件与子目录一并处理。若源文件或目录的形态，不属于目录或符号链
> 接，则一律视为普通文件处理 
> -R 或 --recursive递归处理，将指定目录下的文件及子目录一并处理

> cp 【选项】src   des从源到目的地
> 如果我们直接使用cp只能拷贝空目录。非空目录会拷贝失败
> 想要将目录下的文件都拷贝过去需要使用-r递归式拷贝。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665579794949-a7cb9099-74aa-4123-89c6-4716dfa39db9.png#clientId=u8dee7beb-38aa-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=584&id=ueb6fc9ff&margin=%5Bobject%20Object%5D&name=image.png&originHeight=876&originWidth=757&originalType=binary&ratio=1&rotation=0&showTitle=false&size=40101&status=done&style=none&taskId=u61aa092a-8d2f-4421-a2c1-a804e902772&title=&width=504.6666666666667)

> 当然如果目的地下有相同的文件，系统就会询问是否将这些文件覆盖，如果不想系统询问直接加上-f即可。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665580054391-631beab1-24d8-4e35-9e3b-25ced3cbba15.png#clientId=u8dee7beb-38aa-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=84&id=u61ad86ef&margin=%5Bobject%20Object%5D&name=image.png&originHeight=126&originWidth=788&originalType=binary&ratio=1&rotation=0&showTitle=false&size=13823&status=done&style=none&taskId=udd0d7edd-0b80-4b50-b10b-bf9c946ea25&title=&width=525.3333333333334)

> cp -i 在遇到同名文件的时候让系统主动询问是否覆盖

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665580281922-58728225-0f03-4e21-9e7b-d4154ed5997c.png#clientId=u8dee7beb-38aa-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=480&id=u7cd15910&margin=%5Bobject%20Object%5D&name=image.png&originHeight=720&originWidth=827&originalType=binary&ratio=1&rotation=0&showTitle=false&size=36348&status=done&style=none&taskId=ud5e2657d-9381-46e8-965a-434b9aca57b&title=&width=551.3333333333334)
> cp配合通配符使用，将所有后缀为.txt的文件都拷贝到copy目录下。

### mv指令
> mv命令是move的缩写，可以用来移动文件或者将文件改名（move (rename) fifiles），是Linux系统下常用的命令，经常用来备份文件或者目录。
> **语法**: mv [选项] 源文件或目录 目标文件或目录 
> **功能**: 
> 1. 视mv命令中第二个参数类型的不同（是目标文件还是目标目录），mv命令将文件重命名或将其移至一个新的目录中。 
> 2. 当第二个参数类型是文件时，mv命令完成文件重命名，此时，源文件只能有一个（也可以是源目录名），它将所给的源文件或目录重命名为给定的目标文件名。 
> 3. 当第二个参数是已存在的目录名称时，源文件或目录参数可以有多个，mv命令将各参数指定的源文件均移至目标目录中。
> **常用选项：**
> -f ：force 强制的意思，如果目标文件已经存在，不会询问而直接覆盖
> -i ：若目标文件 (destination) 已经存在时，就会询问是否覆盖！

> mv指令实际就是像是win里面的剪切指令。

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665662629868-878bd2d3-effc-416b-9fa4-db63e2d9eef1.png#clientId=u31be43b0-f529-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=581&id=ueddfcbed&margin=%5Bobject%20Object%5D&name=image.png&originHeight=872&originWidth=786&originalType=binary&ratio=1&rotation=0&showTitle=false&size=45967&status=done&style=none&taskId=ufa28949f-46f5-4158-8c77-b3e1055eaa6&title=&width=524)
> 这里我们使用了mv指令将整个code目录连同子文件以及子目录移动到了dir目录下。

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665662693545-875dc890-4b94-45d0-accc-f6c3115a77a0.png#clientId=u31be43b0-f529-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=131&id=u2aa69a6a&margin=%5Bobject%20Object%5D&name=image.png&originHeight=197&originWidth=793&originalType=binary&ratio=1&rotation=0&showTitle=false&size=20602&status=done&style=none&taskId=u049cec6c-4bc1-46ba-accf-9faea1c9f4a&title=&width=528.6666666666666)
> mv可以直接将dir目录重命名。当然如果是文件也是类似的。

> mv还可以一次移动多个文件，多个文件之中使用空格隔开，一同移动到最后的目录中。

### cat指令
> **语法**：cat [选项][文件] 
> **功能**：查看目标文件的内容
> **常用选项**：
> -b 对非空输出行编号 
> -n 对输出的所有行编号
> -s 不输出多行空行

> cat命令的主要作用就是用来查看文件或者代码。

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665662929819-ac38ff75-c011-4c56-8498-7834e55e1f8d.png#clientId=u31be43b0-f529-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=282&id=u9c427383&margin=%5Bobject%20Object%5D&name=image.png&originHeight=423&originWidth=765&originalType=binary&ratio=1&rotation=0&showTitle=false&size=22409&status=done&style=none&taskId=uc8beb240-7a22-41a6-a549-aa23deb478e&title=&width=510)
> 这里我们创建了一个文件然后使用cat查看其内容。

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665662973615-3153a5b5-50f1-496b-9927-f382a7f1c29a.png#clientId=u31be43b0-f529-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=261&id=u52faad44&margin=%5Bobject%20Object%5D&name=image.png&originHeight=391&originWidth=755&originalType=binary&ratio=1&rotation=0&showTitle=false&size=17576&status=done&style=none&taskId=u1fb9f7e9-dd2b-4536-9cf6-edff669ed89&title=&width=503.3333333333333)
> 带行号显示。

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665663069151-6e42838d-f77f-49e6-a6a7-0d98eeca6a5c.png#clientId=u31be43b0-f529-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=330&id=u45c11cf1&margin=%5Bobject%20Object%5D&name=image.png&originHeight=495&originWidth=746&originalType=binary&ratio=1&rotation=0&showTitle=false&size=17492&status=done&style=none&taskId=ub415f802-9747-447c-89ce-fd33fbf63e1&title=&width=497.3333333333333)
> 只对非空行进行编号

### more指令
> **语法**：more [选项][文件] 
> **功能**：more命令，功能类似 cat 
> **常用选项**：
> -num 一次显示的行数
> q 退出more 

> more指令也是一个查看文件的指令，但是这个指令查看文件只能使用enter向下翻页不能向上翻页。

> 为了方便演示more命令我们现在需要一个大文件，最好是上万行的文件，那么我们如何生成一个大文件呢？当然我们可以选择使用shell脚本。但是也可以使用程序生成最后输出重定向到文件中。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665663821273-e9aa54ad-0e4c-45ac-b1bc-4a2bc8f5660d.png#clientId=u31be43b0-f529-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=357&id=ub50e0518&margin=%5Bobject%20Object%5D&name=image.png&originHeight=536&originWidth=853&originalType=binary&ratio=1&rotation=0&showTitle=false&size=19532&status=done&style=none&taskId=u52a8c00d-0c26-4423-bb09-f6ee302bfa3&title=&width=568.6666666666666)
> 通过这个代码我们可以生成10000行字符串。我们编译后生成了a.out我们运行的时候字符串会直接默认打印在屏幕上。现在我们想要字符串打印到文件里面就可以使用输出重定向符号，就是一个箭头 > 

```c
[root@VM-8-12-centos code]# ./a.out > t1.txt
```
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665663991167-5c4ea7b6-c49b-485b-8bfe-e23de411a2d6.png#clientId=u31be43b0-f529-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=753&id=ued4595e6&margin=%5Bobject%20Object%5D&name=image.png&originHeight=1130&originWidth=543&originalType=binary&ratio=1&rotation=0&showTitle=false&size=60703&status=done&style=none&taskId=u2ff2b857-523b-4f5e-9d7f-1c23bb151c9&title=&width=362)
> 这是more的查看界面，最后一行表示我们查看了该文件的多少。 

### less指令
> **语法：** less [参数] 文件 
> **功能**： 
> less的功能和more类似只是less的功能比起more要强大不少
> less支持使用上下键翻页，并且支持搜索。而且less在查看之前不会加载整个文件。 
> **选项**：
> -i  忽略搜索时的大小写 
> -N  显示每行的行号 
> /字符串：向下搜索“字符串”的功能 
> ?字符串：向上搜索“字符串”的功能 
> n：重复前一个搜索（与 / 或 ? 有关，在less打开的文件中输入） 
> N：反向重复前一个搜索（与 / 或 ? 有关）
> q:quit 

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665666636052-33345d3a-4f13-4673-a047-44a69c42e052.png#clientId=u31be43b0-f529-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=595&id=udb0be08a&margin=%5Bobject%20Object%5D&name=image.png&originHeight=893&originWidth=614&originalType=binary&ratio=1&rotation=0&showTitle=false&size=50839&status=done&style=none&taskId=u23d82ef1-e930-4420-87fa-38686761c6c&title=&width=409.3333333333333)
> 带行号输出。

### head指令
> **语法：** head [参数]... [文件]...  
> **功能**： 
> head就是从头开始显示文件内容，默认head命令打印其相应文件的开头10行。  
> **选项：**
> -n<行数> 显示的行数

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665666747291-01664d74-4326-4b54-9c9f-c2e08b77938c.png#clientId=u31be43b0-f529-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=229&id=ucbda57a8&margin=%5Bobject%20Object%5D&name=image.png&originHeight=344&originWidth=706&originalType=binary&ratio=1&rotation=0&showTitle=false&size=20330&status=done&style=none&taskId=uf6cee3d4-434a-4fa8-ad4b-2b9f54a0118&title=&width=470.6666666666667)
> 默认显示十行

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665666871942-c68b435f-a7c3-4c21-9a47-fe162923c1d5.png#clientId=u31be43b0-f529-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=335&id=udd1e11d3&margin=%5Bobject%20Object%5D&name=image.png&originHeight=502&originWidth=759&originalType=binary&ratio=1&rotation=0&showTitle=false&size=29025&status=done&style=none&taskId=u2aa4f097-e011-4619-963f-ff6990857a8&title=&width=506)

### tail指令
> **语法：** tail[必要参数][选择参数][文件]  
> **功能：**用于显示指定文件末尾内容，不指定文件时，作为输入信息进行处理。常用查看日志文件。 
> **选项：**
> -f 循环读取 
> -n<行数> 显示行数

> 这里如果我们想要查看刚刚那个一万行文件的第1000到1010行应该怎么写呢？
> 简单介绍一个概念就是管道（符号是逻辑或的符号'  |  '）管道就是用来传递数据的。因为在Linux下一切皆文件，文件中最重要的就是数据。

```cpp
[root@VM-8-12-centos code]# head -1010 test.txt |tail -10
```
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665667335221-64de3516-9582-4a8b-9ce6-9054f3996146.png#clientId=u31be43b0-f529-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=233&id=u74f523d1&margin=%5Bobject%20Object%5D&name=image.png&originHeight=350&originWidth=918&originalType=binary&ratio=1&rotation=0&showTitle=false&size=25582&status=done&style=none&taskId=uff3ba6a0-9ee3-4a4e-826f-2036cc13037&title=&width=612)
这个就是使用管道将test.txt的前1010行数据传递tail然后用tail取得其后十行。通过管道将数据传递链接起来又叫做**级联**。
> 当我们使用cat指令的相反的指令tac的时候文件会按照行的逆序打印出来。

> 如果我们只插入一个cat指令不接文件那么默认，我们从键盘上输出的字符都会原样子打印出来。
> 这里我们可以想到在C语言这里编译器会默认打开三个文件，分别是：
> 标准输入（stdin）标准输出（stdout）标准错误（stderr）
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665667690975-607eac52-0b35-43ab-8095-f8ea752f556d.png#clientId=u31be43b0-f529-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=173&id=uaef706f6&margin=%5Bobject%20Object%5D&name=image.png&originHeight=260&originWidth=521&originalType=binary&ratio=1&rotation=0&showTitle=false&size=7140&status=done&style=none&taskId=u0ae7a531-1277-4245-a3df-38ee4e56883&title=&width=347.3333333333333)
> 使用Ctrl + C即可推出。

### 时间相关的指令
### date指令
> date 指定格式显示时间： date +%Y:%m:%d 
> date 用法：date [OPTION]... [+FORMAT] 
> **注意事项**：


> **1.date的使用类似于C语言的printf我们可以格式化的输出时间。只需要选择不同的选项即可。**
> 下面是常用的一些标记选项(注意前面要加个'+')。
> %H : 小时(00..23) 
> %M : 分钟(00..59) 
> %S : 秒(00..61) 
> %X : 相当于 %H:%M:%S 
> %d : 日 (01..31) 
> %m : 月份 (01..12) 
> %Y : 完整年份 (0000..9999) 
> %F : 相当于 %Y-%m-%d 
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665668742711-b54dbc99-f2e5-4356-8683-3e296335d33a.png#clientId=u31be43b0-f529-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=207&id=u4a7aadb4&margin=%5Bobject%20Object%5D&name=image.png&originHeight=311&originWidth=730&originalType=binary&ratio=1&rotation=0&showTitle=false&size=26067&status=done&style=none&taskId=u47344791-bdda-488d-9242-ff8b419ae42&title=&width=486.6666666666667)

> **2.date可以用来设定时间**
> date -s  //设置当前时间，只有root权限才能设置，其他只能查看。 
> date -s 20080523  //设置成20080523，这样会把具体时间设置成空00:00:00 
> date -s 01:01:01 //设置具体时间，不会对日期做更改 
> date -s “01:01:01 2008-05-23″ //这样可以设置全部时间 
> date -s “01:01:01 20080523″ //这样可以设置全部时间 
> date -s “2008-05-23 01:01:01″ //这样可以设置全部时间 
> date -s “20080523 01:01:01″ //这样可以设置全部时间

> **3.时间戳**
> 时间->时间戳：date +%s //这行命令可以得到当前时间转换成时间戳
> 时间戳->时间：date -d@1508749502 // 将时间戳转换成具体的时间
> Unix时间戳（英文为Unix epoch, Unix time, POSIX time 或 Unix timestamp）是从1970年1月1日（UTC/GMT午夜）开始所经过的秒数，不考虑闰秒。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665669261312-50b974d7-e09b-4115-8ce5-cf15076d98db.png#clientId=u31be43b0-f529-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=130&id=ud296794d&margin=%5Bobject%20Object%5D&name=image.png&originHeight=195&originWidth=764&originalType=binary&ratio=1&rotation=0&showTitle=false&size=19105&status=done&style=none&taskId=uf50b0517-6a48-4792-ac0c-2148f691efc&title=&width=509.3333333333333)
> 这里我们@了0，理论上应该显示的是1970年的一月一日零点零分的但是现在显示的是八点。主要是因为我的Linux机器处在东八区，所以显示的也是东八区的时间。

> 千年虫问题：
> 时间的存储在32位的Unix系统下最大就能到2147483647，因为它们的时间起点是格林尼治时间1970年1月1日0时0分0秒，依照此“time_t”标准，在此格式能被表示的最后时间是第2147483647秒（代表格林尼治时间2038年1月19日凌晨03:14:07）
> 所以到了2038年32位Unix系统下的时间就会溢出导致混乱。时间的混乱引起的后果是非常严重的。比如飞机航班的起飞时间的控制器底层都是一些嵌入式设备，这些设备大多都是32位的。不过不需要太担心，这些问题早已经被解决了。

### cal指令
> **命令格式**： cal [参数][月份][年份] 
> **功能**：用于查看日历等时间信息，如只有一个参数，则表示年份(1-9999)，如有两个参数，则表示月份和年份
> **常用选项**：
> -3 显示系统前一个月，当前月，下一个月的月历 
> -j  显示在当年中的第几天（一年日期按天算，从1月1号算起，默认显示当前月在一年中的天数）
> -y  显示当前年份的日历

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665669406217-b6a44288-e6ee-441e-bbfb-b375ddcb6ae3.png#clientId=u31be43b0-f529-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=188&id=u80de2f88&margin=%5Bobject%20Object%5D&name=image.png&originHeight=282&originWidth=1042&originalType=binary&ratio=1&rotation=0&showTitle=false&size=23676&status=done&style=none&taskId=uebafd66a-5908-4139-a072-e83bb2ce3bf&title=&width=694.6666666666666)

```c
cal -y 2018//可以查看2018年的日历
```
### find指令
> **语法：** ﬁnd pathname  -options  文件名
> **功能：**用于在文件树种查找文件，并作出相应的处理（可能访问磁盘）
> **常用选项：**
> -name   按照文件名查找文件。
> -size   按照文件的大小查找文件。

> Linux下ﬁnd命令在目录结构中搜索文件，并执行指定的操作。 
> 即使系统中含有网络文件系统( NFS)，ﬁnd命令在该文件系统中同样有效，只你具有相应的权限。 
> 在运行一个非常消耗资源的ﬁnd命令时，很多人都倾向于把它放在后台执行，因为遍历一个大的文件系
> 统可能会花费很长的时间(这里是指30G字节以上的文件系统)。

> find命令查找的时候如果在内存中找不到文件，会将不在内存中的目录load到内存中然后进行查找，所以find命令的效率不是很高。可以看做是一种非常暴力的查找方式。

find也可以在指定的目录中查找：
find [目录] -name 文件名
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665851108635-4c90cd4b-cf00-4ab3-b489-9887720845e8.png#clientId=u944f5b74-7bb5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=382&id=u7339b013&margin=%5Bobject%20Object%5D&name=image.png&originHeight=573&originWidth=903&originalType=binary&ratio=1&rotation=0&showTitle=false&size=27170&status=done&style=none&taskId=u8b0f646d-d8ed-4a86-8857-0cf967ee114&title=&width=602)
在家目录中查找t1.txt文件操作。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665851254968-41bdca38-8a6d-4537-9fbb-74739006b570.png#clientId=u944f5b74-7bb5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=391&id=u38d09f5b&margin=%5Bobject%20Object%5D&name=image.png&originHeight=587&originWidth=1041&originalType=binary&ratio=1&rotation=0&showTitle=false&size=79491&status=done&style=none&taskId=uea5765e3-e53e-45d1-93de-e1f31310237&title=&width=694)
这里的查找命令第一条是在~目录下查找文件大小是100 到110 字节的文件。查找结果是没找到。
第二条是在根目录下查找，找到就打印出文件的绝对路径。
### which指令
> **语法：** which  [查找的命令]
> **功能：**显式查找的指令文件所在的绝对路径。（which只能用来搜索命令）

![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665851529787-64b8fb9c-3b0f-4a86-b3fe-267f5e35961c.png#clientId=u944f5b74-7bb5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=174&id=ue33c62ff&margin=%5Bobject%20Object%5D&name=image.png&originHeight=261&originWidth=593&originalType=binary&ratio=1&rotation=0&showTitle=false&size=16991&status=done&style=none&taskId=u4f56694a-e11e-4fcc-b12d-10679b0440a&title=&width=395.3333333333333)
通过查找我们可以看到有的指令前面会打印出一行alias开头的字符，有的则是直接打印出该指令所在的路径。
> 这里可以看出我们使用的指令也是一个文件，就像我们写的程序一样，但是有一个不同的方面就是，在执行的时候，执行我们所写的程序的时候需要带上文件的路径，但是执行系统的指令文件的时候就不需要带路径了，这是为什么呢？这其实和Linux的环境变量有关。后面有机会会对于环境变量进行讲解。

这里的alias也是一条指令，是取别名指令。使用方法就是打印出来的这行字符，以ls为例子，ls就是将ls指令配合了自动显示颜色一起重命名成了ls指令。所以我们使用ls打印出来的文件是有各种不同的颜色的。 
没用经过alias重命名的文件就会直接打印出绝对路径。
### whereis指令
> **语法：** whereis  [关键字]
> **功能：**在特定路径下查找指定文件名对应的指令或者文档。

关于三条查找命令的查找范围，which < whereis < find
### grep指令
> **语法：** grep [选项]  搜寻字符串  文件 
> **功能：**在文件中搜索字符串，将找到的行打印出来
> **常用选项：**
> -i ：忽略大小写的不同，所以大小写视为相同 
> -n ：顺便输出行号 
> -v ：反向选择，亦即显示出没有 '搜寻字符串' 内容的那一行

![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665851941692-32af365f-6e0e-4123-834f-c7128cb1a38d.png#clientId=u944f5b74-7bb5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=169&id=ua4fc9117&margin=%5Bobject%20Object%5D&name=image.png&originHeight=254&originWidth=856&originalType=binary&ratio=1&rotation=0&showTitle=false&size=21740&status=done&style=none&taskId=u00f3192f-6059-4c45-816b-4098747ddb0&title=&width=570.6666666666666)
演示在文件中直接查找一个字符串，注意字符串使用单引号引起来。指令是可以组合起来的。-i忽略大小写。-n 打印出来行号。
### sort指令
> **语法：** sort  [文件]
> **功能：**将文件每行按照ASCII码值排序(不会改变原文件）

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665853060512-001c41ea-f46a-4901-b8cf-f3534a87ea37.png#clientId=u944f5b74-7bb5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=617&id=u11dd0801&margin=%5Bobject%20Object%5D&name=image.png&originHeight=925&originWidth=904&originalType=binary&ratio=1&rotation=0&showTitle=false&size=25804&status=done&style=none&taskId=ua4fe2694-cdeb-4ad5-9042-8a4b7f1a5ab&title=&width=602.6666666666666)

### uniq指令
> **语法：** uniq  [文件]
> **功能：**将相邻的相等的文本行进行去重
> 一般想要去重之前都需要先排序，排序可以使得相同的文本行相邻。

![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665853371757-aee61902-efad-4f75-bcaf-01d0060bfacc.png#clientId=u944f5b74-7bb5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=620&id=u519d6fbd&margin=%5Bobject%20Object%5D&name=image.png&originHeight=930&originWidth=807&originalType=binary&ratio=1&rotation=0&showTitle=false&size=14628&status=done&style=none&taskId=u52a2a15a-ae23-4597-be14-39e9925ee52&title=&width=538)
去重前
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665853360134-89853ffc-4c43-419e-a10a-350eaa162ecd.png#clientId=u944f5b74-7bb5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=215&id=u0d4a0741&margin=%5Bobject%20Object%5D&name=image.png&originHeight=322&originWidth=757&originalType=binary&ratio=1&rotation=0&showTitle=false&size=7500&status=done&style=none&taskId=u5ec2addd-dadf-4ac8-82d4-620ef6a7125&title=&width=504.6666666666667)
去重后
### zip/unzip指令
> **语法：** zip 压缩文件.zip  目录或文件
> **功能：**将目录或文件压缩成zip格式
> **常用选项：**
> -r 递归处理，将指定目录下的所有文件和子目录一并处理（如果不用递归方式，那么压缩目录的时候就会只压缩出一个空目录）
> 

> zip和unzip下载命令如下

```cpp
yum install -y zip unzip
```
> zip默认对一个目录进行打包的时候只会对目录文件进行压缩

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665854207917-b7c54f0a-a7ad-452d-83ce-6ec5ec2cd728.png#clientId=u944f5b74-7bb5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=255&id=u65e7ca53&margin=%5Bobject%20Object%5D&name=image.png&originHeight=382&originWidth=843&originalType=binary&ratio=1&rotation=0&showTitle=false&size=37210&status=done&style=none&taskId=u93e461c0-d805-4ffa-9064-57abc857a0a&title=&width=562)
> 使用递归方式对dz目录及其内部文件进行了压缩。

> 关于unzip解压指令默认就是在当前所处路径进行解压
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665854307845-57cb7cdb-15d0-4186-a215-b9e127aae5f3.png#clientId=u944f5b74-7bb5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=311&id=ub05ff81a&margin=%5Bobject%20Object%5D&name=image.png&originHeight=466&originWidth=809&originalType=binary&ratio=1&rotation=0&showTitle=false&size=36553&status=done&style=none&taskId=u76dee6e4-c0f6-499c-a701-e8cf9b91f40&title=&width=539.3333333333334)

> 当然也可指定在某个路径下解压。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1665854476064-67f0dece-9ff8-4d7d-96ed-3860fefabb07.png#clientId=u944f5b74-7bb5-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=108&id=u239cc3c9&margin=%5Bobject%20Object%5D&name=image.png&originHeight=162&originWidth=958&originalType=binary&ratio=1&rotation=0&showTitle=false&size=13743&status=done&style=none&taskId=u56475b8c-5cc9-46cd-8bd0-7489ae174de&title=&width=638.6666666666666)
> 如图所示-d之后，输入要解压到那个路径即可。

总结：为什么文件要打包和压缩？（文件丢失问题和网络传输问题）
1.为了把所有的东西都放到一起，方便保存，不存在部分文件丢失的情况。
2.压缩后的文件变小了，便于高效传输。
### tar指令
> **语法：** **tar [-cxtzjvf] 文件或者目录 ....参数**
> **功能：**将目录或文件压缩成tar格式
> **常用选项：**
> -c ：建立一个压缩文件的参数指令(create 的意思)； 
> -x ：解开一个压缩文件的参数指令！ 
> -t ：查看 tarﬁle 里面的文件！ 
> -z ：是否同时具有 gzip 的属性？亦即是否需要用 gzip 压缩？ 
> -j ：是否同时具有 bzip2 的属性？亦即是否需要用 bzip2 压缩？ 
> -v ：压缩的过程中显示文件！这个常用，但不建议用在背景执行过程！
> -f ：使用档名，请留意，在 f 之后要立即接档名喔！不要再加参数！ 
> -C ：解压到指定目录

![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1666089069949-58abdc57-714d-4825-846e-b27649a67a90.png#clientId=u35c42c7d-8144-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=212&id=u2fe406f3&margin=%5Bobject%20Object%5D&name=image.png&originHeight=318&originWidth=806&originalType=binary&ratio=1&rotation=0&showTitle=false&size=33911&status=done&style=none&taskId=u9566f611-f924-4b26-b1c9-35ccecd2ee2&title=&width=537.3333333333334)
我们直接使用组合指令压缩出一个my.tar文件在当前目录下。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1666089222938-28e6f127-5396-4e80-8d03-b58ccbe05001.png#clientId=u35c42c7d-8144-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=123&id=u9eb1c7e7&margin=%5Bobject%20Object%5D&name=image.png&originHeight=184&originWidth=651&originalType=binary&ratio=1&rotation=0&showTitle=false&size=10067&status=done&style=none&taskId=ua58c6526-1de3-489a-b9c5-176bbd36e9c&title=&width=434)
通过组合t和f指令可以不解压查看压缩文件里面的内容。这里的指令组合要注意，f一定要是最后一个指令，因为f后面不可以在接指令了。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1666089375509-1786ebe7-5125-4727-9344-28846bec319b.png#clientId=u35c42c7d-8144-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=338&id=ub6d04d8b&margin=%5Bobject%20Object%5D&name=image.png&originHeight=507&originWidth=893&originalType=binary&ratio=1&rotation=0&showTitle=false&size=45151&status=done&style=none&taskId=u1ed0ee03-0290-44c2-9e15-eda9010280e&title=&width=595.3333333333334)
加上v就可以看到压缩的过程中的文件。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1666089445031-1b31eebd-572c-4293-8113-41f321fe14a9.png#clientId=u35c42c7d-8144-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=143&id=ua80e9216&margin=%5Bobject%20Object%5D&name=image.png&originHeight=215&originWidth=870&originalType=binary&ratio=1&rotation=0&showTitle=false&size=14216&status=done&style=none&taskId=u991a1f61-7ce8-44c3-b4d0-a2290bf7714&title=&width=580)
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1666089479887-42debc22-cb05-4fc7-a413-662ac620ae80.png#clientId=u35c42c7d-8144-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=389&id=ued76aea6&margin=%5Bobject%20Object%5D&name=image.png&originHeight=583&originWidth=633&originalType=binary&ratio=1&rotation=0&showTitle=false&size=17095&status=done&style=none&taskId=u35810901-9337-4331-a81d-3515e1f1869&title=&width=422)
使用xzvf显示文件的同时解压，最后加上的-C是解压到指定路径下，当然如果不加那么默认就是在当前路径下解压。

### bc指令
> 计算器，可以进行浮点运算，quit退出，bc支持管道。

![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1666089673456-31579cdb-1b1e-401d-94c4-95d23d3c9e7c.png#clientId=u35c42c7d-8144-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=209&id=ua9d5176e&margin=%5Bobject%20Object%5D&name=image.png&originHeight=313&originWidth=1309&originalType=binary&ratio=1&rotation=0&showTitle=false&size=20347&status=done&style=none&taskId=u0dcd86d4-43e7-478a-b9e1-9f6b126803d&title=&width=872.6666666666666)
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1666090068847-d64341a0-50d2-4286-ae7a-521ee0322ac7.png#clientId=u35c42c7d-8144-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=103&id=u49ad0831&margin=%5Bobject%20Object%5D&name=image.png&originHeight=155&originWidth=648&originalType=binary&ratio=1&rotation=0&showTitle=false&size=10322&status=done&style=none&taskId=uf71a6279-5e58-43eb-80a9-6a8287fc5c2&title=&width=432)
利用echo显示出经过管道传送到bc里面的数据的运算结果。
### uname指令
> **语法**：uname [选项]  
> **功能**： uname用来获取电脑和操作系统的相关信息。 
> **补充说明**：uname可显示linux主机所用的操作系统的版本、硬件的名称等基本信息。
> **常用选项：**
> -a或–all 详细输出所有信息，依次为内核名称，主机名，内核版本号，内核版本，硬件名，处理器类
> 型，硬件平台类型，操作系统名称 
> -r 查看计算机的体系结构（cup架构），内核版本

![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1666090184133-17a526cf-de4d-4f41-a1d3-1bee3897cdda.png#clientId=u35c42c7d-8144-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=90&id=u349998ad&margin=%5Bobject%20Object%5D&name=image.png&originHeight=135&originWidth=1865&originalType=binary&ratio=1&rotation=0&showTitle=false&size=17073&status=done&style=none&taskId=u23c6960d-dcc5-41e3-993f-f90d487c6ba&title=&width=1243.3333333333333)
这里的x86_64就是64位机，简写成x64也代表64位机。
### 常用快捷键
> [Tab]按键---具有『命令补全』和『档案补齐』的功能 
> [Ctrl]-c按键---让当前的程序『停掉』 
> [Ctrl]-d按键---通常代表着：『键盘输入结束(End Of File, EOF 戒 End OfInput)』的意思；另外，他也可
> [Ctrl]-r按键---搜索历史指令，通过左右方向键进行选定
> 以用来取代exit

> ls \这里的反斜杠是一个续航符，我们需要在下一行继续书写ls的指令选项。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1666090415690-f3ecdd2a-b67f-4ab7-a5e8-557796341a68.png#clientId=u35c42c7d-8144-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=563&id=ua982d76e&margin=%5Bobject%20Object%5D&name=image.png&originHeight=845&originWidth=971&originalType=binary&ratio=1&rotation=0&showTitle=false&size=87059&status=done&style=none&taskId=uc501b74b-2543-4b13-aa4c-37bb10f3237&title=&width=647.3333333333334)

### 常用查看指令
> lscpu 
> ls直接加上cpu就可以查看，计算机的cpu信息
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1666090516545-738c80f4-a3d7-464a-90e9-b413a9d5fe4e.png#clientId=u35c42c7d-8144-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=562&id=ua325036e&margin=%5Bobject%20Object%5D&name=image.png&originHeight=843&originWidth=1382&originalType=binary&ratio=1&rotation=0&showTitle=false&size=69834&status=done&style=none&taskId=u00b76010-c080-4e29-ba18-6f9eda7e5ce&title=&width=921.3333333333334)

> lsmem查看内存情况
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1666090544368-189abb29-fa16-4643-9f3a-4c599b04dc77.png#clientId=u35c42c7d-8144-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=275&id=u08d7bb80&margin=%5Bobject%20Object%5D&name=image.png&originHeight=412&originWidth=1082&originalType=binary&ratio=1&rotation=0&showTitle=false&size=35902&status=done&style=none&taskId=uf9d74c9d-5886-40a7-be03-e0f5eea7fde&title=&width=721.3333333333334)

> df - h
> 用来查看磁盘的使用请况
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1666090600496-f2b6f3d4-c1fa-43c2-a480-5168673339e0.png#clientId=u35c42c7d-8144-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=175&id=ua3ed84a3&margin=%5Bobject%20Object%5D&name=image.png&originHeight=263&originWidth=851&originalType=binary&ratio=1&rotation=0&showTitle=false&size=24698&status=done&style=none&taskId=u104b85a2-f7d9-49f7-8dbb-79799084c1d&title=&width=567.3333333333334)

> who命令，查看当前服务器在线的人信息
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1666090657834-2aaa083b-2cc0-4495-be86-66e36393d85a.png#clientId=u35c42c7d-8144-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=69&id=u0b1b922a&margin=%5Bobject%20Object%5D&name=image.png&originHeight=104&originWidth=870&originalType=binary&ratio=1&rotation=0&showTitle=false&size=10256&status=done&style=none&taskId=u2a462cbb-f2e3-4424-879e-3c20a2732b4&title=&width=580)

上面的指令就是几乎Linux所有常用的指令了。


