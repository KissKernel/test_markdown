## 基本的具体用户的认识
Linux下有两种基本用户，分别是root用户(超级用户)，和普通用户。root用户是不受权限约束的，普通用户会受到权限的约束。
在我们登录Linux云服务器后可以发现，在命令行提示中，root用户是'#'，普通用户是'$'
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667306789347-717a6bbd-5134-47dd-b870-13376ba34b1e.png#averageHue=%2314110f&clientId=u3df7ef7c-43e8-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=48&id=ue946fe08&margin=%5Bobject%20Object%5D&name=image.png&originHeight=72&originWidth=501&originalType=binary&ratio=1&rotation=0&showTitle=false&size=6100&status=done&style=none&taskId=uceca1a53-63b9-46e9-bb5c-48b2814d081&title=&width=334)
关于用户切换的指令，使用的是su指令。
### su指令
> 语法格式：su [要切换到的用户名]
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667307144605-5b86c5d2-3a4d-479d-8449-204d87c04ea2.png#averageHue=%230e0c0b&clientId=u3df7ef7c-43e8-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=43&id=ub31cd2cc&margin=%5Bobject%20Object%5D&name=image.png&originHeight=65&originWidth=579&originalType=binary&ratio=1&rotation=0&showTitle=false&size=4675&status=done&style=none&taskId=u53b04da2-ffd9-4a32-b89e-921913d4a71&title=&width=386)
> 从普通用户切换到root用户需要输入root用户的密码。
> 从root用户切换到普通用户不需要任何密码。

直接使用su切换用户后，我们当前所处的路径还是切换用户前所处的路径，如果想要切换用户后路径也跟着改变，可以使用su - 这条指令输入root密码之后会切换成root用户同时跳转到root的工作目录。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667307316759-fbe62abd-7ddb-465d-9c17-943890a537aa.png#averageHue=%230a0808&clientId=u3df7ef7c-43e8-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=149&id=u067d6d63&margin=%5Bobject%20Object%5D&name=image.png&originHeight=224&originWidth=827&originalType=binary&ratio=1&rotation=0&showTitle=false&size=17407&status=done&style=none&taskId=uc6329b25-e699-415a-ba86-6017fc5b9e7&title=&width=551.3333333333334)
如果我们想要回到刚刚切换过来的目录，比如我们一开始是xzj，然后切换到了root，现在想要切换会xzj，有两种指令：1. exit指令。 2. ctrl + d 这样都可以退回到上一个用户。
### sudo指令
如果我们不想切换到root用户，但是想使用root的权限执行一条指令，那么就可以在这条指令的前面加上sudo。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667307548501-c15292f3-7043-465f-916f-c550f5b3ddb4.png#averageHue=%230d0c0a&clientId=u3df7ef7c-43e8-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=66&id=ua6f8e7d2&margin=%5Bobject%20Object%5D&name=image.png&originHeight=99&originWidth=584&originalType=binary&ratio=1&rotation=0&showTitle=false&size=6570&status=done&style=none&taskId=u907bbec2-d0e3-474c-abcf-6e122fce6bd&title=&width=389.3333333333333)
我们是xzj用户，使用sudo提权后whoami这条指令就是以root用户的身份执行的。
> 这里首选有一个问题，我们要提权到root用户为什么要输入我们当前账户的密码？这是因为如果想要使用sudo需要将xzj用户添加到/etc/sudoers这个文件中，这个文件类似于我们手机上的白名单，名单上的用户是我们信任的用户，**Linux为了给信任的用户提供最少的执行障碍**，所以输入的就是xzj用户的密码。

并且我们使用sudo提权某条指令，输入了密码之后，在后面的十几分钟或者五六分钟之内再次使用sudo是不需要再输入密码的。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667307897999-44298c45-196a-41fb-9626-6ac00e34c96e.png#averageHue=%230d0b0a&clientId=u3df7ef7c-43e8-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=276&id=u5756a3df&margin=%5Bobject%20Object%5D&name=image.png&originHeight=414&originWidth=591&originalType=binary&ratio=1&rotation=0&showTitle=false&size=27934&status=done&style=none&taskId=u33bb5373-9e2a-4fdf-bd9a-ec7f593e9bb&title=&width=394)
## 权限的周边概念
### 什么是权限（是什么）
> 使用一个例子来介绍权限，比如我们平时在使用爱奇艺看视频的时候，有些视频只有会员才能看，如果你不是会员那么你就被约束了不能看这个视频。

所以说这里的权限的组成有两部分，角色和对象，首先是如果一个对象不具有某种属性那么不管是什么角色都不会有这个权限，比如爱奇艺没有玩游戏的属性，所以任何角色都不能在爱奇艺上玩游戏，任何角色都没有这个权限。只有当某个对象本身具有某种属性，某个角色才会被允许拥有某种权限。
> 权限是约束某种角色的一般是一个人或者一个群体。

> 某种行为允许被谁做？这的谁是指某个人或者某个群体，做是代表对象的某一种属性。

文件权限 = 角色 + 文件属性
> 在Linux中有三种角色：
> 1. 文件拥有者（owner）
> 2. 所属组（grouper）
> 3. 其他人（other）

Linux是个多用户系统，所以每个用户都有一个角色身份，对应着上面的三种角色。
> 文件的拥有者和其他人概念都很清楚，谁创建的文件，谁就是文件的拥有者，除了创建者之外其他的都是其他人，但是这里的其他人有一部分被划分到了所属组。那么什么是所属组呢？
> 比如：现在这里有一个项目需要完成，两个团队进行竞争，团队将代码都放在一个Linux机器上，这时候就出现了两个小组，这两个小组写的代码应该是可以被组内的人看到，但是不能被组外的人看到，由此就产生了所属组的概念，只是这里的所属组只能放一个人，设置某个人为所属组之后，这个人对于该文件就不是other了。就可以访问该文件了。这就是**团队分组概念引出的所属组概念**

### Linux下的文件类型和权限
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667479276340-ed486f8b-625f-46cd-bdcb-cfd8f9af661f.png#averageHue=%230f0d0c&clientId=u3df7ef7c-43e8-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=209&id=u5f05d8f8&margin=%5Bobject%20Object%5D&name=image.png&originHeight=314&originWidth=761&originalType=binary&ratio=1&rotation=0&showTitle=false&size=26391&status=done&style=none&taskId=ua74aaa99-d9fa-4d04-b624-1176becd101&title=&width=507.3333333333333)
首先可以看到每个文件的文件名前面都有很长的前缀，这些前缀代表了什么下面的图种都会有解释。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667518011301-e38ae0cf-c1ce-4989-93f9-4fb3d175ea60.png#averageHue=%23f7f7f7&clientId=u3df7ef7c-43e8-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=185&id=ud2bbee18&margin=%5Bobject%20Object%5D&name=image.png&originHeight=278&originWidth=1223&originalType=binary&ratio=1&rotation=0&showTitle=false&size=83729&status=done&style=none&taskId=ua171d9f0-ac7f-46bc-a607-edd344e5d3a&title=&width=815.3333333333334)
现阶段只需要学会前十个字母，这里面的第一个字母表示文件的类型：

1. -  ：表示普通文件
2. d ： 表示目录文件
3. c ： 表示字符设备文件（键盘，显示器）
4. b ： 表示块设备（磁盘文件）
5. l  ： 表示链接文件（类似Windows里面的快捷方式）
6. p ： 表示管道文件（两个终端，进程间通信）
7. s ： 表示socket（套接字）文件
> 剩下九个字符就是三对rwx，rwx代表了Linux下的三种文件权限：r代表可读，w代表可写，x代表可执行。
> 1. 读（r/4）：Read对文件而言，具有读取文件内容的权限；对目录来说，具有浏览该目录信息的权
> 2. 写（w/2）：Write对文件而言，具有修改文件内容的权限；对目录来说具有删除移动目录内文件的权限 
> 3. 执行（x/1）：execute对文件而言，具有执行文件的权限；对目录来说，具有进入目录的权限
> 4. “-”表示不具有该项权限
> 
第一对也就是除去文件类型后的前三个字母表示了owner对该文件的权限，rwx表示可读可写可执行，rw-表示可读可写不可执行，不具备的权限就是用 - 来代替。第二对代表了grouper对该文件的权限，第三对自然是other对于该文件的权限。

> 文件的权限表示方法有两种，一种是字母表示方法，就是使用上面的rwx，另一种就是使用八进制，rwx分别代表三位二进制位，三位二进制位合起来就是表示了一个八进制的数字。

补充：在Linux下文件的后缀并不能代表文件的类型，系统不像windows一样要求文件的后缀，文件的后缀在Linux下会被当成是文件名的一部分，但是我们是可以通过后缀来区分文件，这是给用户做提示符用来区分文件的，并且虽然Linux不以后缀区分文件类型，但是Linux下的工具比如gcc是用后缀来区分文件的类型的。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667518799772-29aa33aa-af8f-4ed6-9f98-d2cc3221948e.png#averageHue=%23141110&clientId=u3df7ef7c-43e8-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=70&id=u0f723ac0&margin=%5Bobject%20Object%5D&name=image.png&originHeight=105&originWidth=688&originalType=binary&ratio=1&rotation=0&showTitle=false&size=8735&status=done&style=none&taskId=u8ce93188-071a-4012-8eb9-fef8a403d82&title=&width=458.6666666666667)
这里使用gcc编译txt后缀的文件就会报错，编译.c后缀的文件就没问题，虽然他们在Linux系统下都是普通文件。
### 修改文件访问权限的指令
> **chmod **
> **功能：**设置文件的访问权限 
> **格式：**chmod [参数] 权限 文件名 
> **常用选项：**
> 1. R -> 递归修改目录文件的权限 
> 2. 说明：只有文件的拥有者和root才可以改变文件的权限

> chmod使用字母表示文件权限：
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667521747614-e0f21077-f275-48a2-8af8-3330de2d3071.png#averageHue=%23150e0d&clientId=u5a298350-62d8-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=252&id=u2e8f192c&margin=%5Bobject%20Object%5D&name=image.png&originHeight=378&originWidth=779&originalType=binary&ratio=1&rotation=0&showTitle=false&size=34192&status=done&style=none&taskId=ue7f1ea9b-ded5-4b69-a6a8-8ebf47a0da6&title=&width=519.3333333333334)
> 这里的u+就是代表给拥有者加上某个权限，g就是所属组，o就是其他人。
> 当然这里也可以直接多次操作，中间使用逗号连接。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667521686813-ca4a7275-900c-4450-9b64-2c14877b6afd.png#averageHue=%230f0b0a&clientId=u5a298350-62d8-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=167&id=u87678baf&margin=%5Bobject%20Object%5D&name=image.png&originHeight=250&originWidth=883&originalType=binary&ratio=1&rotation=0&showTitle=false&size=22610&status=done&style=none&taskId=uf8d2bff5-4277-4eae-b76e-5c77e82bcf9&title=&width=588.6666666666666)
> 如果想要对所有的角色权限都进行操作，那么可以使用a代表全体角色。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667521835689-0795783a-eca4-4618-9da5-5a0a10081775.png#averageHue=%23140d0c&clientId=u5a298350-62d8-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=149&id=u3358c25f&margin=%5Bobject%20Object%5D&name=image.png&originHeight=223&originWidth=754&originalType=binary&ratio=1&rotation=0&showTitle=false&size=18314&status=done&style=none&taskId=u70e2ed70-4d66-4769-ae99-fbe788bbfae&title=&width=502.6666666666667)
> 要注意的是a+r等这些表达式中间是不能加空格的。

只有root和文件的拥有者才可以修改文件的权限。
**简单说就是ugo +- rwx**
Linux下是如何匹配某个用户是该文件的owner还是grouper亦或者是other？先和owner匹配，不成功就和grouper匹配，如果还不成功就是other。
root之所以可以修改文件的权限是因为，root不受权限的约束，并且就算某个文件对于other没有r权限，root也是可以查看的，因为root对于所有文件都是无视权限的。
> 修改文件的权限还有一种方式就是使用八进制数字整体修改。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667522170230-a7664586-b08b-47c2-a493-b6b1e9a2bc46.png#averageHue=%23fafafa&clientId=u5a298350-62d8-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=379&id=u55cb126c&margin=%5Bobject%20Object%5D&name=image.png&originHeight=568&originWidth=1219&originalType=binary&ratio=1&rotation=0&showTitle=false&size=69902&status=done&style=none&taskId=u074cdf1c-23c4-457e-be1b-ffb8f8c715e&title=&width=812.6666666666666)
> 根据该表格，如果我们想要修改文件使得其所有权限都对所有用户开放，那么就是777
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667522250509-9d2c15ab-5956-4220-b2bd-916a9087073b.png#averageHue=%23140d0c&clientId=u5a298350-62d8-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=169&id=ud9b303f7&margin=%5Bobject%20Object%5D&name=image.png&originHeight=254&originWidth=752&originalType=binary&ratio=1&rotation=0&showTitle=false&size=21894&status=done&style=none&taskId=u5b68f8ad-866f-4633-a34a-b516aec113c&title=&width=501.3333333333333)
> 如果想要减掉某个权限只需要计算出那个权限的二进制位为0的时候代表的八进制数即可。
> 比如想要去掉所有用户的x权限，那么二进制就是：110110110，八进制就是666.
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667522366356-32ebd6fc-e21b-4e98-939d-60f97512ecf7.png#averageHue=%23150e0d&clientId=u5a298350-62d8-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=147&id=u1d6e4244&margin=%5Bobject%20Object%5D&name=image.png&originHeight=220&originWidth=745&originalType=binary&ratio=1&rotation=0&showTitle=false&size=18784&status=done&style=none&taskId=u6aeb6c55-838a-4909-adbb-49631f65e11&title=&width=496.6666666666667)

### 修改文件权限对应用户的指令
> **chown **
> **功能**：修改文件的拥有者 
> **格式**：chown [参数] 用户名 文件名 

修改文件的拥有者普通用户可以修改嘛？答案是：不可以，就算是文件的拥有者也是不可以的。因为你给别人一个东西是需要征得别人的同意的。但是我们无法得到其他用户的统一，所以这里使用chown需要使用sudo提权，用root用户无视权限直接修改。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667827438978-43a45551-19df-4330-8800-509723762242.png#averageHue=%230f0c0b&clientId=uba966951-6c08-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=211&id=ue0b1fcd0&margin=%5Bobject%20Object%5D&name=image.png&originHeight=316&originWidth=831&originalType=binary&ratio=1&rotation=0&showTitle=false&size=29464&status=done&style=none&taskId=u5085f678-5eae-4398-8512-a7f8370a51e&title=&width=554)
> **chgrp **
> **功能**：修改文件或目录的所属组 
> **格式**：chgrp [参数] 用户组名 文件名 
> **常用选项**：-R 递归修改文件或目录的所属组 

> 修改文件的所属组有两种情况，**其一修改方式就是像chown一样，使用sudo提权无视权限将文件的所属组修改为某个用户**。
> **其二就是，如果当前用户是文件的owner**，且当前文件的所属组不是owner，那么不用使用sudo可以将文件的所属组（grouper）修改为自己，但是如果当前文件的grouper是自己那么要修改成别人就需要使用sudo提权。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667828476205-9cc1aa6e-3fa8-4ea1-b4ec-f3203111fa4b.png#averageHue=%23120e0c&clientId=uba966951-6c08-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=187&id=ua975cd30&margin=%5Bobject%20Object%5D&name=image.png&originHeight=281&originWidth=786&originalType=binary&ratio=1&rotation=0&showTitle=false&size=26245&status=done&style=none&taskId=ud6fb51e2-2083-4d64-a251-1bebaa106b7&title=&width=524)
> 当前用户是文件的拥有者但是grouper是其他用户，要将grouper修改成自己此时不需要sudo提权也是可以的。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667827972439-ea0ff84d-c617-4bfb-9774-602fb6a60da9.png#averageHue=%23140d0c&clientId=uba966951-6c08-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=126&id=u7589b172&margin=%5Bobject%20Object%5D&name=image.png&originHeight=189&originWidth=1006&originalType=binary&ratio=1&rotation=0&showTitle=false&size=19361&status=done&style=none&taskId=u0208eed2-db94-4c43-8328-5794e23cb19&title=&width=670.6666666666666)
> 可以看到当前想要不用sudo来修改grouper是不可以的。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667828369100-fa400927-bdf9-463c-aabc-7874f5ec40b3.png#averageHue=%230e0c0b&clientId=uba966951-6c08-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=190&id=u30fa7195&margin=%5Bobject%20Object%5D&name=image.png&originHeight=285&originWidth=849&originalType=binary&ratio=1&rotation=0&showTitle=false&size=26782&status=done&style=none&taskId=u0afe0379-71c0-4207-a583-6d3f6c1bff3&title=&width=566)
> 该情况下必须使用sudo提权才可以进行所属组的修改。
> 举例子：就像是这个东西是属于我的我把他的使用权给你需要经过你的同意你不想要我不能强行给你，但是我想拿回来就是可以随便拿回来。

> 谈到了用户的所属组这里需要记住几个概念：
> 1. 每个文件只有一个owner
> 2. 一个用户可以属于多个组
> 3. 一个组可以有多个用户
> 4. 默认一个组一开始就只用一个用户，如果是该用户是某个文件的owner那么这个组的默认组名就是owner。
> 5. 以上的概念满足了一个文件对于不同的用户有不同的权限。

说完了两个指令，一个是修改文件的owner另一个是修改文件的grouper那么文件的other需要用指令进行修改嘛？
答案是：不需要，因为我们修改了文件的owner和grouper那么除了这两个组之外的成员都是属于other。
### 为什么要有权限（为什么）
权限可以限制某些用户比如普通用户不能访问root用户的文件，如果你不是这个用户的owner也可以通过权限限制你不可以修改这个文件。
> 所以权限的作用就是可以保护一些系统文件不会被随意的修改

为什么要有权限，就是为了便于对系统进行安全管理。
### 文件的起始权限
> 我们创建一个文件，这个文件是具有起始权限的，并且普通文件的起始权限和目录文件的其实权限是不一样的。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667829591882-171c31db-099f-48e4-8c8a-214d958610b1.png#averageHue=%23100d0c&clientId=uba966951-6c08-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=171&id=ue4102d78&margin=%5Bobject%20Object%5D&name=image.png&originHeight=257&originWidth=739&originalType=binary&ratio=1&rotation=0&showTitle=false&size=23680&status=done&style=none&taskId=u3365ac60-b6a0-48e1-845c-6f1420ec9f7&title=&width=492.6666666666667)
> 我们可以看到，目录文件d1的起始权限和普通文件t1的起始权限确实是不一样的。

目录文件和普通文件权限的不同主要取决于两方面：**起始权限和权限掩码**
> 什么是起始权限，就是系统在创建文件的时候给文件的初始权限，目录文件默认是777，也就是owner和grouper还有other都是具有rwx权限的。普通文件的默认权限是666，也即是所有的用户都具有rw权限没有x权限。

但是上面的起始权限和我们刚刚创建的文件也是不一样的啊，这时候就要知道权限掩码。
> 权限掩码叫做umask，系统有一个默认的权限掩码。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667829892696-0cc9e516-6723-4508-8b79-f19decd60de7.png#averageHue=%230e0c0b&clientId=uba966951-6c08-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=49&id=ue2d126bb&margin=%5Bobject%20Object%5D&name=image.png&originHeight=74&originWidth=557&originalType=binary&ratio=1&rotation=0&showTitle=false&size=5070&status=done&style=none&taskId=u7eacabc6-a471-4af8-8492-63b39ec09a6&title=&width=371.3333333333333)
> 第一个0表示后面的三位数是八进制数字。将002这个八进制数字转换成二进制就是000 000 010 对应着三个用户组，系统创建出来文件的默认权限是将初始权限去掉权限掩码中二进制位为1的那一位的权限，0002这个umask转化成二进制是 000 000 010对应到用户就是去掉other用户组的w权限。
> 这里使用的是位运算，文件初始权限 =（起始权限）&（~umask）

这里还有一个问题就是，想要进入一个目录需要拥有什么权限？
答案是：x权限，r权限可以让我们看到目录内的文件都有那些，w权限可以让我们在目录内增删文件，只有拥有x权限才可以进入目录。
这也就是为什么目录的起始权限是777，因为目录被创建出来的时候默认都是可以进入的。
### file指令
> **功能说明**：辨识文件类型。 
> **语法**：ﬁle [选项] 文件或目录...  
> **常用选项**：
> -c 详细显示指令执行过程，便于排错或分析程序执行的情形。
> -z 尝试去解读压缩文件的内容。

**file就是打印出文件的类型**
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667830763578-db1b2451-4174-47f4-9c10-ad86d0308ab0.png#averageHue=%230f0d0c&clientId=uba966951-6c08-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=87&id=uddff9057&margin=%5Bobject%20Object%5D&name=image.png&originHeight=130&originWidth=645&originalType=binary&ratio=1&rotation=0&showTitle=false&size=10406&status=done&style=none&taskId=ub97b1e59-4d60-4db2-b333-26bc1030fc6&title=&width=430)
t1.txt是个普通文件那么就是empty，d1是个目录文件就是directory。
### stat指令
> **功能说明**：查看文件的详细信息 
> **语法**：stat [选项] 文件或目录...  

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667830920489-e3ca2105-a4e3-483a-8752-bc561f46e689.png#averageHue=%230c0a09&clientId=uba966951-6c08-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=187&id=u9a6c044a&margin=%5Bobject%20Object%5D&name=image.png&originHeight=281&originWidth=1261&originalType=binary&ratio=1&rotation=0&showTitle=false&size=31062&status=done&style=none&taskId=u602cf727-0121-4969-bd44-74d4128728a&title=&width=840.6666666666666)
> 这个指令会将文件的所有信息都打印出来，需要特别注意的是着里的三个时间Access是代表最近访问时间，但是这个时间并不是我们访问一次直接就更改这个时间，因为有些文件可能会被短时间内多次访问，如果每次都修改一下Access时间那么太浪费性能了，所以只有遇到文件被短时间内访问了很多次之后这个文件的Access时间才会更改。
> Modify是代表最近文件内容修改的时间。修改了文件的内容，那么Modify时间就会更新。但是修改了文件的内容，文件的大小也会更新也就是文件的属性也会更新，于是Change也会跟着更新。
> Change是代表最近修改时间，只要文件进行了改动不管是内容修改了还是属性修改了，这个Change时间都会更新。

## 粘滞位
现在我们有这样一个需求，就是需要一个公共目录，在这个目录下需要多个用户都可以在该目录下进行开发，所以这个公共目录要对所有用户开放，其权限自然是要设置成 rwx rwx rwx 的。
> 但是如果一个用户对于目录拥有这些权限，比如xzj用户对于公共目录share拥有rwx权限，拥有r权限可以使用ls看到目录内的文件，拥有w权限可以增加删除文件，拥有x可以使用cd进入该目录。所以xzj在这个目录下什么都可以看。

> 假设这个公共的share目录下rxh用户创建了一个文件，该文件的权限是rwx --- --- 那么我作为xzj用户是受到权限的限制的，我是看不到这个文件的内容的。现在我作为xzj我很生气，我感觉我受到了侮辱，我能看到你的文件但是看不到你文件的内容，既然如此，那么我就把你的文件删掉。
> 注意这里xzj用户是可以做到的，因为删除目录下的文件需要的是拥有目录的w权限，与文件的权限无关。
> 所以为了解决上面这种情况，使得在该目录下大家可以各自rwx，但是禁止删除别人的文件，于是引入了**粘滞位**的概念。

> 只要我们对该目录添加了粘滞位，那么就可以禁止用户互相删除（即使用户拥有目录的w权限也不可以删除别人的文件），**粘滞位是权限的一种特殊情况。**

![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667833841563-4463bc08-b295-41f0-809e-f9830c1f2d99.png#averageHue=%23130e0c&clientId=ud3feed87-e154-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=313&id=uea2bddda&margin=%5Bobject%20Object%5D&name=image.png&originHeight=470&originWidth=787&originalType=binary&ratio=1&rotation=0&showTitle=false&size=49847&status=done&style=none&taskId=ucfec2240-716d-430e-90ef-ef67e2aeec0&title=&width=524.6666666666666)
通过指令chmod +t 就可设置一个目录的粘滞位，设置完粘滞位的目录颜色上会有一定的变化。并且目录权限的最后一个字母会从x变成t
## 权限总结
> 目录的-x 是表示你可否在目录下执行命令。 
> 如果目录没有-x 权限，则无法对目录执行任何命令，甚至无法cd 进入目, 即使目录仍然有-r 读权限（这
> 个地方很容易犯错，认为有读权限就可以进入目录读取目录下的文件） 
> 而如果目录具有-x权限，但没有-r权限，则用户可以执行命令，可以cd进入目录。但由于没有目录的读
> 权限所以在目录下，即使可以执行ls命令，但仍然没有权限读出目录下的文档。

