## 多模式编辑器——vim
> vim是一种多模式编辑器，我这里的vim总共有14种模式，如何查看vim都有什么模式呢？

首先输入指令vim打开vim，然后输入：help vim-modes最后输入回车即可查看vim的模式信息。
vim是一个编辑器那么什么是编辑器？编辑器也是可以写代码的，但是不能编译和运行代码，只能用来编辑代码，这就类似Windows里面的记事本，要和vs2019这种IDE区分开来，IDE是一个集编辑器编译器链接器等等的一个**集成开发环境**有了这一个IDE我们就可以完成代码的编写编译和运行调试。
> vim的模式众多但是日常用到的模式只有几个，这里我会介绍四个模式：命令模式（Normal mode），底行模式（last line mode），插入模式（Insert mode），替换模式（Replace mode）。

### vim的基本操作
> vim [文件名]
> 例如： vim test.c
> 使用vim打开文件。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667997570282-fa0867db-a297-4543-a061-828b4e817e46.png#averageHue=%23303030&clientId=u624123eb-1089-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=242&id=u6ed7339c&margin=%5Bobject%20Object%5D&name=image.png&originHeight=363&originWidth=470&originalType=binary&ratio=1&rotation=0&showTitle=false&size=8337&status=done&style=none&taskId=u241e93a0-023f-4777-b057-defe7833417&title=&width=313.3333333333333)
> 然后就进入了这个界面，需要注意的是使用vim打开文件，如果该文件不存在，那么vim会创建一个test.c文件并打开。

> **模式之间的切换**
> 我们使用vim打开文件之后默认处于命令模式
> 在命令模式下，按大多数按键除了命令按键之外，都是没有用的，系统还会发出嗡嗡的警告声。
> **命令模式和插入模式之间的切换**
> 在命令模式下输入i 就会进入插入模式，现在vim最底下会显示一个--INSERT--这时候就可正常进行文件的编写了。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667997845405-99d4967e-a3c5-4e7f-abbf-7eb1bd771b53.png#averageHue=%233f3f3f&clientId=u624123eb-1089-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=77&id=uaf45caab&margin=%5Bobject%20Object%5D&name=image.png&originHeight=116&originWidth=408&originalType=binary&ratio=1&rotation=0&showTitle=false&size=2472&status=done&style=none&taskId=u4c02b6a6-9d35-4483-ae25-ec023024057&title=&width=272)
> 如果想要从插入模式退回到命令模式，只需要按Esc按键即可。
> **命令模式和底行模式之间的切换**
> 在命令模式下，输入：（冒号，也就是shift + ；）即可进入底行模式。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667997995027-916956c1-f0a7-40e7-a699-4f7e20071bdd.png#averageHue=%23343434&clientId=u624123eb-1089-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=77&id=uabeae7f9&margin=%5Bobject%20Object%5D&name=image.png&originHeight=115&originWidth=366&originalType=binary&ratio=1&rotation=0&showTitle=false&size=1457&status=done&style=none&taskId=u83bd89e1-af3d-45c2-b3bc-3faaa68514a&title=&width=244)
> 当前在最底行会显示一个冒号和光标，这时候我们就可在底行输入指令了。
> 从底行模式退回到命令模式也是按Esc即可。
> **注意：不可能从插入模式直接跳到底行模式，或者从底行模式直接跳到插入模式**
> **从命令模式进入替换模式**
> 在命令模式下，输入R（也就是shift + r）就可进入替换模式，同样的，退出替换模式只需要按Esc即可。

### 命令模式（Normal mode）
> 在命令模式下，我们除了可以向其他模式跳转，还可进行一些快速高效率的文件操作。

> 首先是进行光标移动
> 上下左右的移动，可以使用小键盘的方向键，但是还可以使用HJKL这四个按键，因为小键盘离我们的手比较远，所以使用H J K L比较方便。
> H：向左移动光标
> J ：向下移动光标
> K： 向上移动光标
> L： 向右移动光标

那么为什么会出现这种移动光标的按键呢？
其实是在早期的键盘上是没有上下左右小键盘的，所以要进行光标移动就需要使用别的按键
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667998799709-ac174a97-e801-4d07-9120-1f072116cb45.png#averageHue=%2343534d&clientId=u624123eb-1089-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=413&id=ufba0f013&margin=%5Bobject%20Object%5D&name=image.png&originHeight=619&originWidth=1265&originalType=binary&ratio=1&rotation=0&showTitle=false&size=1403813&status=done&style=none&taskId=u2e6a4eb0-e4a7-4361-9929-a6b26552d93&title=&width=843.3333333333334)
> 为了 方便记忆按键对应的方向，可以这样想，H在最左端就是向左移动，L在最右端就是向右移动，J就是Jump的意思，向下跳，自然对应向下移动，K就是King国王自然高高在上就是向上移动。

> 光标定位
> **shift + 4 （$）**：将光标移动到行末
> **shift + 6 （^）**：将光标移动到行始
> **shift + g **： （G）将光标移动到文档末尾
> **gg**：回到文档的第一行。
> **n + shift + g**： 跳转到文档的第n行

> 文本编辑相关的快捷键
> **yy** ：复制光标所在行的文本 （nyy从当前行开始向下总计复制n行文本）yy相当于是1yy
> **p** ：粘贴，将复制的文本粘贴到光标所在行（支持多次粘贴，npp就是从光标所在行向下连续粘贴n次）
> **dd** ：删除， 删除光标所在行（ndd删除n行）
> 剪切指令在vim这里就是先dd然后再p，就可完成剪切的功能。
> **ctrl + r** ：恢复撤销的操作，也就是取消u的操作。
> **shift + `**：（组合起来就是~）将光标移动到要进行大小写转换的行上，一直按住shift + `就自动向后进行大小写转换，会把大写变为小写，小写变为大写，如果想要转换一个字符那么就按一次。
> 行内删除：**x**就是从光标开始向后删除，**shift + x（X）**就是删除光标前的字符，向前删除
> 删除支持**nx**和**nX**，同时删除掉的字符可以使用**p**粘贴出来。
> **w**：向后移动光标，按单词移动
> **b** ：向前移动光标，按单词移动

总结：命令模式给我们提供的快捷键可以大大加快我们对文档进行批量化操作的效率，所以命令模式的意义就是提高编辑效率。
### 插入模式（Insert mode）
插入模式很简单，就是可以正常的进行文本输入。
从命令模式进入插入模式有很多方式，常用的就是三个按键
i，进入插入模式后光标不动，可以直接再光标处输入
a，进入插入模式后光标向后移动一个位置
o，进入插入模式后光标会向下插入一个空行，并且移动到该空行上
### 底行模式（Last line mode）
在命令模式下输入：进入底行模式，在底行里我们可以输入很多指令，这里主要介绍几个常用的指令。

1. set nu  可以显示行号
2. set nonu  取消显示的行号
3. vs [文件名] 可以进行分屏，如果文件不存在会创建一个文件![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668001595022-af4989e7-6ccf-488b-aa21-66189cf333d0.png#averageHue=%23110e0e&clientId=u624123eb-1089-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=743&id=ud6652177&margin=%5Bobject%20Object%5D&name=image.png&originHeight=1114&originWidth=2348&originalType=binary&ratio=1&rotation=0&showTitle=false&size=120436&status=done&style=none&taskId=ua9137831-db30-4f80-89b7-748714605a9&title=&width=1565.3333333333333)还可以分很多屏理论上没有限制，但是我们的屏幕只有这么大，而且在我分到第五屏的时候就卡死了。我们可以使用ctrl + ww在分屏之间切换，也就是在各个屏幕之间移动光标，光标在那一屏那么我们就可以在那一屏进行操作。
4. 底行输入！+ shell指令就可以执行命令，比如我们在vim下编写代码的时候遇到了一个库函数不知道该如何使用，那么我们可以直接用man查询这个函数。指令就是：！man 3 memset 然后加回车即可。![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668002342970-07cb292a-9187-4f0b-a867-8f4356a1f265.png#averageHue=%231e1e1d&clientId=u624123eb-1089-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=832&id=ucad69def&margin=%5Bobject%20Object%5D&name=image.png&originHeight=1248&originWidth=2352&originalType=binary&ratio=1&rotation=0&showTitle=false&size=78960&status=done&style=none&taskId=ubd0995dc-2937-4156-87fc-4a94919362f&title=&width=1568)会直接跳转到man手册里面，按下q退出man手册之后会回到命令行界面。![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668002471121-8d1d34ff-5bc4-4a75-9865-fefcd21341c5.png#averageHue=%23070605&clientId=u624123eb-1089-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=127&id=u7c1d0294&margin=%5Bobject%20Object%5D&name=image.png&originHeight=191&originWidth=1304&originalType=binary&ratio=1&rotation=0&showTitle=false&size=15318&status=done&style=none&taskId=uc95c477a-da21-4419-938a-46210018870&title=&width=869.3333333333334)只要按下enter就可回到vim里面继续编辑代码。
5. : %s/printf/cout/g 批量替换，输入这一行指令就是将文档中所有的printf替换成cout，这里的g就是globa代表全局的意思。
6. 在底行中直接输入数字，就可将光标跳转到数字对应的行。
7. /关键字，输入/后直接输入要查找的关键字，就可以在文档中查找你输入的关键字并进行高亮。如果当前显示的不是你要找的，按n会直接向下跳到下一个关键字处。
8. ？关键字，输入？后直接输入要查找的关键字，就可以在文档中查找你输入的关键字并进行高亮。如果当前显示的不是你要找的，按n会跳到上一个关键字处，按enter会跳到下一个关键字处。

可以看到？和/的功能是不是一样的，看起来是一样的，但是他们还是有不同的。
> 最后的指令就是如何保存文件并退出vim，在底行模式下，输入q就是quit的意思，直接退出不保存，输入w就是保存文档，保存并退出就是wq。如果遇到了只读文件使用w不能保存那么我们可以输入w！加上！表示强制保存，退出的时候如果退出不了也可以使用q！强制退出，wq！就是强制保存并退出。

## vim的基本配置原理
我们现在的vim实际上是非常难用的，没有配置的vim写代码就像是在windows的记事本上写代码，没有自动补全，没有代码高亮，没有缩进等等。
所以我们要对我们的vim进行配置，配置vim的原理就是在当前用户的家目录下添加一个.vimrc的文件，以点开头的文件都是隐藏文件，添加完.vimrc文件然后我们就可以在这个文件里面进行配置了。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668005027858-d42bb0eb-d37f-4a8b-8b90-92f0922e09af.png#averageHue=%23444343&clientId=u624123eb-1089-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=114&id=u9e1717ea&margin=%5Bobject%20Object%5D&name=image.png&originHeight=171&originWidth=295&originalType=binary&ratio=1&rotation=0&showTitle=false&size=4632&status=done&style=none&taskId=ud37df600-9be4-4250-86a2-c3bf3874d48&title=&width=196.66666666666666)
这是.vimrc里面的内容，保存退出。
配置完之后再用vim打开一个文件
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668004964927-3b4043ab-3a5b-48db-8502-3d0bf056a52d.png#averageHue=%23070606&clientId=u624123eb-1089-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=253&id=u5ed5c62c&margin=%5Bobject%20Object%5D&name=image.png&originHeight=379&originWidth=662&originalType=binary&ratio=1&rotation=0&showTitle=false&size=12955&status=done&style=none&taskId=ucec4d050-d2f2-40e2-b2ec-b9c96eb9622&title=&width=441.3333333333333)
这样就可以默认显示行号了。
更多具体的配置我们可以百度出来，然后将对应的选项加入.vimrc文件保存之后，如果添加的指令没有在vim里面生效，可以输入source .vimrc让配置生效一下，如果还是不行，那说明你设置的选项在当前的vim下不支持。
关于vim的配置：

1. vim的配置是一人一份的，配置一个用户不影响其他用户，因为我们当前用户的.vimrc配置文件时是在用户的家目录下，每个用户的配置文件不同那么vim的配置自然就不同。
2. 所有用户使用的是同一个vim程序，但是使用的是不同的vim配置文件。
3. 配置vim，就是修改用户家目录下的.vimrc配置文件。
4. 从哪里可以获取vim的配置选项？百度吧少年

虽然说vim的配置文件是每个用户都有一个的也就是说每个用户的vim配置是不一样的，但是我们也可设置一个全局的vim配置给所有的用户都进行vim的配置，这个配置文件的路径就是/etc/vimrc，vimrc就是这个全局的vim配置文件。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668006157630-b9026e59-4262-454f-b924-a4f3236e85ba.png#averageHue=%2312100e&clientId=u624123eb-1089-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=45&id=u1b072e62&margin=%5Bobject%20Object%5D&name=image.png&originHeight=68&originWidth=826&originalType=binary&ratio=1&rotation=0&showTitle=false&size=7730&status=done&style=none&taskId=u2e831d13-790c-41a0-b807-d64e4ccd03b&title=&width=550.6666666666666)
## 配置sudoers文件
如果没有配置过sudoers文件那么是不能使用sudo指令来进行提权的。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667831608434-41eb4bc9-e857-4f03-ba5a-c8335cb1ddaa.png#averageHue=%230f0d0c&clientId=uba966951-6c08-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=66&id=u0c9c7527&margin=%5Bobject%20Object%5D&name=image.png&originHeight=99&originWidth=1043&originalType=binary&ratio=1&rotation=0&showTitle=false&size=10417&status=done&style=none&taskId=u5a61a34e-aa90-44ed-b588-d13427cdf85&title=&width=695.3333333333334)
会爆出错误说你当前的用户不在sudoers文件中，为了解决这个问题我们就需要配置一下该文件。
首先需要找到sudoers这个文件，这个文件的路径是：/etc/sudoers
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668007650717-fe6ea526-e6c4-4ce7-97ab-8ab3dfa4aec9.png#averageHue=%23110f0d&clientId=u624123eb-1089-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=47&id=u38db09b8&margin=%5Bobject%20Object%5D&name=image.png&originHeight=70&originWidth=856&originalType=binary&ratio=1&rotation=0&showTitle=false&size=7467&status=done&style=none&taskId=u47b068e5-2b72-4df9-ad88-71a3f178f9b&title=&width=570.6666666666666)
然后使用vim打开这个文件，找到下面这一部分文本
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668007704331-677c3337-1735-4ede-84e8-1121215a809e.png#averageHue=%23010101&clientId=u624123eb-1089-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=457&id=uf3eb595f&margin=%5Bobject%20Object%5D&name=image.png&originHeight=686&originWidth=1492&originalType=binary&ratio=1&rotation=0&showTitle=false&size=60514&status=done&style=none&taskId=u486836e3-9d80-4cf3-89d1-0b018a5f739&title=&width=994.6666666666666)
在这里添加上你要授权sudo的用户保存退出即可，因为该文件是只读的，所以保存退出的时候要使用wq!来强行退出。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668007847442-1807555b-2810-490b-b352-87af3d50772e.png#averageHue=%230d0b0a&clientId=u624123eb-1089-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=68&id=u4c8ddfd9&margin=%5Bobject%20Object%5D&name=image.png&originHeight=102&originWidth=622&originalType=binary&ratio=1&rotation=0&showTitle=false&size=7200&status=done&style=none&taskId=uab65bfd8-ab14-4eea-89f4-0a04e6cdd33&title=&width=414.6666666666667)
添加完毕后就可以正常使用sudo了。
