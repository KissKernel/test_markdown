## shell是什么
首先shell的英文解释就是外壳的意思，我们一般将shell认为是一个**命令行解释器**
在我们登录Linux的云服务器之后，我们可以看到一个提示符，提示我们可以输入指令了。
> shell可以将我们输入的指令转换后给操作系统执行，同时将操作系统的执行结果反馈给用户。

Linux是一个操作系统，但是Linux是包括Linux内核和外壳的shell程序的，就像下面的图。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1667202075198-ddd0ea79-7bab-4b83-9667-d92a30ab7248.png#clientId=ucb3ff642-f064-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=183&id=u97925781&margin=%5Bobject%20Object%5D&name=image.png&originHeight=275&originWidth=448&originalType=binary&ratio=1&rotation=0&showTitle=false&size=6334&status=done&style=none&taskId=uebd461b3-385b-4e19-bec3-9a748a38428&title=&width=298.6666666666667)
Linux内核（kernel）加上shell外壳统称为Linux操作系统。
shell程序就是我们用户和操作系统交流之间的桥梁。
## shell的意义
shell存在有两个意义：

1. 方便用户与操作系统进行交互，因为人是善于直接和操作系统交流的。
2. 对于用于输入的非法指令，shell可以直接拒绝传递给操作系统，这是变相的保护了操作系统。
> 关于第一个意义，人是不善于直接与操作系统进行交互，Linux通过shell外壳将用户的指令转换后交给操作系统进行处理。在Windows的GUI图形界面中同样也是如此，我们并不是也不能直接操作内核，而是通过图形界面，进行操作。这些操作会被GUI转换后直接由Windows内核进行处理。并不存在说将图形操作转换成指令然后再由命令行解释器解释后再给操作系统进行执行。

> 第二个意义，在Linux中我们有时候会出现输入错误的指令，这些指令就是非法指令，shell对于我们输入的命令进行合法性分析，然后可以将非法指令挡在操作系统的外面，这样操作系统就可以只关于合法指令的执行了。这就是变相的保护了操作系统。

## shell执行指令的方式
> 执行指令的时候，shell是通过派生子进程的方式执行用户的指令，而shell本身并不直接执行指令。
> 原因是：用户输入的某些非法指令虽然会被挡在操作系统之外，但是shell可能会因为某些指令挂掉，但是shell程序是不可以挂的，一旦shell外壳程序挂掉了，那么用户就无法与操作系统进行交互了。

shell其实是一个外壳程序的统称，具体细分之下，比如centen os和kail还有red head包括Ubuntu这些操作系统他们的shell外壳程序都是bash。
bash和shell之间的关系其实就是一个个体与类之间的关系。
