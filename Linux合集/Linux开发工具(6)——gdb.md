gdb是Linux系统上的一个命令行式的调试工具
> 关于调试的思路是和Windows下一样的，先通过隔离来判断代码出错的大致范围，然后通过调试逐渐缩小排查区域最后找到bug进行修正。

## gdb工具安装
如果没有安装gdb可以通过下面的指令一键安装gdb
```c
sudo yum install -y gdb
```
## gdb指令
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676905952539-adae43f4-df70-4ade-a02f-1b68ff62709f.png#averageHue=%231e1d1d&clientId=uc53a5996-f2ac-4&from=paste&height=664&id=uc0c4b233&name=image.png&originHeight=996&originWidth=1134&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=63824&status=done&style=none&taskId=uccb0ad00-036f-4f0b-bd5b-5d30d4f13cf&title=&width=756)
这段代码是我们即将进行调试的代码。
用该代码编译出来的可执行程序是process。
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676906018913-63e47c48-109e-4a16-976e-a0913d85769c.png#averageHue=%230e0c0b&clientId=uc53a5996-f2ac-4&from=paste&height=152&id=u9ba08cbc&name=image.png&originHeight=228&originWidth=882&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=26328&status=done&style=none&taskId=u76e433cd-3b16-452c-9e44-db3d4a57f1c&title=&width=588)
### gdb  process(可执行程序名称)
使用该指令可以使gdb打开可执行程序
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676906076154-f75cc7c6-5079-47c5-8682-ebd0eb461722.png#averageHue=%23100e0c&clientId=uc53a5996-f2ac-4&from=paste&height=274&id=ueb4a81db&name=image.png&originHeight=411&originWidth=1252&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=50032&status=done&style=none&taskId=u22b348f5-11c4-4141-bf61-434a7215429&title=&width=834.6666666666666)
这时候在下面输入指令是没有用的，因为此时的可执行程序中没有包含调试信息
可执行程序的版本有debug和release，Linux下的g++和gcc编译出来的可执行程序默认都是release版本的。
```c
gcc test.cpp -o process_g -g 
//只需要在编译指令后加 -g 就可以使编译器以debug的方式进行编译
```
> debug版本比release版本多包含了调试信息从哪可以看出？
> 1.文件大小
> ![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676906483504-81331403-e318-4764-b3e3-ae39a034a7db.png#averageHue=%23100d0c&clientId=uc53a5996-f2ac-4&from=paste&height=212&id=u578640fb&name=image.png&originHeight=318&originWidth=877&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=38303&status=done&style=none&taskId=u1a0a1c30-931c-49a7-ad31-c3ff04f06b3&title=&width=584.6666666666666)
> 2.可执行程序的二进制内部结构
> 通过指令``` readelf -S process ``` 就可以查看可执行程序的二进制信息。
> ![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676906639138-1df54bf7-b225-4e89-a07a-819c8198cd9c.png#averageHue=%23120f0d&clientId=uc53a5996-f2ac-4&from=paste&height=535&id=uc70abf62&name=image.png&originHeight=802&originWidth=1196&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=69098&status=done&style=none&taskId=u4735a673-a872-43b9-b566-58c37c935db&title=&width=797.3333333333334)
> 当然是一堆我们看不懂的东西，但是通过grep进行行过滤之后我们可以找到关于debug有关的信息。
> ![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676906762669-da811c88-1160-482b-be64-3cc43e5913a4.png#averageHue=%231c1211&clientId=uc53a5996-f2ac-4&from=paste&height=175&id=uc69485df&name=image.png&originHeight=262&originWidth=1140&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=36727&status=done&style=none&taskId=u8701e3dc-9c64-451c-88f4-17312c194ae&title=&width=760)
> process_g包含了debug信息，process，release版本则没有。

> 为什么程序要有debug和release版本呢，首先debug包含调试信息，程序员可以对程序进行调试，但是用户不需要进行调试，因为我们不能给用户他不需要的东西，所以release版本呢就是去掉了调试信息并优化了可执行程序的速度。

### 查看代码 l 0
l是list的缩写，0就是开始显示的行号，也可以是其他数字。
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676980027550-bc049554-b64e-496f-affb-09f447c5ed19.png#averageHue=%23050404&clientId=u93789737-d16a-4&from=paste&height=260&id=ude93234e&name=image.png&originHeight=390&originWidth=872&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=19000&status=done&style=none&taskId=udc1c9a25-1477-415c-9bd1-447c83e2d0f&title=&width=581.3333333333334)
这时候已经显示了一部分代码，如果想要继续显示下面的代码只需要按回车就行，gdb会记住你上次使用的指令，再次按回车就会重复执行。
### 添加断点 b 27
b是break的缩写，就是breakpoints断点的意思，27就是要添加断点的行号
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676980175566-9f7ce67f-4bd7-4861-be97-b34842d3c99c.png#averageHue=%23070505&clientId=u93789737-d16a-4&from=paste&height=316&id=u1098fe7e&name=image.png&originHeight=474&originWidth=1171&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=32312&status=done&style=none&taskId=ub7eb386d-c280-4e63-8982-affd3eec409&title=&width=780.6666666666666)
这里就是在27行和29行添加了断点
### 查看断点 info b
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676980221424-c1c5e8ea-6dc9-4385-8b15-999a82240e27.png#averageHue=%230c0b0a&clientId=u93789737-d16a-4&from=paste&height=170&id=u1f00327c&name=image.png&originHeight=255&originWidth=1226&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=26430&status=done&style=none&taskId=ud85b4dbe-13ec-4d34-85f6-729d0496ecd&title=&width=817.3333333333334)
通过info b可查看现在已经存在的断点，Num这一列是断点的编号。
### 删除断点 d 1
d是delete的缩写 ， 1就是断点的编号（注意不是断点所在的行号）
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676980280986-9fbe58a8-b48f-4825-95d2-da7cd51055c1.png#averageHue=%230a0908&clientId=u93789737-d16a-4&from=paste&height=278&id=u3d153aa0&name=image.png&originHeight=417&originWidth=1275&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=39234&status=done&style=none&taskId=u162935a1-8bba-45a0-b5d3-21ad8d2528b&title=&width=850)
通过d指令加断点的编号就能删除这个断点。
### 启动调试 r
r就是run的简写，作用就是启动调试跳转到第一个断点处。
如果程序中没有断点会直接跑完整个程序。
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676990063447-207e4ed8-a0b8-4f8a-9995-e9d2c5849ddb.png#averageHue=%230b0a09&clientId=ua6084b7f-75bf-4&from=paste&height=317&id=ud1c9305f&name=image.png&originHeight=475&originWidth=760&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=27814&status=done&style=none&taskId=u292df44a-3748-4288-97aa-a8b56d38a9a&title=&width=506.6666666666667)
run之后程序执行了断点之前的代码并将结果输出到屏幕上，然后在第一个断点处27行停下。
> 如果在r之后再r一次
> ![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676990206601-7e10737b-c36a-43fe-8e9f-07878db920ac.png#averageHue=%230d0908&clientId=ua6084b7f-75bf-4&from=paste&height=381&id=u3acde9ed&name=image.png&originHeight=571&originWidth=891&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=35391&status=done&style=none&taskId=u307a4df4-fedb-4ff3-ac42-0ea43d12e7f&title=&width=594)
> 这时会提示程序debug已经开始了是否要重新开始。输入y就重头开始了。n就无效

### 逐过程 n
n是next的简写，就是类似vs中的F10
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676990374217-d2b5e7e4-d464-419a-88a1-f59d6dfa160c.png#averageHue=%23080706&clientId=ua6084b7f-75bf-4&from=paste&height=277&id=u7ece29e1&name=image.png&originHeight=415&originWidth=732&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=22068&status=done&style=none&taskId=uca1a6f70-e4c3-4df8-be18-1d23e57b612&title=&width=488)
再函数内部使用n就是逐过程，一条一条的执行程序语句。
> 如果在函数入口处用n逐过程那么就不会进入函数内部，直接执行下一条语句。
> ![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676990467139-8a7f7280-36d9-4da3-95a8-893616df2068.png#averageHue=%230d0b0a&clientId=ua6084b7f-75bf-4&from=paste&height=336&id=u4fb94414&name=image.png&originHeight=504&originWidth=660&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=28876&status=done&style=none&taskId=u9d1f0c32-31d2-42b0-90e3-93e24c8f833&title=&width=440)

### 逐语句 s
s是step的简写，就是类似vs中的F11
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676990274168-b1e124b4-a55b-4862-8c08-35df793250b8.png#averageHue=%230c0a09&clientId=ua6084b7f-75bf-4&from=paste&height=135&id=u8f75be0d&name=image.png&originHeight=203&originWidth=598&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=11834&status=done&style=none&taskId=u928f838b-dc48-4c1b-b607-368e5037bd4&title=&width=398.6666666666667)
当断点处于27行，函数Add(10,100)这里的时候，使用s指令逐语句就可以直接进入函数，同时gdb会显示函数参数的值以及当前函数第一行的行号。
### 断点间跳转 c
c是continue的简写，就是从一个断点跳转到下一个断点。
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676990596975-6fa7a5b5-611d-456c-abe4-2149a4e62831.png#averageHue=%23070606&clientId=ua6084b7f-75bf-4&from=paste&height=609&id=u3dc93f7e&name=image.png&originHeight=914&originWidth=1011&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=53957&status=done&style=none&taskId=u6ce1a04e-1749-446e-8479-e2dedd59630&title=&width=674)
当我们在最后一个断点处再次c的时候gdb会直接执行完剩下的所有指令
### 查看调用堆栈 bt
使用bt查看调用堆栈可以看到当前我们所处的函数是第几层，可以查看调用深度。
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676990723815-9eb73de8-80df-40e1-89af-fed1ea65c7dc.png#averageHue=%230b0a09&clientId=ua6084b7f-75bf-4&from=paste&height=155&id=ua54d64d3&name=image.png&originHeight=233&originWidth=811&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=15239&status=done&style=none&taskId=uacb72190-f11a-4a1f-a3b1-ac7b373e55a&title=&width=540.6666666666666)
当前我处于Add函数内，所以调用堆栈上在main函数上压了Add函数

### 结束当前函数调用 finish
可以直接跑完当前所处的函数，并带回一个返回值。
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676990843662-dc988989-b49c-4cb1-aa95-38c226479fde.png#averageHue=%230a0908&clientId=ua6084b7f-75bf-4&from=paste&height=239&id=u49d28354&name=image.png&originHeight=358&originWidth=966&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=26782&status=done&style=none&taskId=ub6db5932-3121-4118-955b-ae9fddb3e41&title=&width=644)
当我进入Add函数中的时候，使用finish就可以直接跑完Add函数的逻辑，并且显示函数的参数，所在文件中的行号，函数地址，以及该函数在主函数中调用的位置，还有返回值是多少。
> 如果我们在main函数中使用finish，gdb不会直接跑完main函数而是会报错。
> ![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676991007218-f0f8fa17-c571-4b21-8fa3-a6bdb682b2d9.png#averageHue=%23080706&clientId=ua6084b7f-75bf-4&from=paste&height=115&id=u1fecb45b&name=image.png&originHeight=173&originWidth=815&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=8407&status=done&style=none&taskId=u4945fe8a-aa20-4787-94d1-7f39555622a&title=&width=543.3333333333334)
> 意思是，finish指令在最外层的栈帧中没有意义，也就是finish在main函数中使用没有意义。

### 监视 p num(变量名)
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676991355818-268d7f97-e87d-4468-8272-b64a4be7e188.png#averageHue=%23090606&clientId=u12c6815c-76f4-4&from=paste&height=357&id=ub7681dee&name=image.png&originHeight=535&originWidth=782&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=27328&status=done&style=none&taskId=uf39f9342-92f8-4483-8f11-7639a219bcc&title=&width=521.3333333333334)
p除了可以用来显示上下文中变量的值之外，还可以显示变量的地址。当然使用p指令显示的变量只会显示一次，下次要看还需要再次输入p指令
### 常显示 display expr(变量或表达式)
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676991604805-e86a2952-9e6d-4e19-8269-2276af686777.png#averageHue=%23050404&clientId=u12c6815c-76f4-4&from=paste&height=582&id=ucf558772&name=image.png&originHeight=873&originWidth=1714&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=55267&status=done&style=none&taskId=u6318a971-ca83-4ece-9146-81f11d19fe3&title=&width=1142.6666666666667)
当我们使用display将i变量常显示之后，随着代码的执行，i变量的值也会跟着变化。
对于已经常显示的变量会被记录在一个自动显示列表，使用info display可以查看。
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676992308326-206900bd-442c-4082-9bf7-4db36c45b307.png#averageHue=%230c0706&clientId=u12c6815c-76f4-4&from=paste&height=437&id=u0ebb842d&name=image.png&originHeight=656&originWidth=655&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=27175&status=done&style=none&taskId=u3e20620d-8636-4f7f-94ca-ae665c1daee&title=&width=436.6666666666667)
每次单步运行显示在变量前面的数字就是变量的编号。
> display还可以进行格式化显示
> display/fmt  expr
> expr是变量或者表达式
> ![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676992525881-9afec414-47bc-40bc-9987-92949bbc6369.png#averageHue=%23f5f4f2&clientId=u12c6815c-76f4-4&from=paste&height=371&id=ufc023c62&name=image.png&originHeight=557&originWidth=609&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=146765&status=done&style=none&taskId=u06613587-481f-4776-973f-e9a0cfd3184&title=&width=406)

### 取消常显示 undisplay 
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676992379687-216582ae-8c87-476b-a7eb-e207ace91ae8.png#averageHue=%23090706&clientId=u12c6815c-76f4-4&from=paste&height=298&id=uf5d9d244&name=image.png&originHeight=447&originWidth=806&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=23997&status=done&style=none&taskId=ue60f8e4c-df23-4a39-b206-4fc1c5fa534&title=&width=537.3333333333334)
undisplay + 变量编号，就可以取消这个变量的常显示，如果直接用undisplay就是直接将所有的常显示的变量都取消常显示。
### 跳转到指定行 until  x (行号）
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676992806390-0e4e287f-f386-4cd8-91a6-56209cf19d9e.png#averageHue=%230c0908&clientId=u12c6815c-76f4-4&from=paste&height=444&id=ucd5d46d8&name=image.png&originHeight=666&originWidth=731&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=36000&status=done&style=none&taskId=u19765785-9254-4fb2-af35-aaeb6db8878&title=&width=487.3333333333333)
使用until进行，行跳转的时候不能进入其他函数，且中间遇到断点会在断点处停止
> 例如现在代码的23行和27行各有一个断点，我现在在23行，想要使用until跳转到29行。
> ![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676992993177-6460cf65-0691-4863-9344-85e168866489.png#averageHue=%23090706&clientId=u12c6815c-76f4-4&from=paste&height=690&id=u0bb9f4c7&name=image.png&originHeight=1035&originWidth=1221&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=73088&status=done&style=none&taskId=ub315f84a-ab71-4c90-8be8-d12b610522d&title=&width=814)
> 当第一次跳转的时候中间遇到27行断点停下来了。第二次跳转中间没有断点才跳转成功

### 修改变量值 set var  i（变量名）
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676993151206-e1b5d6f9-2351-47a7-a666-8282c896c0e2.png#averageHue=%23060504&clientId=u12c6815c-76f4-4&from=paste&height=245&id=uafa75cd6&name=image.png&originHeight=367&originWidth=1142&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=19626&status=done&style=none&taskId=u3c4ac2dd-b5e0-407d-8f84-7f7374ced2b&title=&width=761.3333333333334)
循环本来循环到i = 7，但是我们使用set var直接将i值修改成了5，下次显示变量i的值可以看到，i确实变成5了。
### 显示一个区域内的临时变量 info locals
功能类似与VS中的自动窗口。
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676993334310-0d4f37a2-366a-48dc-8797-64d6f82b30cf.png#averageHue=%23f0eeed&clientId=u12c6815c-76f4-4&from=paste&height=667&id=ufa210cf5&name=image.png&originHeight=1001&originWidth=1034&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=128569&status=done&style=none&taskId=u54e63795-1aa5-42de-8294-f6c666e85fc&title=&width=689.3333333333334)
作用都是显示一个局部范围内变量的值。
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676993464877-409e688a-bf24-45a8-9a5c-044863cefbe6.png#averageHue=%23080707&clientId=u12c6815c-76f4-4&from=paste&height=445&id=ub456b18e&name=image.png&originHeight=668&originWidth=731&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=33617&status=done&style=none&taskId=u80014eeb-823f-47dc-9444-73b8b38dc2c&title=&width=487.3333333333333)
第一次的时候因为sum初始化还没有完成所以是随机值。
可以看出info locals打印出来的局部范围内的变量不是常显示。
### 使能断点 disable 2 （断点编号）
disable 2 使能断点，使之无效
enable 2 使能断点，使之有效
当我们执行代码的时候想要这个断点失效，但是不想删除这个断点的时候就可disable +断点编号使之不能。也就是失效了。
在info b显示的断点列表里面
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676993692596-bb17fb72-bde9-468b-b546-ad7ca03d260d.png#averageHue=%230f0d0c&clientId=u12c6815c-76f4-4&from=paste&height=115&id=u770127e3&name=image.png&originHeight=172&originWidth=1217&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=18785&status=done&style=none&taskId=u8633f471-76db-4739-98a5-414c368f132&title=&width=811.3333333333334)
Enb这一列就是Enable的缩写，y就是yes代表断点有效。
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676993714155-2b666e4b-6b8e-4b12-8a5f-33fd15615d4d.png#averageHue=%230e0c0b&clientId=u12c6815c-76f4-4&from=paste&height=133&id=u3abd5497&name=image.png&originHeight=199&originWidth=1227&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=20375&status=done&style=none&taskId=u9ba5a121-2ed4-456d-ae10-fee69b4b26a&title=&width=818)
当我们将2号断点失效后，Enb就变成了n此时2号断点就失效了。
使用指令enable 2可以令断点2重新生效。
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1676993776368-c73062fe-8686-4827-a926-45f650aebb29.png#averageHue=%230e0c0b&clientId=u12c6815c-76f4-4&from=paste&height=133&id=u040aa61a&name=image.png&originHeight=200&originWidth=1239&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=20537&status=done&style=none&taskId=udea757d5-c040-4dd0-9235-33ce9a1adfb&title=&width=826)






