### 工作路径（当前路径）
我们在C语言部分如果了解过文件相关函数的学习的话一定听过，创建打开文件默认是在当前路径下，那么这个当前路径是什么？
**当前路径就是进程启动时可执行程序所在的路径默认就是当前路径。**
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679577745711-9e485194-81ed-400f-996a-0f1fe3a900ae.png#averageHue=%231e1d1d&clientId=ubc276710-d260-4&from=paste&height=273&id=u1bdd3af6&name=image.png&originHeight=410&originWidth=921&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=27868&status=done&style=none&taskId=u27ada94c-4fb3-4d36-a80c-e3dc7a1de55&title=&width=614)
测试代码
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679577782691-6bce9c95-7a16-48fc-a703-7cb7ac04703c.png#averageHue=%23090807&clientId=ubc276710-d260-4&from=paste&height=330&id=ubb08f17f&name=image.png&originHeight=495&originWidth=785&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=55440&status=done&style=none&taskId=u95a2b478-ac92-4551-8322-e2aed381dc0&title=&width=523.3333333333334)
当进程跑起来之后，在/proc/目录下存在一个和进程PID同名的目录，这个目录里面记录了进程的所有信息。
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679577839636-fbede426-f55e-4168-802c-00cde36b9274.png#averageHue=%230d0b0a&clientId=ubc276710-d260-4&from=paste&height=232&id=uf8d7e5d6&name=image.png&originHeight=348&originWidth=1052&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=37849&status=done&style=none&taskId=u0d1b19fd-4a87-4243-9013-71de94a0004&title=&width=701.3333333333334)
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679577886006-a1dfc9f6-e931-4769-b9e6-457e4eedb42e.png#averageHue=%230b0807&clientId=ubc276710-d260-4&from=paste&height=419&id=u24a810c6&name=image.png&originHeight=629&originWidth=1486&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=74879&status=done&style=none&taskId=u80777562-9554-458f-96c6-d591f810d10&title=&width=990.6666666666666)
exe就是标识着当前进程所执行的可执行程序的路径。
cwd就是当前进程的工作路径也叫做当前路径
### chdir指令
chdir指令的作用就是修改当前进程的工作路径
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679578071825-0908488b-6ffc-40a0-b22d-d8dd171604a6.png#averageHue=%23090808&clientId=ubc276710-d260-4&from=paste&height=229&id=ufb3bb9ad&name=image.png&originHeight=343&originWidth=1421&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=19058&status=done&style=none&taskId=u41e7a3cd-cc84-4cd2-9b73-7256c2a7c9b&title=&width=947.3333333333334)
返回值：修改路径成功返回0，修改失败返回-1并设置错误码。
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679578043796-1fb62830-f512-4154-b82d-c3e8c1add491.png#averageHue=%231f1d1d&clientId=ubc276710-d260-4&from=paste&height=292&id=ua2a71c60&name=image.png&originHeight=438&originWidth=771&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=30828&status=done&style=none&taskId=u3b71ecc7-a04e-4e64-af15-19e3fa81da8&title=&width=514)
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679578163367-ec75aeb9-fd56-4ad1-b933-30242ce312dc.png#averageHue=%23090707&clientId=ubc276710-d260-4&from=paste&height=270&id=uf22c6096&name=image.png&originHeight=405&originWidth=1495&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=52529&status=done&style=none&taskId=ua11b8b01-9381-4b79-aad7-d3b4fe8ae86&title=&width=996.6666666666666)
当这个进程运行起来的时候工作路径就被修改完成了。
### 关于文件
首先是关于文件的几点共识：

1. 空文件也是需要占用磁盘的空间
2. 文件 = 内容 + 属性
3. 文件操作 = 对内容的操作 + 对属性的操作 或者对内容和属性的操作
4. 表示一个文件必须使用：文件路径 + 文件名 (因为树状结构文件路径具有唯一性)
5. 如果在进行文件操作的时候没有规定路径，默认是在当前路径下进行操作
6. 当我们写了一份文件操作的代码编译成可执行程序之后，如果没有运行这个可执行程序，文件操作是不会被完成的，因此我们**对文件的操作本质是进程对文件的操作**
7. 一个文件如果没被打开是不能被访问的，所以**一个文件要被访问就必须先被打开**
8. 磁盘中有很多很多文件，根据是否被打开可以分为两类：
   1. 被打开的文件(文件操作相关)
   2. 没被打开的文件(文件系统相关)

**所以文件操作的本质就是：进程  和  被打开文件之间的关系**
### 关于文件操作
#### 文件操作概述
> 我们学过的C语言中文件接口，C++有文件接口，那Java，Golang，PHP，Python肯定都有文件接口，并且这些语言的文件接口都是不一样的，那么这么多不同的文件接口我们在学习的时候势必会很困难。

但其实我们对文件进行操作首先要先找到文件再打开文件然后才能进行文件操作。
文件是在磁盘上的，磁盘是硬件，想要访问硬件必须要通过操作系统，也就是操作系统提供的系统调用接口，而且我们使用的操作系统只有一个，系统调用接口也是不变的。所以我们直接学习底层的系统调用再去理解各种语言的文件操作就会简单很多了。
所以不管用户层的语言如何变化，他们的库函数调用的都是系统调用接口，都是对系统调用接口的封装，不同语言的封装形式不同罢了。
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679632985191-f115dc63-3a86-4fe8-a0a2-1de758045c49.png#averageHue=%23f1f1f0&clientId=ubc276710-d260-4&from=paste&height=303&id=ud9ffbec5&name=image.png&originHeight=454&originWidth=737&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=12359&status=done&style=none&taskId=u9f093328-7a92-4494-afb0-480a333c9c1&title=&width=491.3333333333333)
#### C语言文件操作
C语言的文件操作接口常见的几个是：

1. fopen  打开文件

![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679633445263-56b639db-666d-4e88-8a1e-7c30af2000cb.png#averageHue=%23222020&clientId=ubc276710-d260-4&from=paste&height=399&id=u5a8b2a87&name=image.png&originHeight=598&originWidth=1360&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=34440&status=done&style=none&taskId=ue9c0ade0-de30-485b-96f3-e65db1a6176&title=&width=906.6666666666666)

2. fclose  关闭文件
3. fprintf  格式化向流中写入

![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679658826263-9d8863f7-7694-49d4-bb17-45ee3b44f6f0.png#averageHue=%230a0807&clientId=ubc276710-d260-4&from=paste&height=272&id=uabfd5742&name=image.png&originHeight=408&originWidth=1844&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=32249&status=done&style=none&taskId=u34e1c0fb-c20b-4145-aa07-e50f3613a13&title=&width=1229.3333333333333)
> 第一个参数就是流，其他参数就和printf是一样的。

4. fgets   从流中按行读取

![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679659556396-8626a0e0-fcac-41cc-a21e-c1a575108413.png#averageHue=%230a0707&clientId=ubc276710-d260-4&from=paste&height=309&id=u8e7c2e9e&name=image.png&originHeight=464&originWidth=1022&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=24077&status=done&style=none&taskId=ub2e5b6c9-6907-43f6-8a88-8ec009cc7b0&title=&width=681.3333333333334)
> 从stream流中读取一行内容到s中，一次读取最多是size字节
> 读取成功就返回s，读取失败就返回NULL

5.fread  
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679659808713-ea6f8f7e-6d2e-46d8-b8f0-70ed29165c82.png#averageHue=%230a0706&clientId=ubc276710-d260-4&from=paste&height=263&id=uae3941a6&name=image.png&originHeight=395&originWidth=1424&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=25900&status=done&style=none&taskId=uc96a37e7-0689-4250-8e70-6236bf213ae&title=&width=949.3333333333334)
> 从stream流中读取数据到ptr中，读取到的每个元素的大小是size，读取的次数是nmemb
> 返回值就是读取到的元素个数

6. fseek  

![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679660533717-a78a8ee7-06cd-4607-b343-dea831c320aa.png#averageHue=%23080606&clientId=ubc276710-d260-4&from=paste&height=349&id=ua8e4a1cc&name=image.png&originHeight=523&originWidth=1442&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=35194&status=done&style=none&taskId=u3afc1d67-3fb8-4f32-a44f-9dc3b37dd9f&title=&width=961.3333333333334)
> fseek的功能就是设置流stream的位置为给定的偏移量，偏移量的位置是相对与whence参数的。
> whence参数总共有三个，是三个宏，SEEK_SET  SEEK_CUR  SEEK_END分别是文件开始，文件指针当前位置，文件尾
> offset就是相对于whence的偏移量，单位是字节。

下面我们通过代码来简单演示几个文件操作接口的使用：
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679633536196-34c1c1fe-0214-420d-bf03-e0d696fc8d71.png#averageHue=%231e1e1e&clientId=ubc276710-d260-4&from=paste&height=312&id=u7485cde3&name=image.png&originHeight=468&originWidth=818&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=27369&status=done&style=none&taskId=u322328e0-7d4b-475b-8492-b1ff1cc070e&title=&width=545.3333333333334)
关于fopen的第二个参数也就是文件打开的方式，常用的有："r"只读，"w"只写，"r+"读写方式打开，如果文件不存在就报错，"w+"读写方式打开文件不存在就创建，"a"以追加的方式进行写入，"a+"以读写的方式打开，如果写入就是追加方式。"rb"，"wb"，"r+b"，"w+b"只要带上b就是bin的意思，是以二进制的方式进行读写操作。
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679658735873-4f573e99-7881-432e-baa3-e8a17271147f.png#averageHue=%23120d0c&clientId=ubc276710-d260-4&from=paste&height=272&id=ue0ffa8f1&name=image.png&originHeight=408&originWidth=876&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=42465&status=done&style=none&taskId=uda93c544-03d9-4d48-8be9-f5c2ec73837&title=&width=584)
当我们运行这个程序的时候就会自动创建一个空文件。
下面我使用fprintf(...)向文件流中写入
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679659018524-3b8114cc-36ad-411c-8dca-b2aacf789ade.png#averageHue=%231e1d1d&clientId=ubc276710-d260-4&from=paste&height=425&id=ud21f86bb&name=image.png&originHeight=638&originWidth=1050&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=41407&status=done&style=none&taskId=u8d76137a-0073-4516-98e3-cd9171c2bc3&title=&width=700)
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679659061109-84eb7c67-d2e7-422d-807e-da41758757ea.png#averageHue=%230e0c0b&clientId=ubc276710-d260-4&from=paste&height=415&id=ud89843c1&name=image.png&originHeight=623&originWidth=876&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=60210&status=done&style=none&taskId=ub24a3143-2e5a-4796-9cb2-7e879863739&title=&width=584)
当我们运行这个程序字符串就写入到文件中了。
> 有一个细节就是如果我们使用写的方式打开文件，此时**C语言**会帮我们清空这个文件然后再写入内容。

使用fgets读取文件内容
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679659712744-92b29771-2ee4-49c5-ba34-de513ce37435.png#averageHue=%231f1d1d&clientId=ubc276710-d260-4&from=paste&height=647&id=u180d7ef8&name=image.png&originHeight=970&originWidth=983&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=58443&status=done&style=none&taskId=ucacc7e7a-8fe4-4506-a125-93ea0292ef9&title=&width=655.3333333333334)
fgets是按行读取文件内容，每次读取一行（其实底层也是使用fgetc一个字符一个字符的读取遇到了\n就停止）
#### 系统文件操作
操作系统给出的操作文件的系统调用接口主要有：open，read，write，close，lseek，功能分别对应C语言的fopen，fread，fwrite，fclose，fseek。

1. open

![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679660839189-3345fef1-e014-410a-a7f0-84bcebf26baa.png#averageHue=%230a0807&clientId=ubc276710-d260-4&from=paste&height=305&id=ue62dc898&name=image.png&originHeight=458&originWidth=1433&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=33719&status=done&style=none&taskId=uf48566a6-fafc-4fbe-8ae8-e8541917796&title=&width=955.3333333333334)
> open有两个，两个参数的是针对文件已存在，三个参数的open是针对文件不存在需要创建文件的，第三个参数是文件的初始权限。返回值就是文件描述符(就是一个整数，用来标记文件的)。
> flags参数有：O_RDONLY  只读，O_WRONLY  写，O_RDWR 读写，O_APPEND 追加，O_CREAT 建议性选项，如果文件不存在将会创建文件，O_TRUNC ，对文件内容清空

系统调用接口和C语言接口有区别的，C语言接口有很多帮我们默认做的事情，比如以w方式打开文件如果文件不存在将创建一个文件，以w方式打开文件系统将会自动清空文件内容。而这些在系统调用接口这里都需要我们自己传参数规定，没有系统帮我们默认做什么。
**标记位传参**
open这个系统调用的第二个参数flags采用的是标记位传参，比如一个int有32个bit位，每个比特位都可以表示一种情况，所以总共可以表示32种参数，下面是样例代码：
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679662756425-37f19f8a-27ee-4164-8405-763e94637898.png#averageHue=%231e1e1d&clientId=ubc276710-d260-4&from=paste&height=629&id=u0f80aa28&name=image.png&originHeight=944&originWidth=1059&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=84514&status=done&style=none&taskId=u55343287-535b-4ce2-bf29-69cb67946d4&title=&width=706)
如果想要同时传多个参数只需要将所有参数或到一起
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679662798616-b40be2a5-c5cc-4ec3-9a04-fc99def8154f.png#averageHue=%23040303&clientId=ubc276710-d260-4&from=paste&height=212&id=ubde58535&name=image.png&originHeight=318&originWidth=680&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=9304&status=done&style=none&taskId=u16724024-5fac-4fcf-a8dd-29a606275e6&title=&width=453.3333333333333)

2. read

![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679663114557-6923eba3-526a-47e7-82cd-ba96d2551215.png#averageHue=%23050504&clientId=ubc276710-d260-4&from=paste&height=197&id=ub6e3ca63&name=image.png&originHeight=296&originWidth=1399&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=16929&status=done&style=none&taskId=u189de1b3-4ae8-45ee-aa0e-3f0efb813c9&title=&width=932.6666666666666)
> fd是文件描述符，buf是将读取的数据存入的位置，count是读取的字节数，在C语言种读取分为二进制读取和文本读取，但是在系统调用这里没有区分所有数据都是按照一个字节一个字节的读取不管你是文本还是二进制。
> ssize_t是系统级的一个整数，读取成功就返回读取成功的字节数，读取失败就返回-1并设置错误码。

3. write

![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679663306563-49bcca24-249b-45d2-a91a-13d62eb945fe.png#averageHue=%23060505&clientId=ubc276710-d260-4&from=paste&height=201&id=u79a55aba&name=image.png&originHeight=302&originWidth=1397&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=17513&status=done&style=none&taskId=uf103d984-6296-43a8-948d-cc49923c1e9&title=&width=931.3333333333334)
> 将buf中的数据写入fd文件描述符对应的文件中写入的字节个数是count，返回值是成功写入的字节个数。

4. close

![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679663860544-16b31026-2037-4f66-96ea-81d25e564ba0.png#averageHue=%23050404&clientId=ubc276710-d260-4&from=paste&height=195&id=u48ca677d&name=image.png&originHeight=292&originWidth=1369&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=14824&status=done&style=none&taskId=u438405a9-841c-4d9a-9af7-bd138e1ba46&title=&width=912.6666666666666)

5. lseek

![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679663897594-87773875-c677-4669-9dd8-b2a009d475b6.png#averageHue=%23070505&clientId=ubc276710-d260-4&from=paste&height=493&id=uec7e7e07&name=image.png&originHeight=739&originWidth=2287&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=51160&status=done&style=none&taskId=u549ae758-23cb-4c03-a3b3-d518071015c&title=&width=1524.6666666666667)
> 返回值，如果lseek调用成功就返回产生的偏移量，如果调用失败就返回（off_t）-1，并设置错误码

下面是代码演示：
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679664197105-29ebbc62-d046-4858-aaa9-526a916b652d.png#averageHue=%231e1e1d&clientId=ubc276710-d260-4&from=paste&height=363&id=uafb428dc&name=image.png&originHeight=545&originWidth=1008&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=39618&status=done&style=none&taskId=udcc9e070-b0aa-4302-843c-addfaf12ec6&title=&width=672)
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679664229986-09701d92-752a-4e45-b529-db059d8fa0cd.png#averageHue=%230f0b0a&clientId=ubc276710-d260-4&from=paste&height=273&id=ufcc8a657&name=image.png&originHeight=409&originWidth=935&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=43775&status=done&style=none&taskId=ua44d38a1-ce6c-40b5-bcad-c1952560583&title=&width=623.3333333333334)
运行后我们发现创建出来的文件是红色的，这是因为我们没有给文件设置初始权限，所以文件权限是乱码。
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679664294810-fcd69429-9092-4e0f-af03-b9c3cab088c7.png#averageHue=%231e1e1d&clientId=ubc276710-d260-4&from=paste&height=354&id=u48e9dc75&name=image.png&originHeight=531&originWidth=1073&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=41330&status=done&style=none&taskId=u59a640c7-de68-42f9-b734-93b46baed15&title=&width=715.3333333333334)
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679664338902-3b59142d-4acf-443e-b28e-22a252fb8862.png#averageHue=%23120d0c&clientId=ubc276710-d260-4&from=paste&height=276&id=u58d17786&name=image.png&originHeight=414&originWidth=860&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=42144&status=done&style=none&taskId=u309e3a45-3c41-4fcf-a9a2-88ded54dc1a&title=&width=573.3333333333334)
现在创建的文件就是正确的了。
文件权限 = 文件初始权限 &~umask
当然我们也可以在程序中自己修改umask。
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679664457528-940a4d75-aaf1-409b-a592-7ee63c81245b.png#averageHue=%23060505&clientId=ubc276710-d260-4&from=paste&height=224&id=ucd37f174&name=image.png&originHeight=336&originWidth=1405&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=19749&status=done&style=none&taskId=u355fea80-50d4-4788-98d5-ddcf4f853a6&title=&width=936.6666666666666)
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679664446226-78c76ab6-44d8-43e7-9068-57f04c3f6113.png#averageHue=%231e1d1d&clientId=ubc276710-d260-4&from=paste&height=379&id=uc189d231&name=image.png&originHeight=568&originWidth=1060&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=43901&status=done&style=none&taskId=ue63fe4b9-ef9c-4266-8ff3-3f9d2479722&title=&width=706.6666666666666)
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679664508873-9dc9c16e-ce77-4759-a3e1-966b9a440c47.png#averageHue=%230c0b0a&clientId=ubc276710-d260-4&from=paste&height=319&id=u53304502&name=image.png&originHeight=478&originWidth=922&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=47288&status=done&style=none&taskId=u6c1e3e25-7c28-40a0-b420-74190652591&title=&width=614.6666666666666)
并且我们修改umask后不会影响到外面的umask，因为我们修改的是子进程的umask

---

![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679665740066-4af9987a-5ceb-4aa4-829f-9d74db9e9b22.png#averageHue=%231e1d1d&clientId=ubc276710-d260-4&from=paste&height=233&id=u079a9f9d&name=image.png&originHeight=349&originWidth=1051&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=28322&status=done&style=none&taskId=ubea9a8fe-8852-4fde-9ae6-cb8abe2b243&title=&width=700.6666666666666)
写入的时候需不需要+1呢，+1其实就是将\0也写入到文件中。
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679665807004-ec43f15f-e6e5-49af-93ed-368049f30f57.png#averageHue=%230c0b09&clientId=ubc276710-d260-4&from=paste&height=128&id=u7a4bb0cd&name=image.png&originHeight=192&originWidth=700&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=12628&status=done&style=none&taskId=uf8faa27a-ee2d-44c9-b831-ef61cd0a490&title=&width=466.6666666666667)
打印看着文件内容没问题，但是当我们打开文件。
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679665847793-6e971d52-998b-4f56-b5d5-276c437249dc.png#averageHue=%23292826&clientId=ubc276710-d260-4&from=paste&height=153&id=uf6a71434&name=image.png&originHeight=229&originWidth=485&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=16486&status=done&style=none&taskId=u7da5523d-a88c-484c-8e97-a4b3893ed44&title=&width=323.3333333333333)
此时我们发现写入文件中的数据是存在乱码的，这就是因为我们写入了\0
并且，\0结尾是C语言字符串的规则，我们这里是系统调用，所以不需要在结尾添加\0。
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679665932245-7b5895a8-ea75-4381-9d60-a1d160a5ddef.png#averageHue=%230e0c0b&clientId=ubc276710-d260-4&from=paste&height=397&id=u80154cfd&name=image.png&originHeight=596&originWidth=833&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=54641&status=done&style=none&taskId=u420a9424-f163-4598-8bbd-45d8fe6f45c&title=&width=555.3333333333334)
我们写入的时候选择不将\0写入此时文件中的数据就是正确的了。
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679665902966-e513a331-094f-4cfc-96f3-16283b8a785f.png#averageHue=%232b2a28&clientId=ubc276710-d260-4&from=paste&height=136&id=u7c1adc29&name=image.png&originHeight=204&originWidth=438&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=12036&status=done&style=none&taskId=ubc643ff3-ba05-4f3e-aa79-51c317596d4&title=&width=292)

---

```c
#include<stdio.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<string.h>


#define FILE_NAME "test.txt"

int main()
{
    umask(0);
    //int fd = open(FILE_NAME,O_WRONLY|O_CREAT|O_TRUNC,0666);
    
    int fd = open(FILE_NAME,O_RDONLY);
    char buf[256] = {0};
    int num = read(fd,buf,sizeof(buf) - 1);
    buf[num] = 0;
    printf("%s",buf);


  // int cnt = 5;
  // while(cnt)
  // {
  //     char buf[64] = {0};
  //     sprintf(buf,"%s,%d\n","hello kisskernel",cnt--);
  //     write(fd,buf,strlen(buf));
  // }
    close(fd);
    return 0;
}

```
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679666366507-20999b72-73fe-4739-bbbb-a0857b538f8b.png#averageHue=%231e1e1d&clientId=ubc276710-d260-4&from=paste&height=480&id=u3e7e9916&name=image.png&originHeight=720&originWidth=1264&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=60380&status=done&style=none&taskId=u5c8e7293-b67d-41b0-8865-3594b656617&title=&width=842.6666666666666)
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679666344271-31d33510-e25d-4006-8ebc-535837d6a581.png#averageHue=%230e0c0b&clientId=ubc276710-d260-4&from=paste&height=149&id=ued53a7b7&name=image.png&originHeight=223&originWidth=649&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=15714&status=done&style=none&taskId=ue12a6761-3c72-4cbe-b12d-86212cee76b&title=&width=432.6666666666667)
所以库函数和系统调用之间的关系就是：
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679666460024-59310fdb-513c-4a19-8331-64d44501cf2f.png#averageHue=%23ecebeb&clientId=ubc276710-d260-4&from=paste&height=258&id=ud42fd624&name=image.png&originHeight=387&originWidth=626&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=11487&status=done&style=none&taskId=u39a8dea1-f24f-4c52-a542-e9d44680a0c&title=&width=417.3333333333333)
### 如何理解文件
文件操作的本质就是 进程  和  被打开的文件之间的关系
那么一个进程可以打开多个文件，系统中的进程很多那么系统中肯定存在很多被打开的文件，此时操作系统就需要对文件进行管理，也需要让进程知道内存中那么多被打开的文件那些才是属于他的，所以**操作系统必须对文件进行管理**，**管理的核心就是先描述再组织**
> 操作系统为了管理文件对文件描述，创建了对应的内核数据结构 struct file{....}这个内核数据结构内包含了文件的大部分属性。

我们使用open打开文件的时候返回的是文件描述符，那么这个文件描述符到底是什么我们可以打印出来看看：
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679667395702-220b668d-6292-4e67-bf46-d7504cd9152a.png#averageHue=%231f1f1e&clientId=ubc276710-d260-4&from=paste&height=583&id=u4baad44e&name=image.png&originHeight=875&originWidth=1031&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=87882&status=done&style=none&taskId=ub5abce59-0cd0-40d2-b032-3e5f95a6fba&title=&width=687.3333333333334)
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679667407914-ca1103a6-ccc9-4ac0-bdf3-76fa5d330a23.png#averageHue=%23050404&clientId=ubc276710-d260-4&from=paste&height=107&id=u5846cb11&name=image.png&originHeight=161&originWidth=632&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=5602&status=done&style=none&taskId=u72681bf0-8c67-46c8-90a1-8f227e1da9f&title=&width=421.3333333333333)
我们发现打印出来的文件描述符，是从3开始的小整数。

1. 为什么文件描述符从3开始
2. 为什么文件描述符是小整数
> 1.文件描述符为什么从3开始，因为系统会默认打开三个文件，分别是stdin，stdout，stderr
> 这三个文件流分别是标准输入，标准输出，标准错误。
> ![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679667590465-d4b3cffb-d393-4372-bfb9-ac509040aaff.png#averageHue=%231a1816&clientId=ubc276710-d260-4&from=paste&height=109&id=u4046e1e8&name=image.png&originHeight=164&originWidth=364&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=8344&status=done&style=none&taskId=uae1ff81a-91ab-4e1f-9818-60e0898b4ad&title=&width=242.66666666666666)
> 这三个文件流的类型都是FILE*的，上面说过库函数是对系统调用接口的封装，但是调用系统调用接口传的参数都是文件描述符，我们在C语言传的参数却是文件指针，由此可知文件指针FILE*这个FILE结构体内部一定包含了文件描述符。
> ![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679668011250-4467878f-39c8-4221-977f-3b1d56fcd35e.png#averageHue=%231f1f1e&clientId=ubc276710-d260-4&from=paste&height=369&id=u0a3641f2&name=image.png&originHeight=553&originWidth=775&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=48096&status=done&style=none&taskId=uac48401b-e64e-4a19-9fa8-87de193b14e&title=&width=516.6666666666666)
> ![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679668022581-6da9e864-031c-4cb6-acc1-bfd4fd2e8cac.png#averageHue=%23040404&clientId=ubc276710-d260-4&from=paste&height=171&id=u43851306&name=image.png&originHeight=257&originWidth=634&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=7219&status=done&style=none&taskId=u6a26a15b-bf03-4aca-a688-20e6d6bc8aa&title=&width=422.6666666666667)

> 2.为什么文件描述符是从0开始的小整数，这些小整数其实是数组下标。
> ![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1679668656685-7cb0774e-62f3-47f2-8765-8665f07fe6bb.png#averageHue=%23f9f9f9&clientId=ubc276710-d260-4&from=paste&height=697&id=u7f756e7f&name=image.png&originHeight=1046&originWidth=1990&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=56898&status=done&style=none&taskId=u8036c752-3b99-4854-bced-146e0f0768a&title=&width=1326.6666666666667)
> 在进程的pcb中有一个files_struct* files的指针，指向了管理该进程文件的内核数据结构，这个files_struct结构体内部有一个指针数组，每一个元素都是struct file*，所以当我们的文件被进程打开加载到内存之后，操作系统为文件创建了一个内核对象来管理该文件，将这个内核对象的地址放入了该进程的文件描述符表中。并将其在文件描述符表中的下标返回。

**所以文件描述符的本质就是下标。**
