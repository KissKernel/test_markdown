## Linux编译器——gcc/g++
### 程序编译背景知识
程序的编译分为四步：
**预处理**：主要完成的是，头文件展开，宏替换，注释删除，条件编译，预处理完的文件后缀是.i
**编译**：把C语言翻译成汇编语言，翻译完成后生成的文件后缀是.s
**汇编**：把汇编翻译成二进制目标文件，翻译完的文件就是可重定向二进制目标文件，后缀为.o
**链接**：将生成的二进制目标文件和C语言标准库里面的代码进行结合最后生成可执程序
### gcc指令
> **预处理：**
> **指令：gcc -E test.c -o test.i**
> **-E **编译器做完预处理的就停下来
> **-o **指明生成的临时文件的名称（test.i)
> 预处理完之后文件里面还是C语言。

> **编译：**
> **指令：gcc -S test.i -o test.s**
> **-S **编译器做完编译工作就停下来
> 编译过程将C语言翻译成了汇编语言

> **汇编：**
> **指令：gcc -c test.c -o test.o**
> **-c **编译器做完汇编过程就停下来
> test.o里面保存的就是二进制码，用vim打开时是乱码，od test.o 可以按二进制方式查看文件。

> **链接：**
> **指令：gcc test.o -o mytest**
> mytest就是可执行二进制文件（库+自己的代码生成的）
> 链接其实就是将库中的代码和自己的代码合并的过程。

指令演示：
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668301307042-826e09c1-bb8e-4822-999b-f52bc2886c1d.png#averageHue=%23262525&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=357&id=ueff047bb&margin=%5Bobject%20Object%5D&name=image.png&originHeight=536&originWidth=530&originalType=binary&ratio=1&rotation=0&showTitle=false&size=33462&status=done&style=none&taskId=udf9b3bac-192b-4096-b499-d7482e66466&title=&width=353.3333333333333)
**gcc -E test.c -o test.i**
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668301418100-8f47854f-15be-4b5a-b5a5-66801f73fa83.png#averageHue=%231f1e1e&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=627&id=u54af9ea2&margin=%5Bobject%20Object%5D&name=image.png&originHeight=941&originWidth=1421&originalType=binary&ratio=1&rotation=0&showTitle=false&size=74294&status=done&style=none&taskId=u382b25e3-73a2-4c5f-9dce-651ea51ddc4&title=&width=947.3333333333334)
预处理后的文件，从原来的几十行变成了八百多行，多出来的内容都是头文件stdio里面展开的内容，宏被直接替换到代码里面，条件编译也完成了。并且预处理后的文件还是C语言的。
**gcc -S test.i -o test.s**
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668301735405-624e598e-3c92-433f-a66e-7779dfb073ef.png#averageHue=%231f1e1e&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=697&id=ub575cddc&margin=%5Bobject%20Object%5D&name=image.png&originHeight=1045&originWidth=1042&originalType=binary&ratio=1&rotation=0&showTitle=false&size=84324&status=done&style=none&taskId=u6c0bc191-81e3-432d-b977-922140eaf69&title=&width=694.6666666666666)
编译后文件里的内容变成了汇编语言，movl和call等等这些都是助记符。
**gcc -c test.c -o test.o**
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668301852849-846e7660-c287-4966-8026-10a4d0d7ff4b.png#averageHue=%23292827&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=591&id=ue4c4aad2&margin=%5Bobject%20Object%5D&name=image.png&originHeight=887&originWidth=919&originalType=binary&ratio=1&rotation=0&showTitle=false&size=67557&status=done&style=none&taskId=uf3d21a41-e254-4f8a-8777-00243b21c8a&title=&width=612.6666666666666)
test.o是二进制目标文件，所以打开就是乱码
**od test.o**
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668301932255-a91c119a-9cc8-420c-b6db-8bc7ba52ea2e.png#averageHue=%2327221d&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=666&id=u13fff927&margin=%5Bobject%20Object%5D&name=image.png&originHeight=999&originWidth=1036&originalType=binary&ratio=1&rotation=0&showTitle=false&size=73016&status=done&style=none&taskId=uc71921c4-6bb0-43cf-b20e-91ba966f47b&title=&width=690.6666666666666)
**gcc test.o -o mytest**
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668302014256-fcd4252c-dacc-4f50-8cd9-ec76c7b8a6ea.png#averageHue=%23120e0d&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=327&id=u8a9fb1f0&margin=%5Bobject%20Object%5D&name=image.png&originHeight=490&originWidth=948&originalType=binary&ratio=1&rotation=0&showTitle=false&size=50953&status=done&style=none&taskId=u290c140a-b512-4958-910e-685d011af89&title=&width=632)
> 关于宏的问题，在Linux我们在编译的时候可以指定宏定义。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668302137985-688b488f-6095-4f3d-af63-8c31913e109d.png#averageHue=%232e2e2e&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=347&id=u62c994d7&margin=%5Bobject%20Object%5D&name=image.png&originHeight=521&originWidth=572&originalType=binary&ratio=1&rotation=0&showTitle=false&size=31545&status=done&style=none&taskId=u61d37fc9-3d3d-4e70-a9f8-4501c824ead&title=&width=381.3333333333333)
> 将代码中的宏定义删掉。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668302226955-7aa1274d-3cfe-4732-9688-508e0bb659c7.png#averageHue=%23130b0a&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=79&id=ub942e9e7&margin=%5Bobject%20Object%5D&name=image.png&originHeight=118&originWidth=1141&originalType=binary&ratio=1&rotation=0&showTitle=false&size=11657&status=done&style=none&taskId=u0be8009c-faef-4d96-8dc1-e02ae38c2d9&title=&width=760.6666666666666)
> gcc可以直接一步就完成预处理编译汇编的过程，后面的选项顺序可以颠倒，只要-o后面一定是目标可执行程序的名称即可。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668302346287-83a7549d-8e98-48c5-bf51-d9abca56e1d5.png#averageHue=%2317100e&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=95&id=uf42147d9&margin=%5Bobject%20Object%5D&name=image.png&originHeight=143&originWidth=968&originalType=binary&ratio=1&rotation=0&showTitle=false&size=14486&status=done&style=none&taskId=u4058e2db-6491-42fd-a961-7c1c7b9c4cf&title=&width=645.3333333333334)
> 此时没有宏定义MAX所以打印的就是这句话了。

### 动态链接和静态链接
链接的本质，就是调用库函数的时，代码和标准库是如何关联起来的。
链接这里存在动态链接和静态链接，同时也存在动态库和静态库。
> 动态链接的时候是在程序执行到库函数调用位置的时候跳转到库里面执行库的代码。这个过程称为**动态跳转**。
> **由此也引出一个问题，程序是如何知到库在哪？**
> - 编译器中有一个链接器，使用链接器就可以和库建立链接，调用库函数的时候直接跳转即可。
> 
**优点：形成的可执行程序小，可以节省资源（内存，硬盘，网络资源等）**

> 静态链接是将调用的库函数代码拷贝到你的代码中。
> 所以静态链接的时候不是产生关联，而是直接将调用到的库函数拷贝到程序中完成静态链接。
> **优点：不受库升级或者删除的影响**
> **缺点：**
> 1. **形成的可执行程序体积太大，会占用大量的内存，硬盘，网络资源。**
> 2. **出席那了大量重复的代码（不同的文件中调用了同一个函数，该函数被拷贝多次，代码膨胀）**

通过file指令可以查看可执行程序的详细信息
> **file [可执行程序名]**
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668304416015-47b147a6-385d-416f-840b-ee15c9a155a3.png#averageHue=%23090706&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=124&id=uec9cf0ed&margin=%5Bobject%20Object%5D&name=image.png&originHeight=186&originWidth=2325&originalType=binary&ratio=1&rotation=0&showTitle=false&size=23419&status=done&style=none&taskId=u343d7a74-7204-47a0-aa4a-23b4554e270&title=&width=1550)
> 这个词组翻译后就是动态链接的意思，前面的信息就是可执行程序的体系架构。
> **ldd [可执行程序名]**
> 通过ldd指令查看可执行程序所依赖的动态库列表（必须是动态链接的可执行程序，静态的查看不到）
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668304593925-caa8d36d-7869-429e-8fb9-cdc162013458.png#averageHue=%231a1211&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=87&id=u88265912&margin=%5Bobject%20Object%5D&name=image.png&originHeight=131&originWidth=983&originalType=binary&ratio=1&rotation=0&showTitle=false&size=15364&status=done&style=none&taskId=u052ff13b-e3d6-48b1-92a3-db1c56bdeb2&title=&width=655.3333333333334)
> 在这三个库中需要着重关注这个库。后面的地址就是调用的方法在那个位置，方便执行的时候进行跳转。![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668308949271-ac5901ca-ec26-4fbb-a8f4-ff47de573754.png#averageHue=%230f0d0c&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=43&id=ubd0ee537&margin=%5Bobject%20Object%5D&name=image.png&originHeight=65&originWidth=1159&originalType=binary&ratio=1&rotation=0&showTitle=false&size=10506&status=done&style=none&taskId=u7689ff2f-e0ff-4c4e-9a0b-08cf75f1d09&title=&width=772.6666666666666)

Linux下库的命名：
动态库：libXXXXX.so （以lib开头和.so结尾的库就是动态库，去掉lib和.so的后缀，剩下的就是库的名称。
所以上面圈出来的动态库的名称就是c，也就是c标准库。
静态库：libXXXXX.a （同样去掉前缀lib和后缀.a剩下的就是静态库的名称。）
> 下面来看一下动态链接生成的可执行程序和静态链接形成的可执行程序有什么区别。
> 使用指令：gcc test.c -o mytest-s -static就可指定编译器使用静态链接方式。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668309153198-a6a237ae-00e7-4dad-b30f-cf7ee41dddd7.png#averageHue=%23110c0b&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=211&id=ua441e755&margin=%5Bobject%20Object%5D&name=image.png&originHeight=316&originWidth=1125&originalType=binary&ratio=1&rotation=0&showTitle=false&size=36856&status=done&style=none&taskId=ucc101b51-3076-4547-8067-335281df44a&title=&width=750)
> 很明显静态链接生成的可执行程序的大小要比动态链接的大了100倍左右。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668309225448-7d0f5288-23c9-4c4f-af15-364e73d9c605.png#averageHue=%230e0b0a&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=69&id=u712827e1&margin=%5Bobject%20Object%5D&name=image.png&originHeight=104&originWidth=2328&originalType=binary&ratio=1&rotation=0&showTitle=false&size=18316&status=done&style=none&taskId=u99230b4b-e62b-4e69-b876-cd6627a29ae&title=&width=1552)
> 静态链接的文件不可以使用ldd
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668309271558-5b37bfff-d783-4502-b7c1-d29d6d08f44d.png#averageHue=%23110e0d&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=47&id=u5fd10445&margin=%5Bobject%20Object%5D&name=image.png&originHeight=71&originWidth=802&originalType=binary&ratio=1&rotation=0&showTitle=false&size=6503&status=done&style=none&taskId=ud86bf9dc-fa6d-495b-8088-d2403bfd5a5&title=&width=534.6666666666666)
> 爆出，该文件不是一个可执行程序的错误。

静态链接的时候如果系统没有静态库会报错
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668309515588-23c62b12-78f9-4383-877f-9da816ebe0c5.png#averageHue=%230e0c0b&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=65&id=u01550411&margin=%5Bobject%20Object%5D&name=image.png&originHeight=98&originWidth=1088&originalType=binary&ratio=1&rotation=0&showTitle=false&size=9798&status=done&style=none&taskId=u7fa7d25c-222b-4689-953d-f6e1acdd430&title=&width=725.3333333333334)
找不到这个静态库。
sudo yum install -y glibc-static下载C语言静态库。
sudo yum install -y libstdc++-static 下载C++的静态库
Linux系统中一般自带C语言的动态库，因为有很多指令都是用C语言写的，所以如果删除了C语言动态库很可能导致大多数程序都无法使用了。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668309920682-185169ce-8c6b-4611-8445-995d2f280417.png#averageHue=%230b0807&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=397&id=u3a0a3950&margin=%5Bobject%20Object%5D&name=image.png&originHeight=596&originWidth=2324&originalType=binary&ratio=1&rotation=0&showTitle=false&size=98513&status=done&style=none&taskId=u0c55e75f-b00c-4778-b9a5-261ed0de9f2&title=&width=1549.3333333333333)
> C语言的动态库虽然被很多程序都使用但是动态库只有一个，所有的程序都是共用c标准库的。所以动态库又被称为共享库。

windows系统下
动态库的后缀是.dll
静态库的后最是.lib
系统为了支持编程给用户提供了：

1. 标准库的.h文件（头文件里面是声明，告诉用户如何使用）
2. 标准的动静态库（库函数的实现）
> 在使用gcc进行链接的时候如果只有一个动态库或者一个静态库，那么就用这个唯一的库进行链接，如果两个库都有，那么默认使用动态库。

### Linux下运行其他语言
Linux并不是只能运行C语言或者C++，其他的语言比如java，python，php等等都是可以的。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1668310459167-b68e584c-462b-47bc-b28d-6a1353e2de4d.png#averageHue=%230d0b0a&clientId=u41642674-0d79-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=49&id=u357c67d2&margin=%5Bobject%20Object%5D&name=image.png&originHeight=73&originWidth=833&originalType=binary&ratio=1&rotation=0&showTitle=false&size=6002&status=done&style=none&taskId=ua12b2caf-5b60-4dd7-b922-21a1e8ab5ba&title=&width=555.3333333333334)
使用python解释器直接就可以运行，因为py是脚本语言，所以不需要编译，甚至只要给test.py这个文件加上可执行权限就可以直接运行。
还有shell脚本和php等等脚本语言都是可以直接运行的。
