文件操作必须要先打开文件，然后才能使用文件操作函数对文件进行操作。没被打开的文件都是在硬盘上呆着的，要对没有被打开的文件进行管理就需要用到文件系统。
了解文件系统之前我们需要知道我们的文件存储在硬盘上，那么硬盘是什么样子呢？
> 现在笔记本大多数使用的硬盘都是ssd了，ssd读写速度快，体积小重量轻，这些对于移动设备来说都是优点，ssd的缺点就是读写次数是有限了，读写次数达到上限之后ssd就会被击穿，数据会丢失。

> 而现在企业中使用的大多数是磁盘，hd，磁盘的优点是性价比高，也就是便宜，并且使用寿命长，数据保存的比较安全。缺点就是读写速度慢，因为磁盘是机械结构，并且还是外设，磁盘不能在上电状态进行移动或者受到撞击这些都可能导致磁盘损坏。

## 磁盘
### 物理结构
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1680781402105-2c326310-d5b9-400b-8831-ff5a9e91b945.png#averageHue=%23787366&clientId=udf01a547-687e-4&from=paste&height=537&id=udf3793f5&name=image.png&originHeight=805&originWidth=1694&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=897888&status=done&style=none&taskId=u24196602-ab74-4d71-91d7-63b975600f0&title=&width=1129.3333333333333)
磁盘的组成：

1. 盘面，一个盘有两个面每个面都是可以存储数据的。磁盘启动后盘面会按照一个方向高速旋转每秒几千转甚至上万转。
2. 磁头，磁头由磁头臂控制，磁头臂由一个马达控制使得磁头在盘面上左右摆动，每一个盘面都对应着一个磁头，磁头和盘面是没有接触的，但是靠得很近。所有盘面的磁头都是同步摆动的。
3. 马达，两个一个控制盘面旋转，另一个控制磁头在盘面上左右摆动。
4. 硬件电路 + 伺服系统，与文件系统配合管理磁盘上的数据。

磁盘的盘面转速要和磁头读写数据的能力相匹配。
> 盘面上是怎么会存储上数据的呢？因为计算机只能识别二进制，所以保存在磁盘上的数据也都是二进制数据，也就是在磁盘的盘面上是怎么表示二进制的。
> 我们可以将磁盘盘面分成无数个细小的存储单元，每个存储单元都是一块小的磁铁，磁头通电通过给盘面的存储单元上磁和退磁，使得存储单元通过有无磁性来表示二进制的01信号。

### 存储结构
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1680784178160-94d69969-5d56-4b27-a533-8ee117c5aa5c.png#averageHue=%23f5f4f0&clientId=udf01a547-687e-4&from=paste&height=391&id=u1d86e071&name=image.png&originHeight=586&originWidth=1819&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=623671&status=done&style=none&taskId=u72cf381d-d297-4c4c-88f7-1d9f4fb609b&title=&width=1212.6666666666667)
磁盘这里的存储结构主要是三个部分，磁头，柱面(磁道)，扇区。
一个盘面上有多个磁道，每个磁道有多个扇区由此每个盘面就被会分成多个扇区，每个扇区是512byte，虽然有的扇区的大小面积不同，但是为了方便管理一般将扇区的大小设置是统一的大小。
> 磁盘要对某个扇区写入必定要先找到这个扇区
> 磁盘定位扇区的方法是，先确定柱面(cylinder)，然后定位磁头(head)确定好在那个盘面，最后定位是几号扇区(sector)。所以定位扇区的方法叫做CHS定位法。

通过CHS定位法可以定位磁盘上的任意一个扇区或任意多个扇区。这是磁盘最基本的定位方法。
### 逻辑结构
文件系统要对磁盘进行管理，管理的核心是先描述再组织，所以肯定得对磁盘的存储结构进行抽象。
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1680788237105-f203056b-b3db-4f45-85b4-d25e9b92d3a7.png#averageHue=%23f6f5f5&clientId=udf01a547-687e-4&from=paste&height=255&id=u9dbbc172&name=image.png&originHeight=382&originWidth=1821&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=170328&status=done&style=none&taskId=u0b99de0f-a00c-4db7-af23-fc46363acb5&title=&width=1214)
磁盘的一圈圈磁道可以看作是磁带的那一卷，磁带可以通过拉伸把它拉直，那么我们抽象出来讲磁盘上的磁道拉直就变成了一个个扇区组合而成的数组。数组每个元素的大小都是512byte。
> 对这个逻辑结构再具体一点的抽象，假设磁盘的大小是500GB。
> ![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1680788944814-c2e7af0c-f055-4002-a004-7f534742146d.png#averageHue=%23f8f7f7&clientId=udf01a547-687e-4&from=paste&height=461&id=ue608bd2c&name=image.png&originHeight=692&originWidth=1830&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=211989&status=done&style=none&taskId=u07551439-cd94-4e46-838f-8538c08e7e1&title=&width=1220)
> 对磁盘抽象出来的数组可以看作是一个sector arr[n]每个数组元素都是扇区也就是512byte。
> 将整个数组划分成多个面，每个面可以分成0号到n号磁道，每个磁道又可以分成0号到n号扇区。
> 所以我们**对磁盘的管理就变成了对数组的管理**
> 只要知道了一个扇区的下标，就能推算定位这个扇区，在操作系统内部这个下标，也可以看作是地址被称为，LBA地址(逻辑块地址)。
> 例如：假设磁盘共有4个盘面，每个盘面有10个磁道，每个磁道有100个扇区，现在一个LBA地址是123，需要定位这个地址表示的扇区。
> CHS定位法，123/1000 = 0，所以在第0面，123/100 = 1所以在1号磁道，123 % 100 = 23，在第23号扇区。由此我们就通过了LBA地址定位到了具体的扇区。

为什么操作系统要对磁盘进行逻辑抽象，然后用LBA地址再转换成CHS地址然后进行定位？直接用CHS地址不是更方便？

1. 方便管理，将具体的硬件设备抽象成线性结构更方便操作系统进行操作和管理
2. 避免了操作系统的代码和硬件设备的强耦合关系

如果操作系统的代码使用的是CHS进行定位，如果计算机的硬盘变成了SSD此时操作系统的代码就失效了。所以操作系统为了更好的适应不同的硬件设备，需要以统一的视角看待所有的设备。这就需要对硬件进行抽象描述，然后进行管理。
> 扇区被称为块设备，也被看作是一个文件，因为Linux下一切皆文件。
> 扇区是块设备的原因：
> 1. 单位不是字节
> 2. 以512byte为单位

## 文件系统
### 文件系统是什么
Linux文件系统中的文件是数据的集合，文件系统不仅包含着文件中的数据而且还有文件系统的结构，所有Linux 用户和程序看到的文件、目录、软连接及文件保护信息等都存储在其中。
> 简单的说文件系统就是一个管理磁盘上未被打开的文件，他们的数据存在哪里，存储数据的时候该怎么划分空间，每个分区的空间使用了多少，创建的新文件该存放到哪里等等关于磁盘上的文件操作的决策问题。

文件系统实际就是一个管理系统。
> 我们在Linux中使用ls -l显示出来的文件除了文件名还包含文件属性
> ![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1680792564590-0449e19a-c8e0-4274-bc4e-354d45bd11e0.png#averageHue=%230d0c0a&clientId=udf01a547-687e-4&from=paste&height=195&id=ua6f05723&name=image.png&originHeight=292&originWidth=912&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=34541&status=done&style=none&taskId=u25bf3bfe-b09d-4840-a83b-45df160ad84&title=&width=608)
> 第一个字母表示文件类型。
> 第2到9个字母分为三组，对应owner ，grouper， other的读写执行权限
> 第二列的数字表示硬链接数
> 第三列是owner
> 第四列是grouper
> 第五列是文件大小
> 第六列是最近修改时间

我们能看到这些信息是因为文件系统从磁盘上将文件信息读取了上来
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1680792837044-80d6781a-03e4-4625-b252-83939eba82c1.png#averageHue=%23e8e8e7&clientId=udf01a547-687e-4&from=paste&height=531&id=ud72a2edb&name=image.png&originHeight=796&originWidth=965&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=137475&status=done&style=none&taskId=uf8247063-6011-4672-bea3-442261fa84a&title=&width=643.3333333333334)
而这些文件信息，包括文件内容都是文件系统进行管理的。
### 文件系统读取磁盘的单位
> 磁盘每次访问的基本但是是扇区，512byte但是这个单位依旧很小，操作系统的文件系统读取磁盘一般每次是1kb，2kb，4kb为基本单位，其中4kb使用的比较多。所以哪怕你要修改磁盘上文件的1bit数据，文件系统也会一次将4kb读入内存，修改完成后再写回磁盘。
> 为什么文件系统读取磁盘数据每次是以4kb呢？
> 这里涉及到局部性原理，就是你访问了这个文件的1bit数据你下次就很有可能访问这1bit附近的数据，文件系统一次读入了4kb，那么下次访问的数据很大可能性就在这4kb中，因此就能减少IO次数，提高数据命中率。进而提高操作系统的效率。

> 还有一个原因是，内存是以4kb为单位进行划分空间的 一个个划分区被称为 —— 页框
> 磁盘中的文件，可执行程序，也按照4kb划分成一个个块，方便载入内存，这些块被称为 —— 页帧

**文件系统每次从磁盘中读取1kb还是2kb亦或者是4kb这个数据是可以修改的，但是要对这个数进行修改就要重新编译操作系统**

---

### 文件系统怎么管理
文件系统怎么对磁盘抽象出来的逻辑结构进行管理。
一个磁盘最小也有500GB，直接对500GB进行管理是很复杂的，所以可以采用分治思想，将大问题划分成小问题解决。
500GB可以分成两个100GB和两个150GB大小的分区。只要管理好一个分区，那么其他分区也使用同样的方法管理即可。但是直接管理100GB大小的分区还是很困难，所以将100GB再分成多个组，每组大小是5GB现在只要管理好一个组，其他的组使用同样的方法就能管理好。
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1680793957929-68a0ed06-8079-490f-a4c3-95628b111cab.png#averageHue=%23fefefe&clientId=udf01a547-687e-4&from=paste&height=577&id=uca295e88&name=image.png&originHeight=866&originWidth=1758&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=48368&status=done&style=none&taskId=u3ec9433f-26b2-4378-b0ff-1084ed48693&title=&width=1172)
这里的分区类似Windows中的C盘D盘E盘这种分盘
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1680794189480-c6a4eb82-6e5d-49c8-8209-2a4f6d73e701.png#averageHue=%23ededed&clientId=udf01a547-687e-4&from=paste&height=281&id=u2314e660&name=image.png&originHeight=422&originWidth=1394&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=132688&status=done&style=none&taskId=uc007220c-c823-44bf-b755-0ef23852bca&title=&width=929.3333333333334)
这个图是Linux ext2文件系统的，磁盘文件系统图，磁盘是典型的块设备，磁盘分区被划分出来了一个个的Block，Boot Block和n个Block group。一个Block的大小是格式化的时候确定的且不可更改。
Boot Block块是系统启动块，当计算机开机的时候会先加载运行Boot Block块将操作系统的基本服务跑起来。所以Boot Block块的大小是确定的。
而每个Block Group组又被分成了以下几部分：

1. Super Block：存放文件系统本身的结构信息。记录的信息主要有：bolck 和 inode的总量， 未使用的block和inode的数量，一个block和inode的大小，最近一次挂载的时间，最近一次写入数据的时间，最近一次检验磁盘的时间等其他文件系统的相关信息。如果Super Block的信息被破坏，可以说整个文件系统结构就被破坏了。
> 但是这么重要的Super Block为什么存放在group组中呢？为什么不在分区中单独开辟一个块来保存呢？其实分区中的每个group他们的结构都是一样的，但是有的group会有Super Block有的就没有。所以一个分区内存在多个group里面都是具有Super Block的，这些副本保证了当一个group中的Super Block损坏的时候，只有一个group的数据丢失，想要恢复只需要将其他group组中的Super Block拷贝就可以了。

2. GDT，Group Descriptor Table：块组描述符，描述的是块组的宏观的属性信息
3. 块位图（Block Bitmap）：Block Bitmap中记录着Data Blocks中哪个数据块已经被占用，哪个数据块没有被占用 
4. inode位图（inode Bitmap）：每个bit位表示一个inode是否空闲可用
5. inode Table:存放文件的几乎所有属性 如：文件大小，所有者，最近修改时间等等，但是文件名并不在inode中。并且每个文件都有一个inode，inode的大小是固定的。inode为了彼此区分每个inode都具有自己的ID。
> 使用ls -li可以将每个文件的inode 的ID显示出来。
> ![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1680795096215-a45d6247-dc11-4601-994e-a9fbf10c588e.png#averageHue=%230f0d0b&clientId=udf01a547-687e-4&from=paste&height=196&id=u4a03e16e&name=image.png&originHeight=294&originWidth=1022&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=38488&status=done&style=none&taskId=u24ccfefb-7d96-4a3c-9719-2d6cccb5d6a&title=&width=681.3333333333334)

6. data Blocks(数据区)：存放文件内容，数据区内部也是被划分成了许多块，每个块的大小固定，一个文件的内容占用多少个块是不确定的。

在一个目录下查找文件统一使用的都是inode，因为inode在一个分区内是唯一的。但是我们平时查找文件用的是文件名并不是inode。这是因为**目录的data Blocks中存放的内容是文件名和inode的映射关系。**通过文件名和inode的映射就能查找到文件。
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1680795908867-84622e69-bb23-484e-adbf-4daef7449340.png#averageHue=%23fefefe&clientId=udf01a547-687e-4&from=paste&height=417&id=u5548fd9a&name=image.png&originHeight=626&originWidth=355&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=25162&status=done&style=none&taskId=uac8951de-90a7-46f7-ac87-d63e20c6b4c&title=&width=236.66666666666666)
inode中保存了文件的属性信息，同时还保存了文件内容在data Blocks中的位置。保存在blocks数组中。
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1680796601337-95e68b7e-6c40-4160-896d-5b4fa00da153.png#averageHue=%23fefefe&clientId=udf01a547-687e-4&from=paste&height=510&id=ue394bb99&name=image.png&originHeight=765&originWidth=1620&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=24714&status=done&style=none&taskId=ubee3777d-8f97-4de3-a491-0dbed757fc7&title=&width=1080)
blocks数组中的0-11下标元素指向的都是文件内容所占用的数据块的编号。但是如果文件内容很大，还是保存不下，此时12下标元素指向的数据块内部存放的就不是文件内容了，而是一些数据块的编号，这些数据块里面存放的才是文件内容。
如果此时数据块还不够用，那么13号下标的元素指向的数据块内存放的是其他数据块的编号，这几个数据块内部又是许多数据块，这些第二次延申的数据块内存放的才是文件的内容。
14号下标的道理也是类似的。
**创建一个文件时要进行的操作**

1. 存储属性，需要在inode Table中添加一个新的inode记录文件的属性信息，然后将inode Bitmap的对应位置置1.
2. 存储数据，将文件的内容数据拷贝到data Blocks中的空闲位置
3. 记录存储位置，将文件数据拷贝到的data Blocks中占据的数据块的编号记录到inode中
4. 添加文件名到目录，将文件名和inode的映射关系添加到当前目录的data Blocks中

![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1680830310036-ac997029-4f31-4ada-8e26-1569bcf61eb4.png#averageHue=%23f2f0ed&clientId=udf01a547-687e-4&from=paste&height=373&id=ucf7fbc94&name=image.png&originHeight=559&originWidth=1359&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=245230&status=done&style=none&taskId=ud124b2c8-2012-40f9-8704-62c60faad72&title=&width=906)
这个图展示的就是文件添加的过程。先找到空闲inode节点，然后找到空闲的数据块，将使用的数据块编号填入inode，最后将inode和文件名的映射添加到目录的数据块中。
## 软硬链接
### 软链接
```c
ln -s test.c soft_link.s
soft_link.s就是添加的软链接的文件名
```
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1680831255181-679e33e8-f46b-43c7-a9c9-ce4aa247752d.png#averageHue=%230d0b0a&clientId=udf01a547-687e-4&from=paste&height=152&id=u0ae51723&name=image.png&originHeight=228&originWidth=1085&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=27390&status=done&style=none&taskId=ua11a01a9-00f4-466a-a4fc-d9f0af2913a&title=&width=723.3333333333334)
我们可以看到软链接文件的inode是和原来的文件不同的，也就是说软链接文件是创建了一个新文件。
当我们将软链接文件链接上的文件删掉，此时软链接文件soft_link.s就会飘红显示错误。
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1680831460143-6cdfed9e-fe38-4ce2-86cb-5852e5a33e0e.png#averageHue=%230d0908&clientId=udf01a547-687e-4&from=paste&height=154&id=u296767c6&name=image.png&originHeight=231&originWidth=1121&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=23856&status=done&style=none&taskId=ubbd5db69-0fa7-4067-9e26-16e9f6c1737&title=&width=747.3333333333334)
错误的原因就是找不到被链接文件。但是其实hard_link.c文件是和test.c文件相同的，他们的inode都是相同的，但是此时软链接文件还是找不到被链接文件，这说明**软链文件的内容保存的并不是被链接文件的inode，而是保存了被链接文件的路径。**
**Linux中绝对路径可以表示唯一文件。**
所以当我们重新添加一个名为test.c的文件，就算新文件和旧文件的inode不同软链接文件也不会报错了。![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1680831663642-bbb123dc-b185-44a2-94c5-a86351a272d5.png#averageHue=%230c0a09&clientId=udf01a547-687e-4&from=paste&height=158&id=uad4ec5ff&name=image.png&originHeight=237&originWidth=1072&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=26739&status=done&style=none&taskId=ua9e8dcf8-8e43-446e-bd91-b075b36667c&title=&width=714.6666666666666)
这就像是狸猫换太子。实际软链接，链接上的文件内容早就不一样了。
> 软链接就类似于Windows中的快捷方式，可以将路径比较深的文件做一层软链接，这样每次访问就不需要每次进入路径深处了。

> ![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1680831936672-7aa18fbd-137b-40fa-85d6-e25e128593fc.png#averageHue=%230c0a09&clientId=udf01a547-687e-4&from=paste&height=377&id=u698ec6e1&name=image.png&originHeight=566&originWidth=1410&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=75445&status=done&style=none&taskId=u1b55a273-44f3-4fa1-8714-f544c5a149a&title=&width=940)
> 创建了很深的路径，然后创建了一个文件，让软链接文件连接到该文件，此时我们不需要进入目录也可以调用这个创建的文件。

### 硬链接
```c
ln test.c hard_link.c
hard_link.c 就是添加的硬链接的文件名
```
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1680830668352-f04526d9-ea53-41a6-8cad-3ec925a43217.png#averageHue=%230d0b0a&clientId=udf01a547-687e-4&from=paste&height=112&id=uf97ce162&name=image.png&originHeight=168&originWidth=917&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=17049&status=done&style=none&taskId=ue705c466-4358-44d8-8401-154ba758fdc&title=&width=611.3333333333334)
现在hard_link.c就是test.c的硬链接
硬链接文件的inode是和被链接文件的inode一样的。
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1680830731424-4c9247b1-50ec-4b6c-a117-383960cabd0e.png#averageHue=%230d0b0a&clientId=udf01a547-687e-4&from=paste&height=111&id=uaea5a76d&name=image.png&originHeight=166&originWidth=917&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=16927&status=done&style=none&taskId=uf5bfc8c2-bacd-4492-878c-56a353ca117&title=&width=611.3333333333334)
文件内容也都是相同的，因为inode相同就是说明没有新文件被创建，hard_link.c和test.c文件其实是同一个文件。
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1680830799158-50b30bc6-2ea6-4d7b-bc80-7a00684b4c76.png#averageHue=%23050404&clientId=udf01a547-687e-4&from=paste&height=337&id=u44120cf3&name=image.png&originHeight=505&originWidth=859&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=25302&status=done&style=none&taskId=u33675452-0f48-4d7f-bf4a-913866a6597&title=&width=572.6666666666666)
当我们对其中的一个文件进行修改，另一个文件也会同步修改。
> 硬链接添加的原理就是在目录里添加了一个新的文件名和test.c文件inode的映射关系。

软硬链接的区别就是有没有创建新文件。软链接是创建了一个新的软链接文件，硬链接只是在目录里添加了一条映射关系，所以有硬链接没有自己独立的inode。
**软硬链接的区别：是否具有独立的inode。**
硬链接每次其实还是用到了一个引用计数的原理，我们对一个文件每添加一个硬链接，这个文件的硬链接数就会+1，这个硬链接数就是引用计数。当我们要删除这个文件的时候并不会直接删除，系统会先将硬链接数-1，直到硬链接数减为零之后才会真正的删除该文件。
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1680832711873-54765e07-3c1b-4dac-b330-9ecaa37e4650.png#averageHue=%23100d0c&clientId=udf01a547-687e-4&from=paste&height=136&id=u04a2f727&name=image.png&originHeight=204&originWidth=749&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=20793&status=done&style=none&taskId=u8d322e99-82a6-470d-a711-bb0619861f3&title=&width=499.3333333333333)
我们创建一个新的目录的时候发现该目录的硬链接数是2，此时除了newdir之外应该还有一个硬链接文件，这个文件就是newdir目录中隐藏的.文件
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1680832821526-5634f8ab-146f-42ea-a37c-6bd8a419762f.png#averageHue=%23110e0d&clientId=udf01a547-687e-4&from=paste&height=113&id=uec4ea355&name=image.png&originHeight=169&originWidth=691&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=15626&status=done&style=none&taskId=ue689ab17-68b1-45f5-8d0a-669127a2ee6&title=&width=460.6666666666667)
所以test2的硬链接数是3也可以解释了，在test2目录内存在.目录指向当前test2，同时test2问价你中还存在..目录指向上一级的目录，此时就有三个了。
test1目录的硬链接数是4，此时除了目录内的.目录隐藏文件，在test2和newdir中各自存在一个..目录硬链接上test1，所以此时test1目录的硬链接数就是4了。
> 但是当我们自己给目录创建硬链接的时候会出现错误
> ![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1680833226877-b2ba849a-1920-42dd-aca6-e95a76a0a462.png#averageHue=%23100c0b&clientId=udf01a547-687e-4&from=paste&height=234&id=u33e0048f&name=image.png&originHeight=351&originWidth=913&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=37804&status=done&style=none&taskId=uea206854-bb79-41da-96d8-6f2c3b7c288&title=&width=608.6666666666666)
> 错误信息是不允许给目录创建硬链接，所以只有操作系统才能给目录创建硬链接，而用户不可以，主要是为了防止你对目录创建硬链接后会导致查找目录时出现无限循环。

**文件的三个时间**
![image.png](https://cdn.nlark.com/yuque/0/2023/png/29079911/1680833404597-c9dbb562-20f1-4d8f-8abe-6a0b960ce53a.png#averageHue=%230d0b0a&clientId=udf01a547-687e-4&from=paste&height=209&id=u69760cf2&name=image.png&originHeight=313&originWidth=1179&originalType=binary&ratio=1.5&rotation=0&showTitle=false&size=35078&status=done&style=none&taskId=u9f49e865-0713-4695-b062-ca002714734&title=&width=786)
Access是最近访问时间，并不是每次访问都会修改，只有访问达到一定次数或者在一段时间内连续访问才会被更新
Modify是文件内容最近修改时间，文件内容改变了Modify时间就会更新
Change是文件属性最近修改时间，文件属性修改，包括文件内容修改都会更新，因为文件内容修改了文件的大小也会跟着变化，文件的大小也是属性。
