二次修错于date：2024 3 14
> 从文档使用到模拟实现。一起进入STL的学习吧！

首先，学习STL的最好的办法就是看文档，这里有两个网址一个是[C++官网](https://zh.cppreference.com/w/%E9%A6%96%E9%A1%B5)，另一个就是[cpluscplus](https://cplusplus.com/)官网的文档全，但是比较乱。
## string的文档大纲![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656346023534-8cae1d0c-3187-455a-aa48-45cb6fe0f79d.png#averageHue=%23f5f4f3&clientId=u915188c0-c501-4&from=paste&height=3079&id=ueba95f16&originHeight=4619&originWidth=1744&originalType=binary&ratio=1&rotation=0&showTitle=false&size=845843&status=done&style=none&taskId=u091c45b6-f3e2-4de9-8fa4-1bc3bed5381&title=&width=1162.6666666666667)
## string类
使用string类必须包含头文件<string>，命名空间std可以选择展开和不展开。不展开需要使用域作用限定符指定访问std中的string。
> string类是经过typedef的，实际类名是basic_string<char>
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656384489785-7e622703-0ead-4331-b3aa-bedbe321433e.png#averageHue=%23d5be85&clientId=u915188c0-c501-4&from=paste&height=157&id=u5b1b475d&originHeight=236&originWidth=1256&originalType=binary&ratio=1&rotation=0&showTitle=false&size=25886&status=done&style=none&taskId=u91446026-4521-4613-9adb-49cd25b5de0&title=&width=837.3333333333334)
> 是将basic_string这个模板，用char来实例化出来的类
> 也就是说明了呢，这个类是用来存放char类型的变量的。那么这个string还可以用来存放其他类型的数据吗，当然可以，比如wchar_t，wchar_t是占用两个字节的。![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656384626142-2ea629a2-128b-49eb-a5a7-43f0efc96e45.png#averageHue=%23f2f1f0&clientId=u915188c0-c501-4&from=paste&height=203&id=ub1c21097&originHeight=305&originWidth=944&originalType=binary&ratio=1&rotation=0&showTitle=false&size=45000&status=done&style=none&taskId=u456184d5-a9bb-4b8e-864a-71b519f9d08&title=&width=629.3333333333334)
> 这里的wstring就是basic_string<wchar_t>模板类，这里的wchar_t是宽字节的意思，就是两个字节，有时候因为字符串不一定都是使用ASCII编码的字符串，比如Unicode，UTF-8编码需要使用多个字节，就出现了wchar_t，char16_t，char32_t等类型。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656384764448-868f9d60-f35e-4a39-9d08-83f3e667421f.png#averageHue=%23eeedea&clientId=u915188c0-c501-4&from=paste&height=96&id=u0484b29c&originHeight=144&originWidth=621&originalType=binary&ratio=1&rotation=0&showTitle=false&size=17041&status=done&style=none&taskId=u70bdd08f-08d0-4900-92ad-98ac907f7f4&title=&width=414)


---

## 字符编码知识
> 中文一般采用的是gbk编码规则。其他还有utf-8，utf-16，utf-32等等

> Unicode（又称统一码、万国码、单一码）是计算机科学领域里的一项业界标准，包括字符集、编码方案等。Unicode是为了解决传统的字符编码方案的局限而产生的，为每种语言中的每个字符设定了统一并且唯一的二进制编码。

![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656385651697-cb5d6899-7e94-4cc6-ae8e-b3ca406abda3.png#averageHue=%23cbe6cc&clientId=u915188c0-c501-4&from=paste&height=409&id=u3217f8ca&originHeight=613&originWidth=726&originalType=binary&ratio=1&rotation=0&showTitle=false&size=46214&status=done&style=none&taskId=u76a278f4-474a-4780-b198-459174b69ae&title=&width=484)
在内存中一个汉字占用两个字节，同时字符在内存中都是使用数字存储的，也就是对应着编码表，不同的语言有不同的编码表。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656385756584-1a278390-0c4c-4b9f-a317-816fc41c4f21.png#averageHue=%23d4a975&clientId=u915188c0-c501-4&from=paste&height=265&id=u6eed3046&originHeight=398&originWidth=823&originalType=binary&ratio=1&rotation=0&showTitle=false&size=26097&status=done&style=none&taskId=uecfd4dc9-1a6b-4099-8802-cb1ef549f1b&title=&width=548.6666666666666)
-42，-48一起组成了“中”，-71，-6一起组成了“国”，那么如果更改[3]的值会发生什么呢？
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656386078669-ff970916-ceea-4c39-b217-682b17ad238f.png#averageHue=%23cae6cc&clientId=u915188c0-c501-4&from=paste&height=303&id=u412f2c4f&originHeight=454&originWidth=850&originalType=binary&ratio=1&rotation=0&showTitle=false&size=54562&status=done&style=none&taskId=u75a74d72-d528-45e5-9b99-819c0928be9&title=&width=566.6666666666666)
> 在中文编码里面近音字是被编在一起的，这可以用到净网行动的不良词汇检查，只要屏蔽掉这个字以及这个字的同音字，就可以一定程度上避免不良词汇出现了。

## string类构造函数
> stl的文档里面函数模块分为三个部分，下面用构造函数模块来举例

### 函数声明部分
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656386346897-076c0543-aac5-4b90-b894-f64d02c30e43.png#averageHue=%23f5fdf4&clientId=u915188c0-c501-4&from=paste&height=325&id=u364477ff&originHeight=487&originWidth=1672&originalType=binary&ratio=1&rotation=0&showTitle=false&size=93939&status=done&style=none&taskId=u8ddb7439-0cb8-45c1-b148-469702065da&title=&width=1114.6666666666667)
> 这里声明了C++98中的七个构造函数，但是实际上经常用到的也就是几个，并不是全都会用。
> 第一个就是默认初始化，对象里面默认放的空字符串，并且会开辟一块空间。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656386519785-87515c1d-3d4a-42ec-a3d3-41e9e2ed478c.png#averageHue=%23cbe6cc&clientId=u915188c0-c501-4&from=paste&height=257&id=u31c3fd58&originHeight=386&originWidth=1428&originalType=binary&ratio=1&rotation=0&showTitle=false&size=61188&status=done&style=none&taskId=u678f3d43-1646-48a0-b98e-eaaedce3d1e&title=&width=952)
> 定义对象的时候不传参调用的就是默认构造，开辟15个空间。
> **第二个**，就是拷贝构造，用一个string对象来构造一个新的对象。
> **第三个**，就是用一个对象的部分来构造新对象，第二个参数pos就是position位置，len就是长度，从pos位置开始的len长度的字符串来构造对象。
> 第四个，就是用一个C语言的字符串来构造一个对象。
> 第五个，就是用一个C语言字符串的从第一个字符开始到第n个字符构造一个对象。
> 第六个，就是用n个字符c来组成一个字符串
> 第七个，就是迭代器，使用迭代器区间来构造一个新的对象。
> 演示：![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656387199842-d111f763-5a99-4e88-807b-0db9ae08dfec.png#averageHue=%23cce6ce&clientId=u915188c0-c501-4&from=paste&height=313&id=u98394fe7&originHeight=469&originWidth=1753&originalType=binary&ratio=1&rotation=0&showTitle=false&size=107846&status=done&style=none&taskId=uadd1ead5-d4b9-42bc-98a9-dd2bbf2fb59&title=&width=1168.6666666666667)
> 构造对象的时候也可以这样写：

```cpp
void Teststring_3()
{
	string s1 = "hello kisskernel";
}
int main()
{
	Teststring_3();
	return 0;
}
```
> 因为构造函数中存在使用C语言字符串构造对象的单参函数，所以这段就是进行了类型转换，先将字符串构造一个临时对象，然后再进行拷贝构造，经过编译器优化之后就变成了直接那这个字符串进行构造。

### 函数功能参数说明部分
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656387260917-ccc409c8-adcb-48fb-9c12-68b15b6c364c.png#averageHue=%23f7f5f5&clientId=u915188c0-c501-4&from=paste&height=706&id=u53d2907e&originHeight=1059&originWidth=1764&originalType=binary&ratio=1&rotation=0&showTitle=false&size=193820&status=done&style=none&taskId=u4311324c-82d2-4c84-a5ce-2fd33708761&title=&width=1176)
这里对于每个构造函数都进行了解释，以及特殊情况下的执行状况，比如第三个构造函数，用一个对象那个的pos位置开始的len长度的字符串来构造对象，如果这个len太长了会怎么样，不会报错，会直接到end of str
如果这个len你输入的是npos也是直接到字符串结尾，这里的npos是-1，但是因为是无符号的-1，所以这个就变成了unsigned int 的最大值42亿多![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656387401027-44e20eea-b23d-4957-8652-8c34fee828a2.png#averageHue=%23d4c283&clientId=u915188c0-c501-4&from=paste&height=75&id=ua4a2f36f&originHeight=112&originWidth=517&originalType=binary&ratio=1&rotation=0&showTitle=false&size=10074&status=done&style=none&taskId=ua5afbea3-8940-4622-a57e-662e0436d8b&title=&width=344.6666666666667)
后面的8，9就是C++11新引入的函数。
**参数含义说明部分**
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656387509802-e3049e26-3edf-4beb-bd32-52a785f9f4cf.png#averageHue=%23f5f3f2&clientId=u915188c0-c501-4&from=paste&height=737&id=u177fe7bf&originHeight=1105&originWidth=1674&originalType=binary&ratio=1&rotation=0&showTitle=false&size=175097&status=done&style=none&taskId=ue742b650-228a-454c-ad1d-0416fc1c4c9&title=&width=1116)
### 返回值说明部分
> 这个部分就是对这些函数的返回值进行解释说明，因为构造函数无返回值，所以这里没有返回值部分，我用一个赋值运算符的重载来演示：
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656387628954-e6a29d40-34bf-4836-b2b3-1db5a6dded17.png#averageHue=%23f7f6f6&clientId=u915188c0-c501-4&from=paste&height=836&id=u94f582cc&originHeight=1254&originWidth=1686&originalType=binary&ratio=1&rotation=0&showTitle=false&size=165742&status=done&style=none&taskId=ue260a06b-c4d3-4256-8921-4314accc6cd&title=&width=1124)
> 这里就是返回值，可以看到返回的*this，使用的是引用返回。

## string类常用函数
### operator+=
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656388384216-a4b0217c-0e9e-4925-af39-e34049da62d7.png#averageHue=%23ccc683&clientId=u915188c0-c501-4&from=paste&height=201&id=u4c468e44&originHeight=302&originWidth=1723&originalType=binary&ratio=1&rotation=0&showTitle=false&size=53905&status=done&style=none&taskId=ua2ee18f7-6a65-4003-93c6-66a017be407&title=&width=1148.6666666666667)
首先是重载的赋值运算符，有三种用法，第一就是赋值一个string对象，第二就是复制一个字符串，第三就是赋值一个字符，注意不管赋值的是那个赋值之后这个被赋值的对象里面只有赋值的内容，但是比如先赋值一个字符串，空间不足扩容了，然后再赋值一个字符，这时候对象里面只有字符，但是开辟的空间并不会缩小。
```cpp
void Teststring_3()
{
	string s1 = "hello kisskernel";
	string s2;
	s2 = "hello";
	s2 = s1;
	s2 = 'c';
}
```
> 这里的s2最后是c，但是开辟的空间是32

### size和lenth
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656398366004-fc3b6e4f-88f4-4a7f-a9f2-859d1ea8eae0.png#averageHue=%23f4f3f3&clientId=u915188c0-c501-4&from=paste&height=337&id=uebcfe04e&originHeight=506&originWidth=1691&originalType=binary&ratio=1&rotation=0&showTitle=false&size=102435&status=done&style=none&taskId=u7851ba85-923d-4678-94d1-792a49da286&title=&width=1127.3333333333333)
> 这两个函数是返回这个字符串的长度，返回的是一个无符号数。size和length的功能是完全一样的，为什么会出现这两个功能一样的函数呢？因为string的诞生时间比stl要早，这也就是string为什么没有被受尽stl里面的原因，早期字符串类当然可以用length来表示长度，但是换了其他的类比如map就不可以用length了。于是为了和其他的stl类统一，就添加了size。

> 这里的max_size返回的是21亿多，官方解释说是返回字符串能到达的最大长度。实际上是没什么用的。


### append、push_back、和operator+=
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656398877349-9345c04a-111b-4292-9e4a-80fa2bb2ee7e.png#averageHue=%23f6f5f5&clientId=u915188c0-c501-4&from=paste&height=141&id=u7c5ebf00&originHeight=211&originWidth=1671&originalType=binary&ratio=1&rotation=0&showTitle=false&size=31819&status=done&style=none&taskId=uef4a3d7a-d480-4a3e-8be5-52593fdb252&title=&width=1114)
> 首先push_back就是在字符串末尾插入一个字符。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656399010844-a227f175-bfa0-4bd6-ad9a-0d539c1d402d.png#averageHue=%23cbc179&clientId=u915188c0-c501-4&from=paste&height=371&id=ubfed90d4&originHeight=557&originWidth=1944&originalType=binary&ratio=1&rotation=0&showTitle=false&size=111512&status=done&style=none&taskId=u481251ff-43fb-4223-bd35-03f36577cde&title=&width=1296)
> 来看append，这里给出了六种函数，常用的其实只有第一种和第三种
> 简单介绍一下：
> 第一就是在字符串末尾插入一个string类的对象
> 第二就是在尾部插入一个string的subpos位置（下标）的字符开始向后sublen长度的字符串。如果len长度很长，那就直接从pos位置到结尾全插入，第三个参数是npos或者不写，也是一样的。
> 第三就是在尾部插入一个字符串。
> 第四个就是在尾部插入一个字符串的一部分，就是从这个字符串s开始的向后数n个字符。
> 第五个就是在后面插入n个字符c
> 第六个就是迭代器插入，参数是迭代器的范围。

![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656406815227-8f5b3408-96dc-4f73-9217-5cbc434e667c.png#averageHue=%23cce6ce&clientId=u915188c0-c501-4&from=paste&height=403&id=ub939fd6d&originHeight=604&originWidth=2036&originalType=binary&ratio=1&rotation=0&showTitle=false&size=123980&status=done&style=none&taskId=u3f12d04b-befd-4358-932e-ebb49f398fb&title=&width=1357.3333333333333)

---

但是实际上使用最多的是operator+=；
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656406366053-d7b8f781-f834-4259-abe9-ef892d7421aa.png#averageHue=%23d4bb7d&clientId=u915188c0-c501-4&from=paste&height=303&id=uc0521489&originHeight=455&originWidth=1718&originalType=binary&ratio=1&rotation=0&showTitle=false&size=77479&status=done&style=none&taskId=udcf50918-9559-4e60-8728-a170bafcb72&title=&width=1145.3333333333333)
> 这里的+=就是在字符串的尾部连接上，可以是一个string类的对象，也可以是一个C语言形式的字符串，也可以是字符。因为+=可以提高代码的可读性，所以实际中使用的比较多。

![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656406912407-27f17d71-5545-428f-81fd-463a6f267d4a.png#averageHue=%23c6a872&clientId=u915188c0-c501-4&from=paste&height=237&id=ud3ee5973&originHeight=355&originWidth=1678&originalType=binary&ratio=1&rotation=0&showTitle=false&size=48232&status=done&style=none&taskId=u1830a462-e3fd-4da7-bb9d-f9aa550bd4e&title=&width=1118.6666666666667)
+=的使用不仅简化代码，而且可读性上升，推荐使用+=。
### insert
> insert就是随机插入，在任意位置插入字符或者字符串等。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656406986285-3e96dd99-3839-47e0-87f9-2fcc98070b34.png#averageHue=%23c9c27b&clientId=u915188c0-c501-4&from=paste&height=441&id=uf83db26a&originHeight=662&originWidth=1750&originalType=binary&ratio=1&rotation=0&showTitle=false&size=130551&status=done&style=none&taskId=u73204309-6377-4fba-a992-6031cd7e1ac&title=&width=1166.6666666666667)
> 1，在pos位置插入一个string对象
> 2，在pos位置插入一个string对象从pos位置开始len长度的字符串，如果len很长或者是npos或者不给参数，就是从pos位置一直到尾部的这个字符串全部插入。
> 3，在pos位置插入一个C语言形式的字符串
> 4，在pos位置插入这个字符串的前n个字符
> 5，在pos位置插入n个字符c，第二种形式是从这个迭代器的位置开始向后插入n个字符c
> 6，在p迭代器的位置之前插入一个字符c
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656431535047-0818006e-186a-4348-bda4-440ea560a907.png#averageHue=%23cbe6cc&clientId=u915188c0-c501-4&from=paste&height=505&id=u76f170d0&originHeight=758&originWidth=1378&originalType=binary&ratio=1&rotation=0&showTitle=false&size=151977&status=done&style=none&taskId=u36ed9af0-f121-49ef-9777-f269493fb07&title=&width=918.6666666666666)

### erase
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656431619702-3e8df652-1807-4eac-b1bf-804afebe5007.png#averageHue=%23cdc584&clientId=u915188c0-c501-4&from=paste&height=217&id=u5e2d38b0&originHeight=325&originWidth=1738&originalType=binary&ratio=1&rotation=0&showTitle=false&size=52989&status=done&style=none&taskId=u25c8aa05-7320-407e-9c90-1c90554af52&title=&width=1158.6666666666667)
> erase就是删除从pos位置开始的len长度的字符，如果len大于pos位置之后字符串长度或者是len不传参，那就是全删。这里是不会改变capacity的在vs的stl中

### 注意：
这里的erase和insert尽量少用，因为在字符串中，随机插入和删除的时间复杂度都是O（n），效率比较低。这也就是为什么STL没有给string类型的头插。
### capacity
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656431857966-95ae3f23-01e7-4c73-ab80-b93bfe332003.png#averageHue=%23d8c98b&clientId=u915188c0-c501-4&from=paste&height=109&id=u480776f7&originHeight=163&originWidth=1679&originalType=binary&ratio=1&rotation=0&showTitle=false&size=21628&status=done&style=none&taskId=ud20999c3-fd36-40df-9732-98bd52f96a5&title=&width=1119.3333333333333)
> 这里返回的是string对象的可用的最大空间，实际空间会+1，因为要保存一个\0;

### clear
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656432048219-74ea3d4c-0cc2-4a8d-8f4a-a27628adc603.png#averageHue=%23dbbe8b&clientId=u915188c0-c501-4&from=paste&height=141&id=u2b6e1472&originHeight=211&originWidth=1729&originalType=binary&ratio=1&rotation=0&showTitle=false&size=22422&status=done&style=none&taskId=u2ff203e4-8afc-4bea-ae8a-a007d7e8543&title=&width=1152.6666666666667)
> 清除字符串的所以内容，让他变成一个空字符串。

### resize
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656432208662-27acac6e-dbaf-4b5a-b390-2a0c98833418.png#averageHue=%23f5f4f3&clientId=u915188c0-c501-4&from=paste&height=299&id=uca7177c0&originHeight=449&originWidth=1637&originalType=binary&ratio=1&rotation=0&showTitle=false&size=78941&status=done&style=none&taskId=u28be9760-1852-4a38-8279-f012b210935&title=&width=1091.3333333333333)
> 解释一下文档：resize就是将这个字符串调整为n个字符的长度，如果原来的长度大于n，大于n的那一部分就会被移除，也就是将size变为n，如果原来的字符串长度小于n，那么会在原来字符串后面追加字符c使得size达到n，如果没有给c传参，那么c默认是空字符。

```cpp
void Teststring_14()
{
	string s1("kisskernel");
	s1.resize(20, 'c');
	cout << s1 << endl;
	s1.resize(5);
	cout << s1 << endl;
}
```
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656432530314-bd5bd297-0d48-40b9-aaf1-e1b9da26718d.png#averageHue=%23cce6ce&clientId=u915188c0-c501-4&from=paste&height=257&id=uf4107a32&originHeight=385&originWidth=1724&originalType=binary&ratio=1&rotation=0&showTitle=false&size=82119&status=done&style=none&taskId=uc8bc45f8-323a-4c1a-a30f-e953cffd901&title=&width=1149.3333333333333)
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656432556515-c3c1f27a-94b3-4319-8fb2-a943570f1692.png#averageHue=%23cce7ce&clientId=u915188c0-c501-4&from=paste&height=220&id=u52f5fe6a&originHeight=330&originWidth=1652&originalType=binary&ratio=1&rotation=0&showTitle=false&size=73474&status=done&style=none&taskId=u911a2336-66f5-4e11-9bc5-8521642bc5d&title=&width=1101.3333333333333)
注意这里size的改变，在windows下缩小的时候capacity是没有跟着变化的，但是在别的编译器就不一定了，因为不同的版本的stl都是按照c++标准来实现的，功能是一样的，但是底层的一些细节是不同的，比如增容的时候是标准扩容二倍还是根据情况。这些都是没有规定的。
### reserve
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656457098913-39003cad-b028-455a-bedd-13c0ec21c7cd.png#averageHue=%23f6f5f3&clientId=u915188c0-c501-4&from=paste&height=312&id=uf134a715&originHeight=468&originWidth=1653&originalType=binary&ratio=1&rotation=0&showTitle=false&size=84413&status=done&style=none&taskId=u820f8df1-5e68-4014-af19-d22bce84c8e&title=&width=1102)
> reserve是用来改变容器的capacity的，如果n大于capacity那么会将capacity变为n或者更大。如果n小于capacity那么会收缩字符串，但是不会改变字符串的内容不会删减字符串，最少就是字符串的长度，但实际一般比这个size更长一点。n<capacity的用法优点类似于shrink_to_fit，将字符串空间减少到合适的大小。
> 实际中的reserve都是用来扩大capacity的，比如已知这个字符串有多少个字符，可以直接用reserve开辟出来这个capacity或者更大的空间，这样就可以减少空间不足的时候向内容申请空间的性能消耗。

```cpp
void Teststring_15()
{

	string s1("kisskernel");
	s1.reserve(100);
	s1.reserve(15);
	s1.shrink_to_fit();
}
//这里就是先将容器扩到100，然后再缩小到15，但是字符串内容是不会改变的
//也就是size是不会改变的
```

### c_str
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656457693569-2c959f70-670e-4b63-b616-372e0a59da63.png#averageHue=%23f3f2f1&clientId=u915188c0-c501-4&from=paste&height=252&id=ucf975dce&originHeight=378&originWidth=1657&originalType=binary&ratio=1&rotation=0&showTitle=false&size=60442&status=done&style=none&taskId=u35606b8b-91c9-4998-a213-f2de9ca3158&title=&width=1104.6666666666667)
> 该函数的作用实际就是返回一个C语言形式的字符串，是以\0结尾的字符串，因为在string中可能中间有\0间隔但是后面还有字符，这时候用string类提供的cout打印和用系统的cout打印效果是不一样的。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656457852781-79544367-f2f5-4a8e-923b-50f4ff9a943f.png#averageHue=%23b8a06a&clientId=u915188c0-c501-4&from=paste&height=215&id=u0c6247fb&originHeight=323&originWidth=1705&originalType=binary&ratio=1&rotation=0&showTitle=false&size=61060&status=done&style=none&taskId=u7dae11d1-e4f4-4f47-a597-95728132fd3&title=&width=1136.6666666666667)
> C语言形式的字符串遇到\0就会终止了。

### substr
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656462608875-5ee52f59-a3bc-4189-94a2-3636c7448bf6.png#averageHue=%23f6f5f4&clientId=u915188c0-c501-4&from=paste&height=204&id=ud8bd24b1&originHeight=306&originWidth=1736&originalType=binary&ratio=1&rotation=0&showTitle=false&size=51548&status=done&style=none&taskId=u71a01a17-8358-4200-aa20-656f5a64791&title=&width=1157.3333333333333)
> substr返回的是一个字符串类对象，这个对象的内容是从pos位置开始len长度的字符串，如果len过长，或者没有传参（默认是npos）那么就是返回从pos到结尾的字符串。
> **注意：这里如果pos等于字符串长度就会返回空字符串，如果是大于字符串长度就会抛出一个out_of_range的异常**

### find和rfind
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656462975861-03febaef-fb8b-4aa4-9895-fe89ee408225.png#averageHue=%23f5f3f2&clientId=u915188c0-c501-4&from=paste&height=413&id=u966ac1f7&originHeight=619&originWidth=1715&originalType=binary&ratio=1&rotation=0&showTitle=false&size=117034&status=done&style=none&taskId=ude7619a8-5984-4983-929c-5ac4a874473&title=&width=1143.3333333333333)
> 这里的find意思是从pos位置开始找一个和string类对象或者是C语言形式的字符串又或者是一个字符相匹配的字符串，并且返回第一次匹配位置的下标。第三个的意思就是可以指定要进行匹配查找的字符串的一部分，从pos位置开始去对象里面查找s字符串的前n个字符，就像下面这样：
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656463914656-b523fcae-4e08-4eea-b327-4a01752b279f.png#averageHue=%23cce6ce&clientId=u915188c0-c501-4&from=paste&height=281&id=uc22b5ff3&originHeight=421&originWidth=1756&originalType=binary&ratio=1&rotation=0&showTitle=false&size=80853&status=done&style=none&taskId=u5dba150c-29ca-43df-a328-9e05868d68e&title=&width=1170.6666666666667)
> 第一次打印的是第一次ss出现的位置，第二次查找的是sskkkkk中前2个字符在字符串中出现的位置，也就是ss在第一次出现的位置后面，即ss第二次出现的位置。
> find如果能找到匹配的字符串就返回第一个字符的下标位置，如果找不到就返回size_t类型的-1，也即是42亿多打印出来。

**rfind**
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656464031101-ec93de8a-fac2-4664-8a59-1d9b6f3c263d.png#averageHue=%23d0b979&clientId=u915188c0-c501-4&from=paste&height=333&id=ud441de49&originHeight=499&originWidth=1751&originalType=binary&ratio=1&rotation=0&showTitle=false&size=97237&status=done&style=none&taskId=u802f6009-6f14-4149-849e-fd2cdd4826c&title=&width=1167.3333333333333)
> rfind可以看到pos默认位置是npos，也就是和find是相反的，rfind是从后向前查找匹配的字符串，依然是上面的代码，查找ss出现的位置，我们来看看如果是rfind回事怎样的结果。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656464627301-e1ed42fd-214e-4d3c-bc05-eba33f93baee.png#averageHue=%23cce6cd&clientId=u915188c0-c501-4&from=paste&height=279&id=u48112d0a&originHeight=419&originWidth=1695&originalType=binary&ratio=1&rotation=0&showTitle=false&size=84923&status=done&style=none&taskId=uc73d589f-d9b1-489b-81d1-eaed93bd22c&title=&width=1130)
> 可以看到正好和find是相反的。

下面来看一下substr和find在实际中可以用到的地方，这几举例：取出一个网址的域名和协议名
```cpp
string GetDomain(const string& s)
{
	size_t pos = s.find("://");
	if (pos != string::npos)
	{
		return s.substr(0, pos);
	}
	return string();
}
string GetProtocal(const string& s)
{
	size_t pos = s.find("://");
	if (pos != string::npos)
	{
		size_t start = pos + 3;
		size_t end = s.find('/', start);
		return s.substr(start, end-start);
	}
	return string();
}
void Teststring_19()
{
	string url1("https://www.bilibili.com/");
	string url2("https://cplusplus.com/reference/string/string/rfind/");

	cout << GetDomain(url1) << endl;
	cout << GetDomain(url2) << endl;

	cout << GetProtocal(url1) << endl;
	cout << GetProtocal(url2) << endl;

}
```
## ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656465866942-42933b30-3c17-4a7d-9cc3-f44197a701ef.png#averageHue=%23cce6cd&clientId=u915188c0-c501-4&from=paste&height=621&id=ucef654d6&originHeight=932&originWidth=1863&originalType=binary&ratio=1&rotation=0&showTitle=false&size=167135&status=done&style=none&taskId=ud535aa78-5d82-497d-a6d6-76903a9f0b1&title=&width=1242)
> 如上就是。end-start就是这个域名字符串的长度。要注意判断是否匹配成功都是使用的npos，失败返回的是空字符串，这里使用的是匿名对象。

## string类的三种遍历方法
> 学习一个容器最重要的就是遍历这个容器内的所有数据

### 1，下标运算符重载遍历
> 先看文档，元素访问这一栏
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656390996834-51af7768-13a0-435d-807a-043a2b7637c5.png#averageHue=%23f5f4f3&clientId=u915188c0-c501-4&from=paste&height=175&id=uc96d5ad1&originHeight=262&originWidth=1685&originalType=binary&ratio=1&rotation=0&showTitle=false&size=50079&status=done&style=none&taskId=u331794ad-3e57-4eef-959d-da04cfabe56&title=&width=1123.3333333333333)
> 第一个就是下标操作符的重载，第二个就是at函数其实作用是和[]一样的，用的很少，第三个就是访问最后一个字符，front就是访问第一个字符。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656391094161-de4a9e7a-bcce-420c-877e-865a67b49802.png#averageHue=%23ccc076&clientId=u915188c0-c501-4&from=paste&height=90&id=u308ef5b1&originHeight=135&originWidth=614&originalType=binary&ratio=1&rotation=0&showTitle=false&size=17571&status=done&style=none&taskId=uacc96ada-1581-4cc8-b6ee-a2e7f5a389a&title=&width=409.3333333333333)
> 这里有两个函数重载，一个是普通对象，另一个是const对象。
> 

```cpp
void Teststring_4()
{
	string s1("hello kisskernel");
	for (size_t i = 0; i < s1.size(); i++)
	{
		cout << s1[i] << " ";
	}
	cout << endl;
	for (size_t i = 0; i < s1.size(); i++)
	{
		cout << s1.at(i) << " ";
	}
}
```
> 这就是第一种遍历，要注意参数是size_t就是无符号整形，这里用int也可以，会发生类型转换，要注意无符号这个i=0的时候不可以再进行减少的操作，因为会直接变成42亿多，可能会有些地方出错。
> **[ ]和at的区别：[ ]这里越界会直接报错，断言错误，终止程序，如果at( )越界会抛出异常，需要主动捕获异常。**

![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656392371339-a97ea712-b763-4f26-ad40-5dff9b06bfdf.png#averageHue=%23cbe6cd&clientId=u915188c0-c501-4&from=paste&height=604&id=ua0e91a49&originHeight=906&originWidth=1426&originalType=binary&ratio=1&rotation=0&showTitle=false&size=99434&status=done&style=none&taskId=u5966d708-578e-4a5a-87f5-26ea061bf9a&title=&width=950.6666666666666)
### 2，迭代器遍历
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656391376762-e2b6f621-6dde-46b8-8c0d-03abe95bc8e9.png#averageHue=%23f3f2f1&clientId=u915188c0-c501-4&from=paste&height=308&id=u3cebfd1e&originHeight=462&originWidth=1735&originalType=binary&ratio=1&rotation=0&showTitle=false&size=103840&status=done&style=none&taskId=uf0cde938-bc01-40c2-b3d0-4b33c7504a0&title=&width=1156.6666666666667)
> 这里就是迭代器一栏的函数，begin和end分别会返回开始位置和结束位置的迭代器，rbegin和rend的意思是反向迭代器，前缀加上c代表const类型的对象（这是C++11中新加入的，但是begin和end本来就以及有重载了const类型对象的函数，新加入的这些cbegin函数很少会用到，作用是为了更加明显吧让你知道这里调用的是const类型的对象。）
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656391614124-d3eb285f-863d-42a0-8d9f-c1c74039b33c.png#averageHue=%23d4b778&clientId=u915188c0-c501-4&from=paste&height=204&id=u3988e2b8&originHeight=306&originWidth=1704&originalType=binary&ratio=1&rotation=0&showTitle=false&size=38532&status=done&style=none&taskId=u49f7ff57-028f-4f9a-bdc1-cd3d62382c6&title=&width=1136)
> 这里的iterator就是迭代器的意思下面来看一下如何使用

```cpp
void Teststring_5()
{
	string s1("12345");
	string::iterator it = s1.begin();
	while (it != s1.end())
	{
		cout << *it << " ";
		*it += 1;
		it++;
	}
	cout << endl;
	it = s1.begin();
	while (it != s1.end())
	{
		cout << *it << " ";
		it++;
	}
	cout << endl;
}
```
> 这里还可以通过迭代器来修改对象的内容，比如*it+=1；
> 这里要注意it一定是！=end（）的，不可以用小于，小于在这没有问题但是再别的地方会出现问题。
> 这里begin的位置是第一个字符的位置，end是最后一个字符的下一个的位置，也即是[begin,end)是左闭右开的区间
> 这里可以想象迭代器是类似指针的东西，因为他的用法就是模拟指针的用法。
> 普通的迭代器是可以修改对象的内容的，但是如果是const对象就需要const类型的迭代器了。

**迭代器的意义**：为什么有了下标重载还需要迭代器遍历呢？因为在后面的有些容器是不支持使用[ ]去遍历的，比如map，list，但是迭代器是通用的，在那种容器都可以使用。
再来看一下**反向迭代器**
```cpp
void Teststring_6()
{
	string s1("12345");
	string::reverse_iterator it = s1.rbegin();
	while (it != s1.rend())
	{
		cout << *it << " ";
		it++;
	}
	cout << endl;
}
```
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656392585583-512fc5f6-6f43-4f88-ad2d-2d8176ff655b.png#averageHue=%23cbe6cd&clientId=u915188c0-c501-4&from=paste&height=313&id=ue0ddad78&originHeight=470&originWidth=1362&originalType=binary&ratio=1&rotation=0&showTitle=false&size=68598&status=done&style=none&taskId=ua6fedce0-3910-4ced-b443-1c71dae651a&title=&width=908)
> 要注意在反向迭代器这里使用的是reverse_iterstor，代表了反向迭代器类型，rbegin和rend返回的都是反向迭代器，**特别注意**：这里的it仍然是++，因为并不是从后向前，而是从反向迭代器开始位置++到结束位置，要特别注意。

这里列举一下常用的迭代器类型
```cpp
1.iterator;
2.reverse_iterator;
3.const_iterator;
4.cosnt_reverse_iterator;
```

至于cbegin和cend这些，由于使用const会自动匹配到begin的const类型所有很少用到cbegin。
> 其他类型的遍历也可以使用迭代器，来看代码：

![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1656393354468-bb164e4f-8eb9-4b16-b8ef-671ad02eec41.png#averageHue=%23cbe6cd&clientId=u915188c0-c501-4&from=paste&height=511&id=u75e1a984&originHeight=767&originWidth=1781&originalType=binary&ratio=1&rotation=0&showTitle=false&size=114396&status=done&style=none&taskId=ud3f5b99c-518a-4fb7-8a17-cb427619e83&title=&width=1187.3333333333333)
可以看到针对list<int>和vector<int>类，迭代器遍历也是没有问题的。
### 3，范围for遍历
> 最后就是范围for遍历，这是C++11引入的，其实范围for底层依靠的还是迭代器，在编译的时候会被编译器转换成迭代器，就像是函数模板一样，在编译的时候会根据参数实例化出具体的函数，所以实际这些语法都是依靠编译器的转换。

```cpp
void Teststring_8()
{
	string s1 = "kisskernel";
	for (char e : s1)
	{
		cout << e << " ";
	}
	cout << endl;
}
```
> 如上就是范围for循环，代码非常简洁，这里要注意：首先如果没有写引用的话，这里是不可以改变对象的成员的，还有就是如上不一定都是auto，如果已经知道了类型直接写也是没有问题的。
> 范围for的缺点就是无法返回元素的下标索引，所以根据情况选择是不是要用范围for。

