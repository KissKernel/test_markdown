> C++面向对象三大特性之一，多态

## 多态的定义
> 多态是什么，直接看就是多种形态。具体细讲就是不同对象在完成同一种行为的时候结果不同。
> 很常见的一种情况就是不同的对象，调用同一个函数，但是出现了不同的结果.

**多态分为两种：**
1.静态多态：函数重载（看起来调用的是一个函数但是传递不同的参数有不同的行为，cout的自动识别）
2.动态多态：一个父类对象的引用或者指针去调用一个函数，传递不同的对象会出现不同的行为
静态：是指在编译阶段实现
动态：是指在程序运行阶段实现
## 多态的实现
### 1.多态实现的两个必要条件
> 1.必须是基类的引用或者指针调用函数
> 2.被调用的函数必须是虚函数，并且派生类对其完成了重写。

### 2.什么是虚函数
> 被virtual修饰的函数就是虚函数

```cpp
class Student
{
public:
	virtual void func1()
	{
		cout << "Student::virtual void func1()" << endl;
	}
};
```
> 这里的func1就是虚函数

**静态成员函数不能成为虚函数**
因为：因为静态的成员函数没有this指针，通过类型：：函数名的方式无法访问到虚函数表，所以静态函数无法放入虚函数表，也就不能成为虚函数。
（为什么要通过this指针访问虚函数表，因为虚函数表的生成是在构造函数的初始化列表里面，需要通过对象的this指针找到虚函数并将地址写入虚函数表）
**解释**：**静态成员函数与具体对象无关，属于整个类，核心关键是没有隐藏的this指针，可以通过类名::成员函数名 直接调用，此时没有this无法拿到虚表，就无法实现多态，因此不能设置为虚函数**
### 3.重写的条件
> 重写就是派生类和基类中满足三同（函数名相同，参数相同，返回值相同）的虚函数，这就是说派生类函数完成了对基类函数的重写（又叫做覆盖）

```cpp
class Person
{
public:
	virtual void func1()
	{
		cout << "Person::virtual void func1()" << endl;
	}
};
class Student : public Person
{
public:
	virtual void func1()
	{
		cout << "Student::virtual void func1()" << endl;
	}
};
```
> 这段代码派生类就完成了对于基类的重写。

### 4.多态实现代码
> 满足了两个必要条件那么这时候派生类和基类的这个虚函数就是满足多态的条件了

```cpp
class Person
{
public:
	virtual void func1()
	{
		cout << "Person::virtual void func1()" << endl;
	}
};
class Student : public Person
{
public:
	virtual void func1()
	{
		cout << "Student::virtual void func1()" << endl;
	}
};

void test1(Person& p)
{
	p.func1();
}

int main()
{
	Person p1;
	Student s1;
	test1(s1);
	test1(p1);

	return 0;
}
```
> 这里使用的是引用接受，然后引用调用，然后传递了不同的对象。

![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1663069691978-54b3781e-37bf-4007-b9c3-04ab662db937.png#clientId=u794c71ed-2f97-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=362&id=u6be84446&margin=%5Bobject%20Object%5D&name=image.png&originHeight=543&originWidth=892&originalType=binary&ratio=1&rotation=0&showTitle=false&size=48412&status=done&style=none&taskId=uc6014d83-ab55-45a8-ad48-7b832f6322c&title=&width=594.6666666666666)
> 多态的核心就是，不同的对象，同一种行为，不同的结果。

这里的函数调用的分析主要是看满不满足多态的两大条件
> 构成多态，传递什么类的参数就调用的是什么类的完成重写的那个虚函数。——与对象有关
> 不构成多态，调用的是参数类的那个名字的函数。——与参数类型有关

### 5.重写的两个例外
> **1.协变（基类与派生类的虚函数返回值不同）**
> 上面讲重写必须要求虚函数符合三同，但是协变情况下，返回值可以不同。返回值可以是父子关系的指针或者引用。可以是其他的继承类也可以是自己这里的父子关系的指针或者引用。
> 但是**要注意**：基类的虚函数必须返回基类的指针或者引用，派生类的虚函数的返回值可以是派生类的指针或者引用也可是基类的（基类的时候就相当于返回值相同了）。

```cpp
class Person
{
public:
	virtual Person* func1()
	{
		cout << "Person::virtual void func1()" << endl;
		return nullptr;
	}
};
class Student : public Person
{
public:
	virtual Student* func1()
	{
		cout << "Student::virtual void func1()" << endl;
		return nullptr;
	}
};
```
> **2.析构函数的重写（基类和派生类的函数名不同）**
> 基类的析构和派生类的析构也可以完成重写，只要两个都是虚函数即可。

```cpp
class Person
{
public:
	virtual Person* func1()
	{
		cout << "Person::virtual void func1()" << endl;
		return nullptr;
	}

	virtual ~Person()
	{
		cout << "~Person" << endl;
	}
};
class Student : public Person
{
public:
	virtual Student* func1()
	{
		cout << "Student::virtual void func1()" << endl;
		return nullptr;
	}

	virtual ~Student()
	{
		cout << "~Student" << endl;
	}
};
void test2()
{
	Person* pp = new Person;
	Person* ps = new Student;

	delete pp;
	delete ps;
}
```
> 这里的析构函数在编译的时候都会被编译器替换成destructor（），因此满足了重写的条件。
> 编译器做的这个工作主要是为了上面代码的这种情况设计的。
> new出来的基类对象和派生类对象都交给基类的指针来管理。这时候如果没有对基类的虚函数进行重写，那么delete的时候两次调用的都是基类的析构函数，那么子类对象就会有一部分没有进行析构资源清理，可能会造成内存泄漏。

> 其实这里的派生类里面进行重写的虚函数不用加virtual也是可以编写通过的。因为派生类在继承的时候先继承了基类的虚函数然后再完成了重写。所以派生类的这个函数也算是虚函数。
> 但是不推荐省略。
> 这种设计的初衷是为了方便进行代码分工，如果基类函数定义成立虚函数，那么派生类继承了基类之后就算忘记加virtual也是可以完成重写的。不至于因为派生类没有重写而造成某些错误（特别是派生类的析构函数要完成重写防止内存泄漏）

## C++11引入的final和override关键字
> C++11为了规范多态的引用引入了final和override关键字，帮助检测是否重写。

**final**
> final关键字的作用是添加在虚函数的后面使得这个虚函数不可被继承，不能被继承进而也就不可以被重写。
> final还可以放在类名后面进行修饰使得这个类不可被继承

```cpp

class Person
{
public:
	virtual Person* func1() final
	{
		cout << "Person::virtual void func1()" << endl;
		return nullptr;
	}

	virtual ~Person()
	{
		cout << "~Person" << endl;
	}
};
class Student : public Person
{
public:
	virtual Student* func1()
	{
		cout << "Student::virtual void func1()" << endl;
		return nullptr;
	}

	virtual ~Student() 
	{
		cout << "~Student" << endl;
	}
};
```
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1663072910535-1c4b40bd-ef26-43c5-90d6-06929751f9fb.png#clientId=u794c71ed-2f97-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=162&id=ua2defcde&margin=%5Bobject%20Object%5D&name=image.png&originHeight=243&originWidth=1447&originalType=binary&ratio=1&rotation=0&showTitle=false&size=35998&status=done&style=none&taskId=u66e77a0e-60e9-4d0f-af8d-d36e8b83715&title=&width=964.6666666666666)

**override**
> override关键字的作用主要是放在派生类虚函数的后面检查该函数是否完成了对基类虚函数的重写，没有重写就会报错。

```cpp
class Person
{
public:
	virtual Person* func1()
	{
		cout << "Person::virtual void func1()" << endl;
		return nullptr;
	}

	virtual ~Person()
	{
		cout << "~Person" << endl;
	}
};
class Student : public Person
{
public:
	virtual Student* func2() override
	{
		cout << "Student::virtual void func1()" << endl;
		return nullptr;
	}

	virtual ~Student() 
	{
		cout << "~Student" << endl;
	}
};
```
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1663073103375-5ce68649-e1fb-4f6c-96d3-68877e747181.png#clientId=u794c71ed-2f97-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=547&id=u69fc5283&margin=%5Bobject%20Object%5D&name=image.png&originHeight=820&originWidth=1464&originalType=binary&ratio=1&rotation=0&showTitle=false&size=106454&status=done&style=none&taskId=ufae39fc5-e511-482a-9adb-9512a326575&title=&width=976)

> 如何让一个类不能被继承？
> 可以使用final使基类中的虚函数不可以被继承。
> 要想一个类不能被继承可以将构造函数设置成为私有，这样就算派生类继承了基类也调不动基类的构造函数无法实例化出对象。（间接限制）
> 直接限制就是，使用final修饰基类，这个基类就不可以被继承了。

## 重载，重写（覆盖），隐藏（重定义）
> 重载条件：
> 1.必须是同一作用域
> 2.必须函数名相同，并且参数不同（类型不同或者个数不同或者顺序不同）

> 重写（覆盖）条件：
> 1.必须是两个虚函数
> 2.必须分别在基类和派生类的作用域
> 3.必须是函数名相同，参数相同，返回值相同（协变除外）

> 隐藏（重定义）的条件
> 1.必须是分别再基类和派生类的作用域
> 2.必须是函数名相同
> 两个基类和派生类的同名函数不是重写就是隐藏。

## 抽象类
> 抽象的含义就是没有实体，或者说现实世界中不存在实物的东西。
> 一个类如果含有纯虚函数（在虚函数后面加上 = 0）那么这个类就是抽象类。抽象类不能实例化出对象，派生类继承类抽象类之后也不可以实例化出对象，只有派生类对抽象类的纯虚函数完成重写之后，派生类才可以实例化出对象。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1663080099920-067e0b7c-51e5-438a-9608-668ed682bd6a.png#clientId=u8609870b-9413-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=142&id=u9b30f4fe&margin=%5Bobject%20Object%5D&name=image.png&originHeight=213&originWidth=1092&originalType=binary&ratio=1&rotation=0&showTitle=false&size=35078&status=done&style=none&taskId=ufcf83a3e-860b-4922-93fa-420a75bae42&title=&width=728)
> 抽象类可以强制子类完成重写虚函数，抽象类不能实例化出对象所以抽象类内部的函数不需要写出实现。因此派生类继承了抽象类相当于继承了抽象类的接口，但是没有实现，抽象类又被叫做接口类。体现了接口继承。

```cpp
class A
{
public:
	virtual void test() = 0;
};

class B : public A
{
public:
	virtual void test()
	{
		cout << "class B : public A::virtual void test()" << endl;
	}
	int _b;
};


class C:public A
{
public:
	virtual void test()
	{
		cout << "class C:public A::virtual void test()" << endl;
	}
	int _c;
};

int main()
{
	A* pb = new B;
	A* pc = new C;
	pb->test();
	pc->test();

	return 0;
}
```
> 在派生类中对抽象类进行重写之后，派生类就可以正常的创建对象了。

## 接口继承和实现继承
> 接口继承就是普通函数的继承。对于普通函数在派生类中可以直接调用，相当于是将函数的实现继承了过来。虚函数的继承是一种接口继承，派生类继承的是基类虚函数的接口，目的是为了重写，达成多态，继承的是接口。**所以如果不实现多态，不要把函数定义成虚函数。**

## 多态的原理
### 1.虚函数表
> 根据多态的实现，我们在使用父类的引用或者指针调用函数的时候，根据父类的指针或者引用指向的对象不同调用的函数就不同，那么这种实现的底层原理是什么呢？

> 是因为在含有虚函数的类实例化出来的对象里面都会存在一个虚函数表指针，这个指针指向了一个虚函数表（是一个虚函数指针数组）简称是虚表，虚表里面存放的就是这个类里面的虚函数的地址，也包括继承下来的虚函数。

![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1663080921979-b7263f0c-eee7-488e-b0be-eeff639e2609.png#clientId=u8609870b-9413-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=379&id=u20318588&margin=%5Bobject%20Object%5D&name=image.png&originHeight=569&originWidth=824&originalType=binary&ratio=1&rotation=0&showTitle=false&size=56399&status=done&style=none&taskId=uf363fcc4-6816-4a23-8c0b-45a4d15526c&title=&width=549.3333333333334)
> 这段代码可以看到，a对象里面有一个成员变量_a，理论上说大小应该是4字节，但是算出来是8个字节也就是印证了对象里面还包含了一个指针变量（虚函数表指针）

![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1663081236148-ca332153-cb5e-4980-a91d-104d7b7082dd.png#clientId=u8609870b-9413-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=402&id=ue11ba877&margin=%5Bobject%20Object%5D&name=image.png&originHeight=603&originWidth=1617&originalType=binary&ratio=1&rotation=0&showTitle=false&size=72629&status=done&style=none&taskId=u275cd4af-4128-4d83-8219-b4c06e8018c&title=&width=1078)
> 这里可以看到a对象里面的第一个成员是一个__vfptr的指针，这个指针就是虚函数表指针。现在虚函数表内有一个虚函数，所以只有一个地址。虚函数表最后以nullptr结尾。

> 下面我们多添加几个虚函数再次观察

```cpp
class A
{
public:
	virtual void func1()
	{
		cout << "func()" << endl;
	}
	virtual void func2()
	{
		cout << "func2()" << endl;
	}
private:
	int _a;
};

class B : public A
{
public:
	
private:
	int _a;
};

int main()
{
	A a;
	B b;
	return 0;
}
```
> 这里B类继承了A类，自然也是继承了A类的虚函数表。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1663081559576-452b0fd9-097d-4b6a-b9e8-060b3bcdfac3.png#clientId=u8609870b-9413-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=332&id=ue00067f2&margin=%5Bobject%20Object%5D&name=image.png&originHeight=498&originWidth=1461&originalType=binary&ratio=1&rotation=0&showTitle=false&size=88844&status=done&style=none&taskId=u973c930c-ef28-4625-8e89-f8574ba515b&title=&width=974)
> 所以b对象的虚函数表就是A类的虚函数表的拷贝
> 下面我们在B类中对于func进行重写，然后增加一个B类自己的虚函数func3再来看这个虚函数表的变化。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1663082022450-e9161fac-eae7-47c4-90bf-a8cc9523cc7e.png#clientId=u8609870b-9413-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=489&id=u6e7b8303&margin=%5Bobject%20Object%5D&name=image.png&originHeight=734&originWidth=1520&originalType=binary&ratio=1&rotation=0&showTitle=false&size=120217&status=done&style=none&taskId=u7e6b2d17-b47f-4ff9-b4c6-e74e2faf1d5&title=&width=1013.3333333333334)
> 从这里我们可以看出，经过重写后，b对象的虚函数表内的第一个函数指针的内容已经变了。也即是重写后新的派生类的虚函数将原来的那个A类的虚函数覆盖掉了。这就是重写又叫覆盖的原因，重写是语法的叫法，覆盖是原理层的含义。

> 从监视窗口我们看不到func3出现在虚表中，是因为vs对于监视窗口进行了优化。这里我们可以通过内存窗口来观察func3的地址是不是真的在虚函数表内。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1663082358808-d6c596a2-2ae0-46ec-9769-9b3b303c7573.png#clientId=u8609870b-9413-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=339&id=uff95a46e&margin=%5Bobject%20Object%5D&name=image.png&originHeight=508&originWidth=1501&originalType=binary&ratio=1&rotation=0&showTitle=false&size=112550&status=done&style=none&taskId=u0346c742-a881-458d-a9b2-4d82f9b156e&title=&width=1000.6666666666666)
> 这个内存里面，在func1和func2地址下面的那个地址就是func3的地址。

> 虚函数表这里也解释了为什么多态的第一个条件，必须是基类的指针或者引用调用。因为子类的指针和引用穿过去会进行切片。保留基类的一部分包括子类的虚函数表。但是如果是传对象，那么就是用子类的里面的基类部分拷贝出来一个对象，此时要注意，拷贝出来的对象那个虚表是基类的虚表，不是子类的虚表。

> 这里的一个规则就是父类对象里面一定是父类的虚表，子类对象里面一定是子类的虚表。
> 试想如果父类对象经过子类对象的一次赋值，虚表就变成了子类的虚表那么后面你自己就会混了。

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1663082733133-810ff7fe-93b7-4297-a417-a4edbab113ff.png#clientId=u8609870b-9413-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=239&id=u60597c6e&margin=%5Bobject%20Object%5D&name=image.png&originHeight=359&originWidth=1331&originalType=binary&ratio=1&rotation=0&showTitle=false&size=58276&status=done&style=none&taskId=ub5797f7d-c0aa-4f1a-9942-d829813082f&title=&width=887.3333333333334)
> 同类型的对象的虚表指针是一样的都是指向同一个虚表。

> 只有虚函数是会存在虚表里面的。但是他们的实际存储位置都是在代码端，只是虚函数将地址又存进了虚函数表内。用来实现多态。

### 2.多态的实现原理
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1663083305791-0a646b9a-a7c4-428d-a0f7-79c857e18120.png#clientId=u8609870b-9413-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=358&id=ud1431c86&margin=%5Bobject%20Object%5D&name=image.png&originHeight=537&originWidth=1453&originalType=binary&ratio=1&rotation=0&showTitle=false&size=110623&status=done&style=none&taskId=uc3d8f135-bd46-402a-82bb-9d74a2ba5c4&title=&width=968.6666666666666)
> 这张图就很好的说明了多态的实现原理。传过去的分别是基类和派生类的对象的地址，派生类传过去发生了切片。所以虽然都是转换成了A*类型但是两次调用时候的虚函数表指针是不同的。去虚函数里面找func1的时候找到的函数也就不同，这就实现了多态。

> 对外看就是不同的对象去完成同一行为的时候出现了不同的形态
> 这时我们也可以理解为什么需要子类虚函数覆盖（重写）父类的虚函数。因为子类的虚表是继承父类的虚表。重写之后才可以将虚表内的函数改为子类自己的虚函数地址。
> 为什么要父类的指针或者引用来调用，因为不会发生拷贝构造，穿过的子类对象虽然发生了切片，但是虚函数表指针还是子类的虚函数表指针。由此才可以调用到子类的虚函数。


>  再通过下面的汇编代码分析，**看出满足多态以后的函数调用，不是在编译时确定的，是运行起来以后到**
> **对象的中去找的。不满足多态的函数调用时编译时确认好的**。** **
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1663160193318-9bbb14af-4381-44f0-8941-9c3c39a83046.png#clientId=u46a5233e-596f-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=558&id=u46bbd4f7&margin=%5Bobject%20Object%5D&name=image.png&originHeight=837&originWidth=1737&originalType=binary&ratio=1&rotation=0&showTitle=false&size=232505&status=done&style=none&taskId=u18e6a9ae-35b7-4d73-bb95-4896336ad12&title=&width=1158)
> 可以看到前面的代码就是取出pa指向的对象的前四个字节的虚函数表指针，然后找到虚函数表，再找到func1，将func1的地址放到eax寄存器里面，最后通过call指令调用。但是其实这里的地址并不是func1函数真正的地址。call之后会跳到真正的func1函数的地址处。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1663160405713-c6168893-49c9-40cd-9319-2b9b3cbf9b3d.png#clientId=u46a5233e-596f-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=237&id=u045967fd&margin=%5Bobject%20Object%5D&name=image.png&originHeight=355&originWidth=1875&originalType=binary&ratio=1&rotation=0&showTitle=false&size=100035&status=done&style=none&taskId=uabe9c5ad-0a05-4767-9974-fcb6f690e57&title=&width=1250)
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1663160442983-0478f9b2-1f64-4348-99eb-02d39a59a398.png#clientId=u46a5233e-596f-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=436&id=u842a5fe3&margin=%5Bobject%20Object%5D&name=image.png&originHeight=654&originWidth=914&originalType=binary&ratio=1&rotation=0&showTitle=false&size=114288&status=done&style=none&taskId=u93f804b1-b741-41f8-893a-30323aae5d3&title=&width=609.3333333333334)
> 可以看到vs这里对齐封装了一层。所以第一次call之后并不是直接跳到func1函数体处开始执行，而是跳到中间层然后再次jmp跳到真正的func1函数处开始执行。

> 那么为什么vs这里要做多一层的封装呢？实际是为了中间层做一些事情。

**总结多态原理：**
> 满足多态的条件调用函数的时候才回去虚函数表里面去找，不满足多态编译的时候就已经确定了调用函数的地址。
> 如果派生类重写基类的函数放在私有里面，那么满足多态的条件的时候也是可以调用的动的。因为可以通过虚函数表里面的虚函数地址来找到函数，直接调用（堪称流氓玩法，形象的例子就是，道德规范限制了你的某种行为但是法律没有明确说明，你依旧可以做出这个行为，只是不道德）

## 关于动态绑定与静态绑定
> 动态绑定和静态绑定其实就是动态多态和静态多态的实现原理方面。
> **动态绑定**是指**程序运行期间**确定了程序的具体行为（比如调用那个函数）所以也称为**动态多态**
> **静态绑定**是指再**程序编译阶段**确定了函数的具体行为，将调用的函数地址填入调用的地方。因此成为**静态多态。**比如**函数重载**。

## 单继承和多继承时的虚函数表
> 这里主要关注的时派生类对象里面的虚函数表。因为基类的虚函数表已经明确了都有什么元素。

### 单继承的虚函数表
> 上面讲过vs编译器将虚函数表内的有些虚函数进行了隐藏，下面我们就打印出虚函数表来直接观察虚函数表内的函数地址。

> 那么如何打印虚函数表？
> 首先我们需要拿到派生类对象的前四个字节（也就是虚函数表指针）

```cpp
class A
{
public:
	virtual void func1()
	{
		cout << "class A : func1()" << endl;
	}
	virtual void func2()
	{
		cout << "class A : func2()" << endl;
	}
private:
	int _a;
};

class B : public A
{
public:
	virtual void func1()
	{
		cout << "class B : public A :: func1()" << endl;
	}
	virtual void func3()
	{
		cout << "class B : public A :: func3()" << endl;
	}
	virtual void func4()
	{
		cout<<"class B : public A :: func4()" << endl;
	}
private:
	int _b;
};
/////////////////////////////////////////////////////////////////////////////////////////
typedef void(*_vfptr)();

void VfuncPrint(_vfptr vfunc[])
{
	for (int i = 0; vfunc[i] != nullptr; i++)
	{
		printf("第%d个虚函数地址：%p\n", i + 1, vfunc[i]);
	}
}
int main()
{
	A a;
	B b;
	VfuncPrint((_vfptr*) *((void**)&a));
	puts("");
	VfuncPrint((_vfptr*)*((void**)&b));
}
```
> 这段代码可以打印出虚函数表内的虚函数地址。
> 首先如何取出对象的前四个字节的？
> 先取出对象的地址然后强转成为void**，当然也可以是其他的二级指针，比如int**，强转成二级指针的原因是二级指针里面存放的是一级指针的地址，一定是四个或者八字节，我们对象里面的前四个字节或者八个字节就是一个指针（指向函数指针数组的指针，又叫虚函数表指针）所以完全吻合，不论是32位机还是64位机。
> 对二级指针解引用刚好拿到前四个字节，也即是虚函数表指针。然后强转成指向函数指针的指针（一个二级指针）然后传参给Print函数，进行打印即可。

> 这里的强转也可以将a的地址转成int*，然后解引用拿到前四个字节（只适合32位机，64位要使用long long，不可以是double因为会有精度损失使得二进制错误）
> 强壮的时候也要注意，不可以随便转，必须是相关类型才可以互相转换。

打印结果：
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1663162756399-7f79da8f-b207-474c-add3-cd56aa2e624e.png#clientId=u46a5233e-596f-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=537&id=u25892a8f&margin=%5Bobject%20Object%5D&name=image.png&originHeight=806&originWidth=1234&originalType=binary&ratio=1&rotation=0&showTitle=false&size=146027&status=done&style=none&taskId=u72b9eb23-c624-4278-ba41-a420be0bfef&title=&width=822.6666666666666)
> 第一个和第二个虚函数是派生类继承下来的，派生类对于第一个虚函数进行了重写所以重写后的虚函数的地址覆盖了原来的地址因此第一个不一样。后面第三个和第四个虚函数地址是派生类自己的虚函数。

### 多继承的虚函数表
先看代码：
```cpp
class Base1 
{
public:
	virtual void func1() { cout << "Base1::func1" << endl; }
	virtual void func2() { cout << "Base1::func2" << endl; }
private:
	int b1;
};
class Base2 
{
public:
	virtual void func1() { cout << "Base2::func1" << endl; }
	virtual void func2() { cout << "Base2::func2" << endl; }
private:
	int b2;
};
class Derive : public Base1, public Base2 
{
public:
	virtual void func1() { cout << "Derive::func1" << endl; }
	virtual void func3() { cout << "Derive::func3" << endl; }
private:
	int d1;
};

typedef void(*_vfptr)();

void VfuncPrint(_vfptr vfunc[])
{
	printf("虚表地址：%p\n", vfunc);
	for (int i = 0; vfunc[i] != nullptr; i++)
	{
		printf("第%d个虚函数地址：%p\n", i + 1, vfunc[i]);
	}
}
int main()
{
	Base1 b1;
	Base2 b2;
	Derive d;
	VfuncPrint((_vfptr*) *((void**)&b1));
	puts("");
	VfuncPrint((_vfptr*)*((void**)&b2));
	puts("");
	VfuncPrint((_vfptr*)*((void**)&d));
}
```
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1663163610041-db3d05eb-0356-4db0-aae3-fb8ea34f2139.png#clientId=u46a5233e-596f-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=548&id=u04c24df9&margin=%5Bobject%20Object%5D&name=image.png&originHeight=822&originWidth=1779&originalType=binary&ratio=1&rotation=0&showTitle=false&size=200455&status=done&style=none&taskId=ud97a24ad-c7df-43a5-ba1b-9a6d4efa9b6&title=&width=1186)
> 首先在这里可以看到Derive继承了Base1和Base2，所以d对象里面存在着两个虚表。分别是第一个继承的类和第二个继承的类的虚表的拷贝，如果没有进行重写，那么没有被重写的函数就还是原来的地址，如果进行了重写，这里Base1和Base2里面的虚函数是满足三相同的所以一旦重写了一个，另一个也会被重写。

> 这段代码对func1进行了重写，就是对继承的两个类都完成了重写，所以这里的两个虚表的第一个地址应该是一样的。但是这里监视窗口看到的是不一样的。因为这里vs进行了封装，实际上他们调用的是同一个函数。
> 下面用代码来验证一下。

![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1663164138035-c2df925e-1fb3-4efe-adf7-bcf1c08a64b5.png#clientId=u46a5233e-596f-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=263&id=ua3c9f55a&margin=%5Bobject%20Object%5D&name=image.png&originHeight=395&originWidth=640&originalType=binary&ratio=1&rotation=0&showTitle=false&size=39364&status=done&style=none&taskId=ub5ea178e-3f3a-4cf5-bcee-933098e4c62&title=&width=426.6666666666667)
> 先创建d对象然后使用派生类对象d的地址切片赋给Base1的指针和Base2的指针。这时候访问func1都是在子类的两个虚表中查找。因为派生类对func1进行了重写所以调用的是同一个函数。

下面看汇编代码演示：
![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1663164347569-24808422-ce92-4ed8-b15f-f159befff715.png#clientId=u46a5233e-596f-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=367&id=uea47280a&margin=%5Bobject%20Object%5D&name=image.png&originHeight=550&originWidth=1802&originalType=binary&ratio=1&rotation=0&showTitle=false&size=148839&status=done&style=none&taskId=u22e96910-6026-44fc-8edb-bb7416624e1&title=&width=1201.3333333333333)
> 这里的eax就是存放在虚函数表里面的func1的地址，跳转来看汇编代码
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1663164426080-c88f3d44-26f3-47b9-899d-66c18e774d01.png#clientId=u46a5233e-596f-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=89&id=u294f8ea9&margin=%5Bobject%20Object%5D&name=image.png&originHeight=133&originWidth=807&originalType=binary&ratio=1&rotation=0&showTitle=false&size=39188&status=done&style=none&taskId=u6682e3aa-3891-4ca4-a0bb-782f31b7e09&title=&width=538)
> 这里并不是直接跳到了func1而是到一层中转层。再次跳转来看汇编
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1663164489629-eefc9768-c595-42f0-a368-e87a5a21748f.png#clientId=u46a5233e-596f-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=429&id=u5b56b64c&margin=%5Bobject%20Object%5D&name=image.png&originHeight=644&originWidth=908&originalType=binary&ratio=1&rotation=0&showTitle=false&size=127306&status=done&style=none&taskId=u35281b5c-23b6-4ebd-b50b-8be12a18a06&title=&width=605.3333333333334)
> 这次才是真正调用到了func1.

下面我们在来看第二个类调用的func1函数的汇编代码，来分析为什么这两个虚表内第一个func1的地址是不一样的。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1663164635185-1f5af6ed-8516-462e-a38b-bb62ccc77cb0.png#clientId=u46a5233e-596f-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=393&id=u83feec5a&margin=%5Bobject%20Object%5D&name=image.png&originHeight=590&originWidth=1726&originalType=binary&ratio=1&rotation=0&showTitle=false&size=187610&status=done&style=none&taskId=u30bbebed-d759-4bb3-b4d5-6bcf8ae7624&title=&width=1150.6666666666667)
> 第一步调用eax寄存器内的地址进行跳转。可以看到这里是和上一次调用时候eax的地址不一样的。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1663164704178-9f32e920-9bbf-418e-879c-6dfa67c7f9b3.png#clientId=u46a5233e-596f-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=74&id=ue6e971b3&margin=%5Bobject%20Object%5D&name=image.png&originHeight=111&originWidth=910&originalType=binary&ratio=1&rotation=0&showTitle=false&size=30316&status=done&style=none&taskId=u05a8cc70-5787-44b1-a50a-8ab0e2e9cfa&title=&width=606.6666666666666)
> 跳转到这一层，我们再次跳转，在上一次调用的时候，应该就会到func1函数出了。到那时我们可以发现这个地址是和上次的func1地址不一样的。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1663164817280-cdc29eef-3eee-49ab-b9bc-e06633be02b6.png#clientId=u46a5233e-596f-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=89&id=uc52308f8&margin=%5Bobject%20Object%5D&name=image.png&originHeight=134&originWidth=721&originalType=binary&ratio=1&rotation=0&showTitle=false&size=26857&status=done&style=none&taskId=u77e76fb3-9549-4654-8460-b2fd7d2b38b&title=&width=480.6666666666667)
> 这次跳转到了一句汇编代码，将ecx寄存器里面的值减掉了8，然后下一句jmp跳到了上次调用jmpfunc1的指令地址处。
> 先来看再次跳转。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1663164908802-7c415dc6-b0b5-4b1a-a10b-23c9832f1b98.png#clientId=u46a5233e-596f-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=46&id=u0f6d6595&margin=%5Bobject%20Object%5D&name=image.png&originHeight=69&originWidth=747&originalType=binary&ratio=1&rotation=0&showTitle=false&size=20123&status=done&style=none&taskId=ua291dd08-e6fa-4029-9890-574fcec2594&title=&width=498)
> 再次跳转
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1663165006416-ab6ee99d-9388-4665-be27-9aefafeb9118.png#clientId=u46a5233e-596f-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=417&id=u8248ab81&margin=%5Bobject%20Object%5D&name=image.png&originHeight=625&originWidth=887&originalType=binary&ratio=1&rotation=0&showTitle=false&size=125512&status=done&style=none&taskId=uc57abc35-7459-4fa6-a8b1-73a39c85866&title=&width=591.3333333333334)
> 到了函数实现。

> 为什么ecx寄存器要先减掉8个字节呢？
> **切片造成的this指针偏移**
> 首先说明ecx里面存放的是this指针。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1663165198408-92170840-32cc-453b-8b93-9fddd38a9eab.png#clientId=u46a5233e-596f-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=210&id=u9f49c3c1&margin=%5Bobject%20Object%5D&name=image.png&originHeight=315&originWidth=988&originalType=binary&ratio=1&rotation=0&showTitle=false&size=40804&status=done&style=none&taskId=u8bf4224a-2ec9-4451-8bad-8d713efc4bd&title=&width=658.6666666666666)
> 这是Derive的对象模型，Derive先继承了Base1所以在d对象里面Base1是在前面的。因此在切片的时候直接将d的地址给Base1*是没有问题的，但是在Base2这里切片的时候就得移动d的地址了。栈向下增长，但是地址是减小的。所以d的地址减掉Base1的大小字节数，就能到Base2部分的其实地址处了。因此中间需要加一层移动ecx寄存器的指令所以监视看到的第一个func1函数的地址是不相同的。
> 但是在汇编看到实际最后调用到的还是相同的函数func1。

> 下面我们来打印一下Derive的d对象的两个虚函数表，来看看Derive自己的虚函数是放在那个虚函数表的。

![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1663165643888-74aa44cf-cea7-4b41-8936-0b58415f7bbe.png#clientId=u46a5233e-596f-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=303&id=u39f75bee&margin=%5Bobject%20Object%5D&name=image.png&originHeight=455&originWidth=1180&originalType=binary&ratio=1&rotation=0&showTitle=false&size=108353&status=done&style=none&taskId=u3b1f0252-1a87-4b75-9cbf-53802651e56&title=&width=786.6666666666666)
> 可以看到这里Derive自己的func3是存放在第一个虚函数表的。（默认将派生类的虚函数存放在第一个继承的类的那个虚函数表里面）

> **总结多继承的虚函数表**，继承了几个类，就有几个虚函数表，自己的虚函数总是存放在第一个虚函数表内。

## 例题考察
1.下面程序输出结果是什么? （）
```cpp
#include<iostream>
using namespace std;
class A {
public:
	A(const char* s) { cout << s << endl; }
	~A() {}
};
class B :virtual public A {
public:
	B(const char* s1, const char* s2) :A(s1) { cout << s2 << endl; }
};
class C :virtual public A {
public:
	C(const char* s1, const char* s2) :A(s1) { cout << s2 << endl; }
};
class D :public B, public C {
public:
	D(const char* s1, const char* s2, const char* s3, const char* s4) 
		:B(s1, s2)
		,C(s1, s3)
		,A(s1)
	{
		cout << s4 << endl;
	}
};
int main() {
	D* p = new D("class A", "class B", "class C", "class D");
	delete p;
	return 0;
}
```
A：class A class B class C class D B：class D class B class C class A 
C：class D class C class B class A D：class A class C class B class D
> 答案是A，因为这里的D类对象创建的时候会先调用A类的构造函数，在初始化列表的最后一个位置。因为虽然A类不是直接被继承的。但是也是相当于继承在D类中，而且是最先声明的。初始化列表的初始化顺序与写的顺序无关与声明顺序有关，所以后面依次是B和C最后是D自己。
> 这里的D类里面只有一个A因为B和C都是虚拟继承了A。
> 但是如果不是虚拟继承，那么继承在D类中就有两份A一份在B类部分，一份在C类部分。D中就不能直接对A进行初始化了。但是B和C初始化的时候会带上A所以A会分别在B和C前面出现一次如下图：
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1663167273465-6e12f581-aa43-4dcc-a041-db4132a84d8b.png#clientId=u46a5233e-596f-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=511&id=ue51afbe3&margin=%5Bobject%20Object%5D&name=image.png&originHeight=766&originWidth=1468&originalType=binary&ratio=1&rotation=0&showTitle=false&size=31327&status=done&style=none&taskId=u6117b119-92de-4b1a-8765-dddc8b916ec&title=&width=978.6666666666666)

2.以下程序输出结果是什么（）
```cpp
class A {
public:
 virtual void func(int val = 1){ std::cout<<"A->"<< val <<std::endl;}
 virtual void test(){ func();}
};
class B : public A {
public:
 void func(int val=0){ std::cout<<"B->"<< val <<std::endl; }
};
int main(int argc ,char* argv[])
{
 B*p = new B;
 p->test();
 return 0; }
```
A: A->0 B: B->1 C: A->1 D: B->0 E: 编译出错 F: 以上都不正确
> 这里的答案是B
> 原因是，派生类继承的时候继承了基类的虚函数全部属性，但是重写的之后，就相当于是重写了基类虚函数的实现，接口还是基类的。所以这里的val缺省值也会继承基类的，不管子类的写多少最后都是基类的缺省值。

## inline函数可以是虚函数
> inline函数可以是虚函数，在调用这个虚函数的时候如果不构成多态，那么这个虚函数显的是inline属性，就会被原地展开。
> 构成多态的时候调用，就没有inline属性了。因为多态调用的时候会去对象的虚函数表里面找虚函数的地址，此时inline属性就被编译器忽略了。

## 静态成员函数不可以是虚函数
> 静态成员函数不可定义成虚函数，因为没有价值。多态就是为了让虚函数实现多态调用，但是静态函数可以直接使用类型：：函数名调用。多态对此来说就没有意义了了。

## 构造函数不可以是虚函数
> 虚函数的意义就是为了构成多态，调用时要去虚表里面找虚函数，对象中的虚表指针是在初始化列表初始化的。这也就注定了构造函数不可以是虚函数。

## 析构函数作为虚函数的场景
> 析构函数做虚函数只有一种场景，就是基类和派生类都开辟了对象，都是交给了基类的指针管理，那么在delete的时候如果析构函数不是虚函数没有完成重写，就会造成派生类的一部分成员资源没有清理干净，可能会造成内存泄漏。

