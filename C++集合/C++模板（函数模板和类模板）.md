> 二次修订于date：2024：3：13

## 泛型编程
> 泛型编程就是编写与类型无关的代码，简单来说就是一个语言机制，可以通过一个模板容器，实现不同类型的对象都可以调用这个容器，泛型编程让你编写完全一般化并可重复使用的算法，其效率与针对某特定数据类型而设计的算法相同。
> 泛型即是指具有在多种[数据类型](https://baike.baidu.com/item/%E6%95%B0%E6%8D%AE%E7%B1%BB%E5%9E%8B/10997964)上皆可操作的含义，与模板有些相似。泛型编程最著名的就是STL标准模板库。

## 函数模板
> C++的很多特性都是为了解决C语言的不足而提出来的，模板同样也是。

C语言中如果要实现两个数交换，可以实现一个函数，但是如果是不同类型的数值，比如int和double却是不能使用同一个函数。如下代码：
```c
void Swap(int* a, int* b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

int main()
{
	int a = 10, b = 20;
	Swap(&a, &b);
	double c = 10.0, d = 20.0;
	return 0;
}
```
这段代码只能实现int类型的交换而c，d的交换只能再写一个函数。
> c++有函数重载可以用同一个函数名调用不同的函数，但还是要写两个类似逻辑的函数的。如此C++就引入了模板这个语法。

### 模板的原理
> 模板就类似于生活中的模子，通过这个模子可以复刻出来很多东西，比如手机壳，更换颜色或者材料又会生产出来不同的手机壳，但是他们本质都是为一个手机类型生产的，这就是模板，并且模板本身并不是一个手机壳。
> 所以函数模板本身并不是一个函数，他只是一个蓝图，编译器根据模板实例化出来的才是执行的函数。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1655972981300-21834568-e28f-4cb9-b1ca-68d335dd912d.png#averageHue=%23fdfdfd&clientId=u9033a4dd-585d-4&from=paste&height=446&id=u46396561&originHeight=669&originWidth=1252&originalType=binary&ratio=1&rotation=0&showTitle=false&size=159506&status=done&style=none&taskId=ub97515f7-b109-428f-b9dd-f2436a7ccba&title=&width=834.6666666666666)
> 这张图就可以展示出模板的原理和作用，编译器根据传的参数double,int,char实例化出来三段函数。就是编译器干了本来该我们干的重复性的工作。

### 隐式实例化(自动推演)
```cpp
template <class T>
void Swap(T& a, T& b)
{
	T tmp = a;
	a = b;
	b = tmp;
}
int main()
{
	int a = 10,b = 20;
	Swap(a, b);
	double c = 1.6, d = 2.8;
	cout << c << " " << d << endl;
	Swap(c, d);
	cout << c << " " << d << endl;
	return 0;
}

```
> 模板关键字template接上<class T>T就是Type的意思(当然也可以随便换成别的名字），代表了不同的类型，然后这里面的class可以换成typename

编译器可以根据传过去的参数类型去推导T的类型，然后将模板实例化，按照函数模板实例化出来一个具体类型的函数如int或者double
> 推演和实例化这个过程是在预处理阶段完成的，假如实参推演出T的类型为int、char、double、就会实例化出来三段代码，这三段代码构成函数重载。这种实例化就叫做**隐式实例化**

问题：既然编译器根据传参实例化的函数，那如果传参的两个参数不同呢？
```cpp
template <class T>
void Swap(T& a, T& b)
{
	T tmp = a;
	a = b;
	b = tmp;
}
int main()
{
	int a = 10, b = 20;
	double c = 1.6, d = 2.8;
	Swap(a, c);
	return 0;
}
```
> 如上，这段代码编译器会报错，因为推演的时候不能确定是根据第一个参数的类型还是根据第二个参数的类型进行实例化。
> 错误信息：“add”: 未找到匹配的重载函数

如果要实现一个参数两种类型，有如下方法
```cpp
template <class T1,class T2>
T2 Add(const T1& x, const T2& y)
{
	return x + y;
}

int main()
{
	int a = 10, b = 20;
	double c = 1.6, d = 2.8;
	cout << Add(a, c) << endl;
	return 0;
}
```
> 可以通过定义两个模板参数来实现，将T1转换成int，T2转换成double，返回值可以是T1或者T2.

第二种方法，将参数强制类型转换：
```cpp
template <class T1>
T1 Add(const T1& x, const T1& y)
{
	return x + y;
}

int main()
{
	int a = 10, b = 20;
	double c = 1.6, d = 2.8;
	cout << Add((double)a, c) << endl;
	return 0;
}
```
> 比如这里可以将a强转成为double类型或者将c强转成int类型。这时候只需要一个模板参数即可。
> **注意**：如果不加强制类型转换是编译不能通过的。
> 但是以前写的函数是不是不加强转也是可以编译通过的呢？对的，因为这时候发生了隐式类型转换，**但是在使用模板的时候类型转换是不会发生的。**因为还没有走到隐式类型转换的那一步就报错了，编译器还没有实例化出来具体的函数。

```cpp
int Add(const int& x, const int& y)
{
	return x + y;
}
int main()
{
	int a = 10, b = 20;
	double c = 1.6, d = 2.8;
	cout << Add(a, c) << endl;
	return 0;
}
```
> 这段代码将c隐式类型转换成int类型，创建一个临时变量，然后将转换成int类型的c放到临时变量里面然后传参给y，因为临时变量具有常性，所以要加const否则也是编译不过的。c的类型是不会改变的。
> 注：隐式类型转换和强转都会生成一个临时变量

第三种方法就是显示实例化
```cpp
template <class T>
T Add(const T& x, const T& y)
{
	return x + y;
}
int main()
{
	int a = 10, b = 20;
	double c = 1.6, d = 2.8;
	cout << Add<double>(a, c) << endl;
	return 0;
}
```
> 在函数名后加尖括号写出实例化的类型就可以指定T实例化成什么类型了，这种就是显式实例化。

这时候如果类型不匹配那编译器会进行类型转换，如果转换失败就会报错。
### 模板匹配规则
1，如果存在和参数匹配的现成的非模板函数，那编译器会直接调用非模板函数不会再去实例化一个新的函数
```cpp
int Add(const int& x, const int& y)
{
	return x + y;
}
template <class T>
T Add(const T& x, const T& y)
{
	return x + y;
}
int main()
{
	int a = 10, b = 20;
	double c = 1.6, d = 2.8;
	cout << Add(a, b) << endl;
	cout << Add(a, c) << endl;
    cout << Add<int>(a,b)<<endl;
	return 0;
}
```
> 这里因为存在int，int类型的函数和a，b的类型匹配所以直接调用现成的，如果是第二个也会调用现成的，因为单个参数无法实例化出匹配的，但是现成的这个可以发生类型转换。
> 当然也可以指定调用实例化模板函数，就是第三个调用。(显式实例化)

> 上述代码，显示写出来的add和后面显示实例化出来的add<int>是一样的函数，但是他们可以同时存在，因为函数模板的命名修饰规则和函数名修饰规则不同。

2，对于非模板函数和同名函数模板，如果其他条件都相同，在调动时会优先调用非模板函数而不会通过模
板实例化一个函数。如果模板可以产生一个具有更好匹配的函数， 那么将选择模板。（与第一条的第二个调用有关）
```cpp
int Add(const int& x, const int& y)
{
	return x + y;
}
template <class T1,class T2>
T1 Add(const T1& x, const T2& y)
{
	return x + y;
}
int main()
{
	int a = 10, b = 20;
	double c = 1.6, d = 2.8;
	cout << Add(a, c) << endl;
	return 0;
}
```
> 下面的调试图可以看到调用的是模板函数，这时候模板可以生产出来一个第一个参数是int第二个参数是double，不需要发生类型转换显然比现成的更加和参数匹配，所以这时候就会优先调用模板函数。

![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1655975802439-1412276a-b9a7-44ed-aa7a-44a0507ad37a.png#averageHue=%23cbe7cd&clientId=u9033a4dd-585d-4&from=paste&height=410&id=ua8c69a5a&originHeight=615&originWidth=813&originalType=binary&ratio=1&rotation=0&showTitle=false&size=70720&status=done&style=none&taskId=uf4f5ac29-ab99-4d79-9810-8d77003991f&title=&width=542)
3、**模板函数不允许自动类型转换，但普通函数可以进行自动类型转换**
## 类模板
### 类模板的定义
```cpp
//类模板的定义格式
template<class T1, class T2, ..., class Tn>//模板参数列表
class 类模板名
{
 // 类内成员定义
};
```
下面来看一下顺序表的定义格式和使用。
```cpp
namespace lz
{
	template <class T>
	struct vector
	{
		vector()
			:_a(nullptr)
			, _size(0)
			, _capacity(0)
		{}
		void push_back(const T& x)
		{
			if (_size == _capacity)
			{
				int newcapacity = _capacity == 0 ? 4 : 2 * _capacity;
				T* tmp = new T[newcapacity];
				memcpy(tmp, _a, sizeof(T) * _size);
				delete(_a);
				_a = tmp;
				_capacity = newcapacity;
			}
			_a[_size] = x;
			_size++;
		}
		int size()
		{
			return _size;
		}
		T& operator[](int pos)
		{
			assert(pos < _size);
			return _a[pos];
		}
	private:
		T* _a;
		int _size;
		int _capacity;
	};
}

int main()
{
	lz::vector<int> v1;//显式实例化
	v1.push_back(1);
	v1.push_back(1);
	v1.push_back(1);
	v1.push_back(1);
	v1.push_back(1);
	v1.push_back(1);
	v1.push_back(1);
	for (int i = 0; i < v1.size(); i++)
	{
		v1[i] *= 2;
	}
	for (int i = 0; i < v1.size(); i++)
	{
		cout << v1[i] << " ";
	}
	cout << endl;
	return 0;
}
```
**注意**，空间不够扩容的时候要注意释放以前的空间
> 如果在类内声明，类外定义的话还需要在每个函数定义上面再次声明模板参数。

第二为什么要将这个类放在命名空间里面，因为C++本身的stl库就有vector如果引用的vector这个头文件就会报错命名冲突，所以可以放在单独的一个命名空间保护起来。
下面就是声明在类里面，定义在类外面
```cpp

namespace xzj
{
	template <class T>
	struct vector
	{
		vector()
			:_a(nullptr)
			, _size(0)
			, _capacity(0)
		{}
		void push_back(const T& x);
		T& operator[](int pos);
		int size();
	private:
		T* _a;
		int _size;
		int _capacity;
	};

	template <class T>
	void vector<T>::push_back(const T& x)
	{
		if (_size == _capacity)
		{
			int newcapacity = _capacity == 0 ? 4 : 2 * _capacity;
			T* tmp = new T[newcapacity];
			memcpy(tmp, _a, sizeof(T) * _size);
			delete(_a);
			_a = tmp;
			_capacity = newcapacity;
		}
		_a[_size] = x;
		_size++;
	}
	template <class T>
	T& vector<T>::operator[](int pos)
	{
		assert(pos < _size);
		return _a[pos];
	}
	template <class T>
	int vector<T>::size()
	{
		return _size;
	}
}
```
> 在定义函数的时候要指定函数属于那个类，vector是类名，vector<T>才是类型，而且因为函数在类外面定义所以编译器不能识别T是模板参数，要声明template <class T>让编译器知道这个T是模板参数。

总之就是：**类模板中函数放在类外进行定义时，需要加模板参数列表；**
**模板是不支持声明与定义分离的，就是声明在头文件定义在cpp文件。**模板不支持分离编译。
> 原因：在链接之前vector.cpp和main.cpp是互相独立的，所以vector.cpp中并不知道T是什么类型，所以不会实例化出具体函数，因此模板分离编译会出现LNK Error。

> 解决方法：
> 1. 在vector.cpp中声明T的类型

```cpp
template
class vector<int>;
```

2. 不要分离定义，将定义和声明都写到vector.h文件中
### 类模板的显式实例化
```cpp
int main()
{
    vector<int> v1;
    vector<double> v2;
}
```
> 这里类模板实例化的时候必须指定实例化的类型也即是<int>，并且vector不是类型，实例化出来的vector<int> 这个才是类型。

为什么一定要指定类模板的类型呢？因为这里类模板参数没办法像函数模板哪里根据参数来推演类型。
