二次修订于date：2024：3：16
## vector介绍
> vector是一个大小动态可变的一个数组的序列容器。
> 动态可变自然就是他的大小可以根据插入的数据多少动态的扩容，来适应存储更多的数据。内存不够了会自动增容。数组也就是说vector可以像数组一样使用下标对每个元素进行高效的访问与修改。并且他也像数组一样在内存之中占用一块连续的内存空间。容器就是说明这个vector可以存储各种类型的数据也可以是其他容器。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1658730188553-21b6c872-12f5-41b5-b986-e0c93ca134a8.png#averageHue=%23f9f8f7&clientId=u3cd297de-d023-4&from=paste&height=91&id=u6301b8e1&originHeight=137&originWidth=1158&originalType=binary&ratio=1&rotation=0&showTitle=false&size=14117&status=done&style=none&taskId=ua2766c0a-06bc-4968-8e5b-383c7f49e0b&title=&width=772)
> 第一个参数是模板参数，指明了vector可以存储任意类型的数据，第二个参数alloc是空间配置器，也就是内存池。这里给了默认的参数，使用了STL的内存池，也可以传用户写的内存池。

## vector的使用
### vector构造函数的使用
> vector的构造函数总共由四种方式初始化。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1658730297877-8adde35a-6662-4ed5-b437-832ef5186635.png#averageHue=%23f3fcf1&clientId=u3cd297de-d023-4&from=paste&height=248&id=u646f38be&originHeight=372&originWidth=1179&originalType=binary&ratio=1&rotation=0&showTitle=false&size=70957&status=done&style=none&taskId=uac0a2c66-b497-4c3e-bb8a-1031c1c4598&title=&width=786)
> 第一是不初始化，也即一个无参的构造函数。
> 第二是使用n个val来构造初始化vector，这里的value_type 就是T，这里在文档首页都可以查询到。
> 第三是使用一个迭代器区间来初始化，不仅可以使用vector的迭代器，也可以使用其他容器的迭代器，因为这个InputIterator也是一个模板类型。
> 第四是拷贝构造，使用一个vector来拷贝构造一个vector对象。

下面来看一下这个使用迭代器来给vector初始化。
```cpp
#include<iostream>
#include<vector>
#include<string>
using namespace std;

int main()
{
	vector<int> v1;
	vector<int> v2(10, 6);
	vector<int> v3(v2.begin(), v2.end());
	string s1("hello kisskernel");
	vector<char> v4(s1.begin(), s1.end());

	return 0;
}
```
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1658730815716-0d0da0e8-c890-48f3-8a01-8e71466a6700.png#averageHue=%23cce6cd&clientId=u3cd297de-d023-4&from=paste&height=501&id=ubb8010f0&originHeight=752&originWidth=1760&originalType=binary&ratio=1&rotation=0&showTitle=false&size=145137&status=done&style=none&taskId=u8f94cf0c-08c2-46e8-b7dc-b5b6e6449d5&title=&width=1173.3333333333333)

> 可以看到vector<char> 也可以像string一样存储字符，那么vector<char>可以代替string类嘛？

答案：不可以
> string类和vector<char> 的区别是：
> vector<char>里面是没有保存字符串里面的\0的，也就是vector这个就是数组里面存放的字符，string可以使用cout直接输出，vector<char>只能一个一个字符的输出
> 第二：vector<char>要想在末尾添加字符只能使用push_back，没有+=，append可以直接在末尾添加一个字符串
> 第三：string可以重载大于小于来比较大小。但是vector<char>的比较大小并没有意义。
> 并且，这两个类的流提取，流插入的作用也是不一样的。就是<<和>>

### vector迭代器的使用
> vector也是和string一样四种迭代器类型，正向迭代器、反向迭代器、const类型的正向迭代器、const类型的反向迭代器。
> iterator  begin是指向vector的第一个元素，end是指向最后一个元素的下一个位置。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1658731488020-f245ef89-104b-4afa-b90c-740231a786bd.png#averageHue=%23dedede&clientId=u3cd297de-d023-4&from=paste&height=272&id=u226990cc&originHeight=408&originWidth=1224&originalType=binary&ratio=1&rotation=0&showTitle=false&size=55490&status=done&style=none&taskId=ue453ff74-9a8c-4124-b271-9ada5763d38&title=&width=816)
> 在vector和string这里正向迭代器就是指针，后面的容器不一定是指针，反向迭代器就是通过封装正向迭代器得到的。具
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1658731603785-083df603-fbdf-4699-8dea-51fa77d2be86.png#averageHue=%23f3f3f3&clientId=u3cd297de-d023-4&from=paste&height=191&id=u8f85eee4&originHeight=286&originWidth=947&originalType=binary&ratio=1&rotation=0&showTitle=false&size=66358&status=done&style=none&taskId=uc7ad6b1b-69d6-47e2-baa5-c8345501e09&title=&width=631.3333333333334)

```cpp
#include<iostream>
#include<vector>
#include<string>
using namespace std;

int main()
{
	string s1("hello kisskernel");
	vector<char> v4(s1.begin(), s1.end());
    
	vector<char>::iterator it = v4.begin();
	while (it != v4.end())
	{
		cout << *it;
		it++;
	}
	return 0;
}
```
> it一定是要使用！=，虽然在连续的容器里面比如是string和vector，使用<也是没有问题的。但如果是list和map就不可以使用小于号了，而！=是通用的。不管是什么类型的容器都可以使用这种方式来进行遍历。

### vector空间函数的使用
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1658732237762-9378f260-0c1a-41a5-8166-6765c6e12060.png#averageHue=%23f2f1f0&clientId=u3cd297de-d023-4&from=paste&height=259&id=u704e1878&originHeight=389&originWidth=1340&originalType=binary&ratio=1&rotation=0&showTitle=false&size=79446&status=done&style=none&taskId=ucfa733fa-ccb3-4f2f-af2c-ddf311b135b&title=&width=893.3333333333334)
> 关于vector的空间函数用到比较多的是size，resize，capacity，reserve
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1658732295125-3a767353-bd37-4ab9-9f50-b2be2a8c1018.png#averageHue=%23d0c580&clientId=u3cd297de-d023-4&from=paste&height=114&id=uef89887e&originHeight=171&originWidth=818&originalType=binary&ratio=1&rotation=0&showTitle=false&size=21295&status=done&style=none&taskId=u611d0b61-f345-4785-9c1f-36a371619ef&title=&width=545.3333333333334)
> resize就是开辟n个元素的空间，并且使用val来进行初始化。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1658732788240-593a086c-946b-462a-8f44-98f0d82df370.png#averageHue=%23d2be80&clientId=u3cd297de-d023-4&from=paste&height=70&id=u15a8c1bc&originHeight=105&originWidth=562&originalType=binary&ratio=1&rotation=0&showTitle=false&size=12021&status=done&style=none&taskId=u24c0a58d-d1de-4a5d-9b9f-957e86fb9da&title=&width=374.6666666666667)
> reserve就是开辟空间但是不会进行初始化。更直观的区别也就是，使用resize扩容后，size会跟着改变，使用reserve扩容后，size并不会跟着改变。
> reserve扩容之后，比如reserve（100），然后再reserve（50）不会再将容量缩小，所以reserve不会缩容。
> 如果事先知道了vector需要的内存是多少，就可使用reserve来进行内存分配，以减少push_back内存不足的时候向系统申请内存空间产生的性能消耗。

> 关于vs的PJ版本他的增容方式是1.5倍作用的方式增容，也就是vs使用的STL版本。
> G++使用的SGI版本的STL增容方式是严格的2倍增长。

### vector的增删查改
| vector增删查改函数 | 作用简介 |
| --- | --- |
| push_back | 尾插 |
| pop_back | 尾删 |
| find | 在容器内查找（这并不是vector的成员函数，是算法模块实现的） |
| insert | 在pos位置之前插入一个val |
| erase | 删除pos位置的值，或者删除迭代器区间 |
| swap | 交换两个vector的数据空间 |
| operator[ ] | 类似数组访问的方式来访问vector |

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1658736002559-32bcd3f1-8f5f-4f33-885e-91138710b1e8.png#averageHue=%23c6bd6c&clientId=u7b72f89e-0dc9-4&from=paste&height=99&id=u43a41845&originHeight=149&originWidth=1125&originalType=binary&ratio=1&rotation=0&showTitle=false&size=16400&status=done&style=none&taskId=ua8704b3c-7832-4cbb-b0bb-b763dd28fe2&title=&width=750)
> find参数是模板参数的迭代器，和一个参数类型的值val，使用方法是传一个迭代器区间在这个迭代器区间内查找一个值val。返回值也是一个迭代器，就是查找到value的这个迭代器位置。这个查找的范围也是和迭代器遍历的时候是一样的左闭右开的一个区间[first，last）找不到就会返回end()位置迭代器。

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1658736164067-bf8820c8-fd42-4c2b-ace3-3bc4f5272630.png#averageHue=%23f4fcf2&clientId=u7b72f89e-0dc9-4&from=paste&height=195&id=u38c3f3f6&originHeight=292&originWidth=1437&originalType=binary&ratio=1&rotation=0&showTitle=false&size=55880&status=done&style=none&taskId=udb5b953d-311d-478a-adce-73bd82d9c89&title=&width=958)
> insert这里给出了三种实现方式，可以再一个迭代器位置pos之前插入一个val或者n个val，或者是一段迭代器区间。

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1658736761138-5fdfd406-89f7-4e1d-95a2-5d153c2d2ee1.png#averageHue=%23c7c177&clientId=u7b72f89e-0dc9-4&from=paste&height=138&id=u9d14b411&originHeight=207&originWidth=740&originalType=binary&ratio=1&rotation=0&showTitle=false&size=23592&status=done&style=none&taskId=ua244caf0-c60a-4d3b-b2f3-4dde5c6e7bf&title=&width=493.3333333333333)
> erase给出了两种实现，一种是删除pos位置，第二种是删除一个迭代器区间这个也是左闭右开的区间[first，last）

> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1658737855539-e308ac4d-f50d-41f9-9fc2-3a18db24cb4b.png#averageHue=%23cdbd76&clientId=u7b72f89e-0dc9-4&from=paste&height=105&id=u5adc2e2a&originHeight=157&originWidth=682&originalType=binary&ratio=1&rotation=0&showTitle=false&size=20411&status=done&style=none&taskId=u2a4ee8cd-3ddc-4925-9297-c34c3934b92&title=&width=454.6666666666667)
> operator[]返回值是模板类型的引用其实就是n位置这个值的引用。

## 关于迭代器失效问题
> **迭代器的主要作用就是让算法能够不用关心底层数据结构，而迭代器底层实际就是一个指针，或者是对指针进行了封装**，比如：**vector的迭代器就是原生态指针T***。因此**迭代器失效，实际就是迭代器底层对应指针所指向的空间被销毁了，或者是指针指向的空间变了，也就是这个指针的意义变了，而使用一块已经被释放的空间**，造成的后果是程序崩溃(即**如果继续使用已经失效的迭代器，程序可能会崩溃，特别是vs编译器对此有严格的检查，一旦使用了失效的迭代器程序就会崩溃**)。

> 一般引起迭代器失效有两种情况，一种就是进行了某些会改变底层数据的操作之后迭代器指向的对象变了，迭代器的意义也就变了。第二种就是，迭代器指向的空间被释放了，迭代器变成了野指针。

> **会引起其底层空间改变的操作，都有可能是迭代器失效**，比如：resize、reserve、insert、assign、 
> push_back等。
> 下面这段代码就是insert引起的迭代器失效。

```cpp
#include <iostream>
using namespace std;
#include <vector>
int main()
{
	vector<int> v{ 1,2,3,4,5,6 };

	auto it = v.begin();
	// 将有效元素个数增加到100个，多出的位置使用8填充，操作期间底层会扩容
	// v.resize(100, 8);

	// reserve的作用就是改变扩容大小但不改变有效元素个数，操作期间可能会引起底层容量改变
	// v.reserve(100);

	// 插入元素期间，可能会引起扩容，而导致原空间被释放
	// v.insert(v.begin(), 0);

	// v.push_back(8);

	// 给vector重新赋值，可能会引起底层容量改变,容量改变原来的空间就会被释放
	v.assign(100, 8);

	/*
	出错原因：以上操作，都有可能会导致vector扩容，也就是说vector底层原理旧空间被释放掉，
   而在打印时，it还使用的是释放之间的旧空间，在对it迭代器操作时，实际操作的是一块已经被释放的
   空间，而引起代码运行时崩溃。
	解决方式：在以上操作完成之后，如果想要继续通过迭代器操作vector中的元素，只需给it重新
   赋值即可。
	*/
	while (it != v.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;
	return 0;
}
```
> 下面列举一种具体的迭代器失效，insert插入元素之后，迭代器失效问题。

```cpp
int main()
{
	vector<int> v{ 2,3,1};
	vector<int>::iterator pos = find(v.begin(), v.end(),3);

	v.insert(pos, 30);
	cout << *pos << endl;

	return 0;
}
```
> 插入元素导致增容，pos就不是指向3了而是变成了野指针所以如果想要迭代器再生效就需要再次查找。当然如果没有发生增容，那么pos就是变成了指向30了，因为元素3的位置被插入了30，这时虽然pos指向了30不是空指针，但是迭代器也是失效的，如果对失效的迭代器进行解引用就会引发程序崩溃，因为迭代器的意义变了。

第二种迭代器失效问题就是，删除某些元素的时候，数据发生了挪动，导致迭代器失效。
> 比如要实现，删除数组中的偶数。

```cpp
#include<iostream>
#include<vector>
#include<string>
using namespace std;

int main()
{
	vector<int> v1{ 1,2,3,4,5,6,7};
	vector<int>::iterator it = v1.begin();
	while (it != v1.end())
	{
		if (*it % 2==0)
		{
			v1.erase(it);
		}
		cout << *it << " ";
		it++;
	}
    cout<<endl;
	return 0;
}
```
> 这段代码放到vs上面跑的话会直接崩溃，什么都不会输出，这是因为删除it指向的偶数之后，剩下的元素会跟着移动，但是it的位置还是不变，只是指向了被删除元素得下一个位置答案是此时迭代器已经失效了，后面对这个位置迭代器进行使用的时候就会导致程序崩溃，因为vs对迭代器失效检查得很严格，一但使用失效得迭代器，程序就会崩溃
> 但是Linux上面就可以正常运行，不会崩溃掉。
> ![image.png](https://cdn.nlark.com/yuque/0/2022/png/29079911/1658742724803-69972d8e-8325-4bb9-b619-4669aaee889d.png#averageHue=%23131110&clientId=u7b72f89e-0dc9-4&from=paste&height=83&id=u2371c062&originHeight=125&originWidth=769&originalType=binary&ratio=1&rotation=0&showTitle=false&size=11971&status=done&style=none&taskId=ua9299242-d7a8-4a59-ab6c-488660c3c22&title=&width=512.6666666666666)
> 因为Linux对于迭代器失效得检查不是很严格，只要这个迭代器没有越界就可以正常使用，但是虽然Linux上面成功输出了结果，并不代表这段代码就是对的，将数据换成1，2，3，4，5，6那么Linux上面运行也会爆出段错误，也就是越界了。这是因为当我们删除了一个元素之后，剩下得元素向左移动it就指向了下一个元素，但是后面又将it++了，所以这个被删除元素得下一个元素没有被判断。如果有连续得偶数就会删除得不彻底，那么为什么换成1到6就会报段错误呢？
> 因为当最后一个元素是6，刚好这个6会被检查到，删除之后end位置向前移动就到了it得位置，但是it++跳过了end，那么就已经越界了，it后续也不可能等于end了，所以发生了段错误。

正确得代码应该如下：
```cpp
#include<iostream>
#include<vector>
#include<string>
using namespace std;

int main()
{
	vector<int> v1{ 1,2,3,4,5,6,7};
	vector<int>::iterator it = v1.begin();
	while (it != v1.end())
	{
		if (*it % 2==0)
		{
			it = v1.erase(it);
		}
		else 
			it++;
	}
	for (auto e : v1)
	{
		cout << e << " ";
	}
	return 0;
}
```
> erase删除元素之后返回得是被删除元素得下一个位置，虽然不给it赋值it也会指向那个位置，但是这种情况下迭代器得意义被重新赋予了，所以迭代器没有失效。

insert和erase的返回值都是迭代器类型，如果使用了这俩个函数之后还需要继续使用迭代器，那么就让it接收他们的返回值，即可避免迭代器失效。insert返回的是新插入元素的第一个位置的迭代器，erase返回的是删除位置的下一个位置的迭代器。
