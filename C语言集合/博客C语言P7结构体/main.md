
[toc]

## 什么是结构体

>结构体我们这个可以和数组联系一下，数组是同类型元素的集合。结构体是不同种元素的集合
比如我们定义一个学生为一个变量，肯定不能只用数字来表示学生的信息把，我们肯定要记录学生的姓名，学号，性别。最少的啦这是。这时候用数组显然不合适，于是我们就有了结构体。
### 1.定义
```c
struct stu
{
    char name[20];
    char sex[10];
    int stu_id[20];
};//这里的分号不能忘记了
```

### 2.结构体变量的初始化
```c
struct Point
{
 int x;
 int y; 
}p1; //声明类型的同时定义变量p1
struct Point p2; //定义结构体变量p2
//初始化：定义变量的同时赋初值。
struct Point p3 = {x, y};


struct Stu        //类型声明
{
 char name[15];//名字
 int age;      //年龄
};
struct Stu s = {"zhangsan", 20};//初始化


struct Node
{
 int data;
 struct Point p;
 struct Node* next; 
}n1 = {10, {4,5}, NULL}; //结构体嵌套初始化//直接在这里定义的全局变量中初始化。只有在这初始化不需要写标签，其他地方都需要加上结构体标签。

struct Node n2 = {20, {5, 6}, NULL};//结构体嵌套初始化
```
>上述就是用一个结构体定义了一个学生变量类型。（这里要注意定义结构体类型是不会占用内存的，只有我们用这个类型开辟了变量之后，才会占据内存）

## 结构体的声明
上面那个其实就是一个结构体的声明。
同时我们写struct stu如果嫌弃太麻烦了我们还可以重命名。
```c
typedef struct stu
{
    char name[20];
    char sex[10];
    int stu_id[20];
}stu,s3 = {"zhengsan","nan",{2,3,3,3,3,3,3,3,3}};
int main()
{
    struct stu s2;//这就是创建变量的两种方法。
    stu s1;//
   // stu s3 = {"zhengsan","nan",{2,3,3,3,3,3,3,3,3}};
}
//这就是创建结构体的三种方法，s3是全局变量，s1，s2，是局部变量。
//如果要给s3赋值不可以直接写 s3 = {"zhengsan","nan",{2,3,3,3,3,3,3,3,3}};
//应该再s3前加上stu方可不会报错。
```
>结构体内的成员可以是数组，指针，变量，还可以是结构体（但是不能是自己的这个结构体）


## 结构体的成员访问

>结构体的成员访问有两种操作符在操作符那个章节介绍过，一个是. 操作符，另一个是-> 操作符。
代码演示：
```c
struct stu
{
    char name[20];
    char sex[10];
    int stu_id[20];
};
void print(struct stu *pa)
{
    printf ("%s %s %d",pa->name,pa->sex,pa->stu_id[0]);
}

int main()
{
    struct stu s1 = {"zhangsan","nan",{0}};
    print(&s1);
    printf("%s %s %d ",s1.name, s1.sex,s1.stu_id[0]);
}
```

>可以看到我们对结构体变量的直接访问就是用.操作符，如果是对结构体指针进行访问就是使用的->

## 结构体传参

>结构体传参直接说结论，有两种一个是，传值，一个是传地址，一般采用传地址的方式，因为参数压栈问题。（关于[参数压栈](https://blog.csdn.net/qq_62745420/article/details/121779865?spm=1001.2014.3001.5501)，这里有详细介绍）。