#关于在vs中使用scanf不安全的问题

<span id="jump">请点击跳转</span>


[TOC]
###首先是scanf为什莫不安全

首先是关于内存溢出的问题，scanf在使用的时候不会检测你输入的字符串长度所以这时就有内存溢出的风险。
```c
#include<stdio.h>
int main()
{
    char arr[6]={0};
    scanf("%s",&arr);
    //假设输入helloworld就会出现越界访问的问题，所以scanf在这种情况下会不安全。
}
```

###其次是如何解决scanf不安全的问题
解决这个问题有两个方法
１．一个是打开vs  <kbd>项目</kbd>选项看到最后一个选项<kbd>属性</kbd>
![](图片1.png)
将SDL检查禁用
２．在文件.c中第一行插入下面这段代码；
<kbd>#define _CRT_SECURE_NO_WARNINGS</kbd>

在每个文件的开头都插入显然略显麻烦，所以还有一个一劳永逸的方法，下面我来介绍一下。首先找到你的vs安装路径找到这个文件<kbd>newc++file.c</kbd>将他复制到桌面上然后在将上面一段代码复制进去，再将这个文件复制回原来的路径底下。替换原来的文件即可。*那么这里为什么要这莫做呢，因为当你在原来路径直接更改newc++file.c会提示你没有权限更改。




###最后是为什么scanf_s安全
vs中提供的scanf_s 函数需要规定输入字符的个数，所以需要额外输入一个限定数字。这样就可以保证函数不会越界访问。
```c
#include<stdio.h>
int main()
{
    char arr[6];
    scanf_s("%s",&arr,5);//那么这个时候就不会造成意外的越界访问
    return 0;
}
```
只会有人为的越界访问比如你要输入hello五个字符却只定义了arr[5]那么字符串结尾的\0;会因为无处安放而造成越界。
当然我还是建议在日常编程中使用scanf因为scanf_s是vs自己写出来的在其他的编译器上会报错，可复用性太差。

以上文件我都上传在了我的码云，需要的可以看看
[链接](https://gitee.com/KissKernel/test_markdown)