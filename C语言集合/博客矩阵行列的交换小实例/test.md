##介绍一个关于矩阵交换行列的小问题

###那么我们来看问题吧。
输入描述：
第一行包含两个整数n和m，表示一个矩阵包含n行m列，用空格分隔。 (1≤n≤10,1≤m≤10)

从2到n+1行，每行输入m个整数（范围-231~231-1），用空格分隔，共输入n*m个数，表示第一个矩阵中的元素。
接下来一行输入k，表示要执行k次操作（1≤k≤5）。接下来有k行，每行包括一个字符t和两个数a和b，中间用空格格分隔，t代表需要执行的操作，当t为字符'r'时代表进行行变换，当t为字符'c'时代表进行列变换，a和b为需要互换的行或列（1≤a≤b≤n≤10，1≤a≤b≤m≤10）。

提示：当t为别的字符时不需要处理
输出描述：
输出n行m列，为矩阵交换后的结果。每个数后面有一个空格。
####很显然这个问题的关键就是如何实现行列的交换。那么下面我们来看一下如何交换行列，实际上很简单，就是类似于交换了两个变量。

```c
#include<stdio.h>

int main()
{
    int n,m,i,j,k;
    int num1,num2;//要交换的两行或两列
    char ch;//决定进行行变换还是列变换
    int rek;//进行几次操作
    int tmp;
    scanf("%d %d",&n,&m);
    int arr[n][m];
    //输入矩阵
    for(i=0;i<n;i++)
    {
        for(j=0;j<m;j++)
        {
            scanf("%d",&arr[i][j]);
        }
    }
    
    scanf("%d",&k);
    
    for(rek=0;rek<k;rek++)
    {
       scanf(" %c %d %d",&ch,&num1,&num2); 
        if(ch=='r')//进行行变换
        {
            for(j=0;j<m;j++)
            {
                tmp=arr[num1-1][j];//进行行变换的时候约定矩阵的行不变循环递归列然后交换即可
                arr[num1-1][j]=arr[num2-1][j];
                arr[num2-1][j]=tmp;
            }
        }
        else if(ch=='c')//进行列变换
        {
            for(i=0;i<n;i++)
            {
                tmp=arr[i][num1-1];//同理进行列变换的时候约定列为需要交换的两列不变后，循环递归循环行即可。
                arr[i][num1-1]=arr[i][num2-1];
                arr[i][num2-1]=tmp;
            }
        }

    }
    
    for(i=0;i<n;i++)
    {
        for(j=0;j<m;j++)
        {
            printf("%d ",arr[i][j]);
        }
        printf("\n");
    }
    
    

    
    return 0;
}

```