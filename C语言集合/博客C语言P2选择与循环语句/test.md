[TOC]
循环结构为什么会出现，因为人生就是日复一日的不断重复既有相同的事情，又有相似的事情，却无论如何也无法回到最初。要想在生活中的每一刻都能有新的发现，恐怕只是一个美好的愿景。
选择结构为什么会出现，因为我们的每一天不会一模一样的度过，例如，如果今天下雨了，你就要带伞。  
## 1.分支语句
### 1.1 if语句  
==**if语句的基本语法**==  
```c
if(判断表达式)//表达式结果非零执行分支一，否则执行分支二；
{
    分支一；
}
{
    分支二；
}

```  
上面是单分支的情况，当然if语句也可以实现多分支如下：  
```c
if(表达式)
{
    分支一；
}
else if (表达式)
{
    分支二；
}
else
{
    分支三；
}
```  
当然你还可以写更多的分支，但一般分支特别多的时候我们习惯使用switch语句这个后面介绍。  
==知识点1：悬空else问题==  
```c
#include <stdio.h>
int main()
{
int a = 0;
int b = 2;
if(a == 1)
  if(b == 2)
    printf("hehe\n");
  else
    printf("haha\n");
return 0;
}
```  
在这里我们打印的结果是什么呢？答案是“hehe”，那么为什莫呢？  
这里的else并不会和第一个if结合，else只会和离他最近的if结合，这就是悬空else问题。  
==知识点2：if和else实际为一个语句==  
下面这个程序就证明了if和else实际是一个语句  
```c
#include<stdio.h>

int main()
{
    int n=0;
    scanf("%d",&n);
    if(n>0)
        if(n%2==0)
            printf("该数为偶数\n");
        else
            printf("该数为奇数\n");
    else
        printf("您输入的不是正数");   
    return 0;             

}
```  
上述代码实际上很容易造成误解，虽然有缩进但是会让人误会，下面我们来改进一下；
```c
#include<stdio.h>

int main()
{
    int n=0;
    scanf("%d",&n);
    if(n>0)
    {
        if(n%2==0)
        {
            printf("该数为偶数\n");
        }
        else
        {
            printf("该数为奇数\n");
        }
    }
    else
    {
        printf("您输入的不是正数"); 
    }
    return 0;
}
```  
这样看来是不是就清晰很多了，实际上上面两个代码的结果是一样的，这也说明了，在c语言中if和配套的else合起来看作一个语句。  
==知识点3：短路求值问题==  
在我们写分支语句的过程中肯定会写判断语句，那么短路求值的意思就是：  
在使用逻辑与运算符的时候例如：  
表达式1&&表达式2  
如果表达式1的判断结果为假，那么整个表达式结果为假，表达式2就不会再去判断了。当然，平时使用没问题，但是  
当表达式2为st++；这种自增语句的时候就会有问题了，因为跳过了表达式2，所以表达式2 的值就不会自增。  
同样的逻辑或运算符也是一样：
表达式1 | | 表达式2
如果表达式1 判断为真，那么表达式2就不会再判断； 

### 1.2 switch语句  
==**switch语句的基本语法**==
```c
#include<stdio.h>
int ch=0;
scanf("%d",&ch);

switch(ch)
{
    case 1:
    printf("hellow world");
    break;
    case 2:
    printf("hellow world");
    break;
    case 3:
    printf("hellow world");
    break;
    ......
}
```  
其中ch必须为整形常量表达式，case后面的必须是常量；  
==switch 中 break的作用==
其实没有break的switch语句并不能实现多分支，因为当ch=1是程序跳到case 1；这个标签处执行下面的语句，遇到break，跳出switch语句，若是没有break则继续执行case 2；标签下面的内容，这就是break的作用。
==switch 语句中 default 的作用==
上面程序我们写完整一下：
```c
#include<stdio.h>
int ch=0;
scanf("%d",&ch);

switch(ch)
{
    case 1:
    printf("hellow world");
    break;
    case 2:
    printf("hellow world");
    break;
    case 3:
    printf("hellow world");
    break;
    default :
    printf("input is error\n");
    break;
}
```  
如果我们给ch输入8，程序中没有case 8：这个标签，就会执行default语句下的代码；default可以放在switch语句中的任意位置，最好是放在结尾。

---
以上我们学完了switch语句和if语句那么什么时候用if 什么时候用switch呢？
一般情况下，我们通过**单一表达式**控制程序分支的时候，使用switch语句的效果比if语句的效果要好。
## 2.循环语句 
### 2.1 while语句
下面我来介绍while的基本语法，直接来看一个例子，
输入一个整数值，数值它递减到零的每个数字。
```c
#include<stdio.h>

int main()
{
    int n=0;
    scanf("%d",&n);
    while(n>=0)
    {
        printf("%d",n);
        n--;
    }
    return 0;
}
```  
这就是一个很简单的while循环，在while的大括号内就是循环体，执行完一次之后就上去判断n>=0 如果成立，继续循环，直到n小于0；
==知识点1：while循环中的break==
```c
#include<stdio.h>

int main()
{
    int n=0;
    scanf("%d",&n);
    while(n>=0)
    {
        printf("%d",n);
        n--;
        if(2==n)
        {
            break;
        }
    }
    return 0;
}
```  
上面代码我进行了修改，如果n等于2时就执行break 这里会直接跳出循环，所以break的作用就是跳出while（要注意如果while嵌套，break只能跳出一层）
==知识点2：while中的continue==
这里出现了一个新关键字，continue，老规矩看代码：
```c
#include<stdio.h>
int main()
{
    int n=0;
    scanf("%d",&n);
    while(n>=0)
    {
        printf("%d",n);
        
        if(2==n)
        {
            continue;
        }
        n--;

    }
    return 0;
}
```  
可以停下来想一想这个代码的执行结果是什么  
如果我们输入5，那么会打印 5 4 3 2 2 2 2 2······然后死循环了，那么这里continue的作用就是，结束本次循环直接跳到判断部分，也就是跳过了continue后面的代码，因为跳过了调整部分所以造成了死循环，实际写代码的时候要注意哦。

### 2.2 do while 语句
do while，顾名思义，上来先干，不管怎么样先进行一次循环，然后在判断，满足条件就继续，不满足就结束。
先看栗子，输入一个整数显示他是奇数还是偶数，然后重复。
上代码！
```c
#include<stdio.h>

int main()
{
    int retry=0;
    do
    {
        int n=0;
        printf("请输入一个整数：");
        scanf("%d",&n);
        if(n%2==0)
        {
            printf("偶数\n");
        }
        else
        {
            printf("奇数\n");
        }
        printf("重复：yes\1  no\0:);
        scanf("%d",&retry);
    }while(retry);
    return 0;
}
```  
正如上面一样上来先执行一次，然后判断是否继续重复。其他与while类似不多赘述。
### 2.3 for语句 
==**for语句语法**==
写一个程序输出0-12之间的数字
直接上代码
```c
#include<stdio.h>

int main()
{
    int n=0;
    for(n=0;n<=12;n++)
    {
        printf("%d ",n);
    }
    return 0;
}
```  
上面就是for的基本语法，n=0；为初始化语句，n<=12为判断语句，n++为调整语句。
==知识点1：for语句中的break==
如同while循环一样，for循环内部遇到break会跳出循环；
==知识点2：for语句中的continue==
虽然作用和while中的continue一样但是，for循环中的continue，并不是直接跳到了程序的判断部分，而是跳到了程序的调整部分，这样子程序就不会出现像是，while中的死循环。
==知识点3：for循环的变种==
在for循环中（）内的三个表达式都是可以省略的，如果省略了第二个表达式也就是判断表达式，那就会死循环，初学不建议省略

---
循环语句的使用中要注意，尽量不要在循环内部修改循环的控制量，防止循环失去控制。

## 3.goto语句  
关于goto，下面这段话是我copy的，大家看看理解就好，goto的使用很简单
C语言中提供了可以随意滥用的 goto语句和标记跳转的标号。
从理论上 goto语句是没有必要的，实践中没有goto语句也可以很容易的写出代码。
但是某些场合下goto语句还是用得着的，最常见的用法就是终止程序在某些深度嵌套的结构的处理过
程。
例如：一次跳出两层或多层循环。
多层循环这种情况使用break是达不到目的的。它只能从最内层循环退出到上一层的循环。
代码演示：
```c
for(...)
    for(...)
    {
        for(...)
        {
            if(disaster)
            goto error;
        }
    }
…
error:
    if(disaster)
// 处理错误情况
```  
这就是goto最常用的场景，跳出多层嵌套的循环，可以省去写多个break；

---
## 4.练习题 
**练习1**
编写代码，演示多个字符从两端移动，向中间汇聚
```c
#include<stdio.h>
#include<string.h>
#include<Windows.h>
int main()
{
    char arr[] = { "***************" };
    char brr[] = { "hellow world!!!" };
    int sz = strlen(arr);
    int left = 0;
    int right = sz - 1;
    while (left <= right)
    {
        arr[left] = brr[left];
        arr[right] = brr[right];
        right--;
        left++;
        printf("%s\n", arr);
        Sleep(1000);

        system("cls");

    }
    printf("%s\n", arr);


    return 0;
}
```  
**练习2**  
二分查找法（这里是最简单的二分查找，详细的后面会介绍的）
```c
#include<stdio.h>

int main()
{
	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
	int findnum = 8;
	int left = 0;
	int right = sizeof(arr) / sizeof(arr[0]);
	while (left <= right)
	{
		int mid = left + (right - left) / 2;
		if (arr[mid] < findnum)
		{
			left = mid + 1;
		}
		else if (arr[mid] > findnum)
		{
			right = mid - 1;
		}
		else
		{
			printf("找到了，下标为 %d ", mid);
			break;
		}
	}

	return 0;
}
```  
**练习3**
猜数字游戏
```c
#include<stdio.h>
#include<time.h>
void menu()
{
	printf("***********************\n");
	printf("******  1  play  ******\n");
	printf("******  0  exit  ******\n");
	printf("***********************\n");

}

void play()
{
	int ret = rand()%100;
	while (1)
	{
		printf("请开始猜数字吧！\n");
		int sc = 0;
		scanf("%d", &sc);
		if (sc < ret)
		{
			printf("猜小了\n");
		}
		else if (sc > ret)
		{
			printf("猜大了\n");
		}
		else
		{
			printf("恭喜你，猜对了！！！\n");
			break;
		}

	}
}

int main()
{
	srand((unsigned int)time(NULL));
	int input = 0;

	do
	{
		menu();
		scanf("%d", &input);
		if (input == 1)
		{
			play();
		}
	} while (input);
	return 0;
}
```  
**练习4**  
关机小程序，用goto语句实现
```c
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
int main()
{
	char arr[10] = { 0 };
	system("shutdown -s -t 60");
again:
	printf("请输入我是猪，否则你的电脑将在60s内关机\n");
	printf("请输入：");
	scanf("%s", &arr);
	if (0==strcmp(arr,"我是猪"))
	{
		printf("取消关机\n");
		system("shutdown -a");
	}
	else
	{
		goto again;
	}

	return 0;
}
```